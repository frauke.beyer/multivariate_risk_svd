from sklearn.datasets import make_circles
from sklearn.neighbors import kneighbors_graph
import numpy as np

from itertools import chain

import numpy as np
import pandas as pd
import seaborn as sns


#taken from here: https://juanitorduz.github.io/spectral_clustering/

def generate_circle_sample_data(r, n, sigma):
        """Generate circle data with random Gaussian noise."""
        angles = np.random.uniform(low=0, high=2*np.pi, size=n)
    
        x_epsilon = np.random.normal(loc=0.0, scale=sigma, size=n)
        y_epsilon = np.random.normal(loc=0.0, scale=sigma, size=n)
    
        x = r*np.cos(angles) + x_epsilon
        y = r*np.sin(angles) + y_epsilon
        return x, y
    
def generate_concentric_circles_data(param_list):
        """Generates many circle data with random Gaussian noise."""
        coordinates = [ 
            generate_circle_sample_data(param[0], param[1], param[2])
         for param in param_list
        ]
        return coordinates
    

# Number of points per circle. 
n = 1000
# Radius. 
r_list =[2, 4, 6]
# Standar deviation (Gaussian noise). 
sigmas = [0.1, 0.25, 0.5]

param_lists = [[(r, n, sigma) for r in r_list] for sigma in sigmas] 
# We store the data on this list.
coordinates_list = []

fig, axes = plt.subplots(3, 1, figsize=(7, 21))

for i, param_list in enumerate(param_lists):
    coordinates=generate_concentric_circles_data(param_list)
    coordinates_list.append(coordinates)
    ax = axes[i]
    for j in range(0, len(coordinates)): 
        x, y = coordinates[j]
        sns.scatterplot(x=x, y=y, color='black', ax=ax)
        #ax.set(title=f'$\sigma$ = {param_list[0][2]}')



from itertools import chain

coordinates = coordinates_list[0]

def data_frame_from_coordinates(coordinates): 
    """From coordinates to data frame."""
    xs = chain(*[c[0] for c in coordinates])
    ys = chain(*[c[1] for c in coordinates])
    
    return pd.DataFrame(data={'x': list(xs), 'y': list(ys)})

data_df = data_frame_from_coordinates(coordinates)

# Plot the input data.
fig, ax = plt.subplots()
sns.scatterplot(x='x', y='y', color='black', data=data_df, ax=ax)
ax.set(title='Input Data');

from scipy import sparse

def generate_graph_laplacian(df, nn):
    """Generate graph Laplacian from data."""
    # Adjacency Matrix.
    connectivity = kneighbors_graph(X=df, n_neighbors=nn, mode='connectivity')
    adjacency_matrix_s = (1/2)*(connectivity + connectivity.T)
    # Graph Laplacian.
    graph_laplacian_s = sparse.csgraph.laplacian(csgraph=adjacency_matrix_s, normed=True)
    graph_laplacian = graph_laplacian_s.toarray()
    return graph_laplacian 
    
graph_laplacian = generate_graph_laplacian(df=data_df, nn=8)

# Plot the graph Laplacian as heat map.
fig, ax = plt.subplots(figsize=(10, 8))
sns.heatmap(graph_laplacian, ax=ax, cmap='viridis_r')
ax.set(title='Graph Laplacian');

from scipy import linalg
n_components=50

#using linalg
eigenvals, eigenvcts = linalg.eig(graph_laplacian)
eigenvals = np.real(eigenvals)

eigenvals_sorted_indices = np.argsort(eigenvals)
eigenvals_sorted = eigenvals[eigenvals_sorted_indices]

#using eighsh with smallest eigenvalues as input
eigenvalues_eighsh, eigenvectors_eighsh = eigsh(graph_laplacian, k=n_components,
                                      which='SM')


fig, ax = plt.subplots(figsize=(10, 6))
sns.lineplot(x=range(1, eigenvals_sorted_indices.size + 1), y=eigenvals_sorted, ax=ax)
ax.set(title='Sorted Eigenvalues Graph Laplacian', xlabel='index', ylabel=r'$\lambda$');
sns.lineplot(x=range(1, eigenvalues_eighsh.size + 1), y=eigenvalues_eighsh, ax=ax)
ax.set(title='Sorted Eigenvalues Graph Laplacian', xlabel='index', ylabel=r'$\lambda$');  


  
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  9 12:51:07 2023

@author: fbeyer
"""

def _set_diag(laplacian, value, norm_laplacian=True):
    """Set the diagonal of the laplacian matrix and convert it to a
    sparse format well suited for eigenvalue decomposition
    Parameters
    ----------
    laplacian : array or sparse matrix
        The graph laplacian
    value : float
        The value of the diagonal
    norm_laplacian : bool
        Whether the value of the diagonal should be changed or not
    Returns
    -------
    laplacian : array or sparse matrix
        An array of matrix in a form that is well suited to fast
        eigenvalue decomposition, depending on the band width of the
        matrix.
    """
    from scipy import sparse

    n_nodes = laplacian.shape[0]
    # We need all entries in the diagonal to values
    if not sparse.isspmatrix(laplacian):
        if norm_laplacian:
            laplacian.flat[::n_nodes + 1] = value
    else:
        laplacian = laplacian.tocoo()
        if norm_laplacian:
            diag_idx = (laplacian.row == laplacian.col)
            laplacian.data[diag_idx] = value
        # If the matrix has a small number of diagonals (as in the
        # case of structured matrices coming from images), the
        # dia format might be best suited for matvec products:
        n_diags = np.unique(laplacian.row - laplacian.col).size
        if n_diags <= 7:
            # 3 or less outer diagonals on each side
            laplacian = laplacian.todia()
        else:
            # csr has the fastest matvec and is thus best suited to
            # arpack
            laplacian = laplacian.tocsr()
    return laplacian



def run_spectral_clustering(merged_input, mask, simtype, n_components, n_clusters,  eigengap=True, norm_laplacian=True):

    import numpy as np
    import os
    from nilearn import image
    #import scipy.io as spio
    import nibabel as nib
    from nilearn.masking import apply_mask
    from sklearn.cluster import SpectralClustering
    from sklearn.metrics.pairwise import pairwise_kernels
    from scipy import sparse
    from scipy.sparse.linalg import eigsh




    #Prepare data 
    print('loading mask data')
    img = nib.load(mask)
    dat=img.get_data()
    shape_mask=np.shape(dat)

    if not os.path.isfile("wmlinput.npy"):
        print('loading WML data from nifti')
        allwml = nib.load(merged_input)
        X=apply_mask(allwml, img, dtype='f', smoothing_fwhm=None, ensure_finite=True)
        Xtr=np.transpose(X)  #shape: (20335, 1781), Phuah et al had around 30.000 voxels per participant
        np.save("wmlinput.npy",Xtr)
    else:
        print('loading WML data from .npy')
        Xtr=np.load("wmlinput.npy")
    
    #it does not work to load data from matlab properly
    #mat = spio.loadmat('/data/pt_life_whm/Results/NNMF/old_1115/input_data/WML_65_usable_binds_min11_3mm.mat', squeeze_me=True)
    #X=mat['usable_WML_r']


    #create similarity matrix
    #cossin similarity seems to be good for sparse vectors (such as WML across voxels)

    #but in Phuah et al they use Gaussian kernel with sigma=16 (gamma=2 ? 4?) for similarity, Laplacian transformation prior to Eigen-decomposition, 
    #k-means clustering to group highly correlated voxels, mapped back to MNI space.
    #https://scikit-learn.org/stable/modules/metrics.html#rbf-kernel
    if simtype=='cosine':
        if not (os.path.isfile("simmmatrix_%s.npy" %simtype)):
            print('calculate similarity matrix with %s' %simtype)
            simmatrix=pairwise_kernels(Xtr, metric=simtype)
            np.save(("simmmatrix_%s.npy" %simtype), simmatrix)
        else: 
            print('load similarity matrix with %s' %simtype)
            simmatrix=np.load("simmmatrix_%s.npy" %simtype)
    elif simtype=='phuahsigma2':
        if not (os.path.isfile("simmmatrix_%s.npy" %simtype)):
            print('calculate similarity matrix with %s' %simtype)
            simmatrix=pairwise_kernels(Xtr, metric='rbf', gamma=1/4) #sigma=2, sigma**2=4, so gamma=1/sigma**2 = 1/16
            np.save(("simmmatrix_%s.npy" %simtype), simmatrix)
        else: 
            print('load similarity matrix with %s' %simtype)
            simmatrix=np.load("simmmatrix_%s.npy" %simtype)
    elif simtype=='phuahsigma8':
        if not (os.path.isfile("simmmatrix_%s.npy" %simtype)):
            print('calculate similarity matrix with %s' %simtype)
            simmatrix=pairwise_kernels(Xtr, metric='rbf', gamma=1/64) #sigma=2, sigma**2=4, so gamma=1/sigma**2 = 1/16
            np.save(("simmmatrix_%s.npy" %simtype), simmatrix)
        else: 
            print('load similarity matrix with %s' %simtype)
            simmatrix=np.load("simmmatrix_%s.npy" %simtype)
    elif simtype=='phuahsigma4':
        if not (os.path.isfile("simmmatrix_%s.npy" %simtype)):
            print('calculate similarity matrix with %s' %simtype)
            simmatrix=pairwise_kernels(Xtr, metric='rbf', gamma=1/16) #sigma=4, sigma**2=16, so gamma=1/sigma**2 = 1/16
            np.save(("simmmatrix_%s.npy" %simtype), simmatrix)
        else: 
            print('load similarity matrix with %s' %simtype)
            simmatrix=np.load("simmmatrix_%s.npy" %simtype)
    elif simtype=='phuahsigma16':
        if not (os.path.isfile("simmmatrix_%s.npy" %simtype)):
            print('calculate similarity matrix with %s' %simtype)
            simmatrix=pairwise_kernels(Xtr, metric='rbf', gamma=1/16**2)#sigma=16, so gamma=1/sigma**2 = 1/256
            np.save(("simmmatrix_%s.npy" %simtype), simmatrix)
    elif simtype=='phuahsigma10':
        if not (os.path.isfile("simmmatrix_%s.npy" %simtype)):
            print('calculate similarity matrix with %s' %simtype)
            simmatrix=pairwise_kernels(Xtr, metric='rbf', gamma=1/10**2)#sigma=10, so gamma=1/sigma**2 = 1/100
            np.save(("simmmatrix_%s.npy" %simtype), simmatrix)
        else: 
            print('load similarity matrix with %s' %simtype)
            simmatrix=np.load("simmmatrix_%s.npy" %simtype)
   

    
    if eigengap:
        print("perform eigenvalue estimation for first %i eigenvalues" %n_components)
        #Perform Eigengap method (following here: https://juanitorduz.github.io/spectral_clustering/)
        #do the same steps as in scikit-learn code: first transform matrix to sparse and set diagonal to 1 (shifting eigenvalues -1 -> 1)
        #then invert (also eigenvalues so that smallest eigenvalues are around 1).
        #Use inverse-shift-mode
        #https://github.com/scikit-learn/scikit-learn/blob/f0ab589f/sklearn/manifold/spectral_embedding_.py#L308
        #https://github.com/scikit-learn/scikit-learn/blob/f0ab589f/sklearn/cluster/spectral.py#L259
        laplacian = sparse.csgraph.laplacian(simmatrix, normed=norm_laplacian)
        laplacian = _set_diag(laplacian, 1, norm_laplacian)

        laplacian *= -1
        
        lambdas, diffusion_map = eigsh(laplacian, k=n_components,
                                    sigma=1.0, which='LM')
        return(lambdas)

    else:
        #Perform clustering with defined number of clusters
        print("perform spectral clustering with %i clusters" %n_clusters)
        clustering = SpectralClustering(n_clusters=n_clusters,affinity='precomputed', #affinity="rbf", #
        assign_labels="kmeans",
        random_state=0).fit(simmatrix) 

        dat[dat==1]=clustering.labels_
        dat_resh=np.reshape(dat, shape_mask)

        array_img = nib.Nifti1Image(dat_resh, img.affine)
        nib.save(array_img, ('/scratch/beyer/multivariate_risk_svd/Analysis/LIFE/SpectralClustering/%s_n%s_clusters.nii' %(simtype, n_clusters)))


        
        
        return(clustering)

  
  
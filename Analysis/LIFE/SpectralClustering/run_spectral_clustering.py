#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  9 12:51:07 2023

@author: fbeyer
"""

from functions_spectral_clustering import _set_diag
from functions_spectral_clustering import run_spectral_clustering
import matplotlib.pyplot as plt
import numpy as np
import nibabel as nib

merged_input="/scratch/beyer/3C/BIDS/derivatives/WML/merged/mergedfile_N1781_space-MNI_res-3.nii.gz"
mask="/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/input_data/WML_mask_min17_3mm.nii"
simtype="phuahsigma2" #implemented: phuahsigma4, phuahsigma16, cosine, phuasigma10

out=run_spectral_clustering(merged_input, mask, simtype=simtype, n_components=20, n_clusters=4, eigengap=True)

#For Eigenvalue case
plt.plot(out)
plt.savefig('Eigengap_results_%s.png' %simtype)

# Identify the optimal number of clusters as the index corresponding
# to the larger gap between eigen values

topK=10
index_largest_gap = np.argsort(np.diff(out))[::-1][:topK]
nb_clusters = index_largest_gap + 1

print(nb_clusters)
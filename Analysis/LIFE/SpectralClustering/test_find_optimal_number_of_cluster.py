import numpy as np
from nilearn import image
import scipy.io as spio
import nibabel as nib
from nilearn.masking import apply_mask
from sklearn.cluster import SpectralClustering
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.metrics.pairwise import pairwise_kernels
from scipy import sparse
from scipy.linalg import eigh
from scipy.sparse.linalg import eigsh, lobpcg
from scipy.sparse.csgraph import connected_components
import matplotlib.pyplot as plt


#see https://juanitorduz.github.io/spectral_clustering/
simtype="phuahsigma2"
simmatrix=np.load("simmmatrix_%s.npy" %simtype)

def eigenDecomposition(A, plot = True, topK = 5):
    #Calculate Laplacian of affinity matrix and solve using apack
    laplacian, dd = sparse.csgraph.laplacian(A, normed=True,
                                                 return_diag=True)
    n_components=20
    
    #find smallest eigenvalues as described in tutorial 
    eigenvalues, eigenvectors = eigsh(laplacian, k=n_components,
                                      which='SM')
    
    
    if plot:
            plt.title('Smallest eigen values of input matrix')
            plt.plot(np.arange(len(eigenvalues)), eigenvalues)
            plt.grid()          
            plt.savefig('Eigengap_results_rightorder_%s.png' %simtype)

    # Identify the optimal number of clusters as the index corresponding
    # to the larger gap between eigen values
    #here take the eigenvalues as they come as smallest are looked for
    index_largest_gap = np.argsort(np.diff(eigenvalues))[::-1][:topK]
    nb_clusters = index_largest_gap + 1
    
    return eigenvalues, nb_clusters

ev, nb=eigenDecomposition(simmatrix)

print(nb)
print(ev)
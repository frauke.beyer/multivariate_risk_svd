create_bullseye<-function(sample, mode="HSB", plot=F, location, filename="tmp.png"){
  
if (plot==T){
library(ggplot2)
library(geomtextpath )}

#data.frame:
#  "sample" must include variables: pseudonym (MRT-Pseudonym), MR_y_n_fu, EstimatedTotalIntraCranialVol
#location:
#  path to individual parcellations, e.g. "/data/pt_life_whm/Data/WMparcellations_indiv/"
#mode: 
#  - HSB: refers to WMH segmentation with sum of probabilities in parcels which uses only baseline segmentations
#  - long: uses binarized values from coregistered LCL for subjects with long LST
#  - other/old: uses binarized probabilities at 0.1 & for FU subjects results from LCL and for BL baseline segmentations

# Function creates bullseye plot for TIV adjusted, median WMH

if (mode=="HSB" ){
    print(mode)
  colnames=c(
    "BG_1","BG_2", "BG_3", "BG_4",
    "frontal_lh_1","frontal_lh_2","frontal_lh_3","frontal_lh_4",
    "occipital_lh_1","occipital_lh_2","occipital_lh_3","occipital_lh_4",
    "temporal_lh_1","temporal_lh_2","temporal_lh_3","temporal_lh_4",
    "parietal_lh_1","parietal_lh_2","parietal_lh_3","parietal_lh_4",
    "frontal_rh_1","frontal_rh_2","frontal_rh_3","frontal_rh_4",
    "occipital_rh_1","occipital_rh_2","occipital_rh_3","occipital_rh_4",
    "temporal_rh_1","temporal_rh_2","temporal_rh_3","temporal_rh_4",
    "parietal_rh_1","parietal_rh_2","parietal_rh_3","parietal_rh_4")

  subj0=sample[1,"pseudonym"]
  if (sample[1,"MR_y_n_fu"]==0){
    res=read.csv(paste0(location,subj0,"/res_HSB.txt"))
    res=t(res)}else
    {res=read.csv(paste0(location,"blwmh/", subj0,"/res_HSB.txt"))
    res=t(res)}
  
  #then run loop from subject 2 on
  for (subj in sample$pseudonym[2:length(sample$pseudonym)]){
    if (sample[sample$pseudonym==subj,"MR_y_n_fu"]==0){
      #print(subj)
      tmp=read.csv(paste0(location,subj,"/res_HSB.txt"))
      res=rbind(res,t(tmp))
    }
    else{
      #print(subj)
      tmp=read.csv(paste0(location,"blwmh/", subj,"/res_HSB.txt"))
      res=rbind(res,t(tmp))
    }
  }
  
  res=as.data.frame(res)
  colnames(res)=colnames
  res$pseudonym=substring(rownames(res),first=4, last=14)
  
  res=merge(res, sample[c("pseudonym", "EstimatedTotalIntraCranialVol")], by="pseudonym")
  
  res_long = res %>% 
    pivot_longer(
      cols = BG_1:parietal_rh_4,
      names_to = c("bullseye_c"),
      values_to = "val",
      values_drop_na = TRUE
    )
  
  
  
  regions=factor(rep(c("BG", "frontal_lh", "occipital_lh", "temporal_lh", "parietal_lh", "frontal_rh", "occipital_rh", "temporal_rh", "parietal_rh"),
                     each=4),
                 levels = c("frontal_rh", "temporal_rh", "parietal_rh",
                            "occipital_rh", "BG", "occipital_lh", "parietal_lh",
                            "temporal_lh", "frontal_lh"))
  depth=rep(c("1","2","3","4"),9)
  res_long$region=rep(regions,length(unique(res$pseudonym)))
  res_long$depth=rep(depth,length(unique(res$pseudonym)))
  
  
  
  
  meanTIV=mean(res_long$EstimatedTotalIntraCranialVol, na.rm=T)
  res_long = res_long %>% mutate(WMH_vol_adj=val*meanTIV/EstimatedTotalIntraCranialVol)
  
  ##summarise data for repeating plot from paper
  res_sum = res_long %>% group_by(region,depth) %>% summarise(med_WMH_vol_adj=median(WMH_vol_adj, na.rm=T))
  
}else if(mode=="long" ){
    print(mode)
    colnames=c("undefined",
                          "BG_1","BG_2", "BG_3", "BG_4",
                          "frontal_lh_1","frontal_lh_2","frontal_lh_3","frontal_lh_4",
                          "occipital_lh_1","occipital_lh_2","occipital_lh_3","occipital_lh_4",
                          "temporal_lh_1","temporal_lh_2","temporal_lh_3","temporal_lh_4",
                          "parietal_lh_1","parietal_lh_2","parietal_lh_3","parietal_lh_4",
                          "frontal_rh_1","frontal_rh_2","frontal_rh_3","frontal_rh_4",
                          "occipital_rh_1","occipital_rh_2","occipital_rh_3","occipital_rh_4",
                          "temporal_rh_1","temporal_rh_2","temporal_rh_3","temporal_rh_4",
                          "parietal_rh_1","parietal_rh_2","parietal_rh_3","parietal_rh_4")
    
    subj0=unique(sample$pseudonym)[1]
    # First subject at baseline
    res=read.csv(paste0(location,subj0,"/_extractparc0/res.txt"))
    res=t(res)
    
    tmp=read.csv(paste0(location,subj0,"/_extractparc1/res.txt"))
    res=rbind(res,t(tmp))
    
    #then run loop from subject 2 on and add baseline + followup data
    for (subj0 in unique(sample$pseudonym)[2:length(unique(sample$pseudonym))]){
        tmp=read.csv(paste0(location,subj0,"/_extractparc0/res.txt"))
        res=rbind(res,t(tmp))
        tmp=read.csv(paste0(location,subj0,"/_extractparc1/res.txt"))
        res=rbind(res,t(tmp))
      }

    #add identified of timepoint
    res=as.data.frame(res)
    colnames(res)=colnames
    res$tp=rep(c("bl","fu"), times=length(unique(sample$pseudonym)))
    res$pseudonym=rep(substring(rownames(res),first=4, last=14)[seq(1,length(rownames(res)),2)], each=2)
    
    res=merge(res, sample[duplicated(sample$pseudonym),c("pseudonym", "EstimatedTotalIntraCranialVol")], all.x=T, by="pseudonym")
    
    res_long = res %>% 
      pivot_longer(
        cols = BG_1:parietal_rh_4,
        names_to = c("bullseye_c"),
        values_to = "val",
        values_drop_na = TRUE
      )
    
    
    
    regions=factor(rep(c("BG", "frontal_lh", "occipital_lh", "temporal_lh", "parietal_lh", "frontal_rh", "occipital_rh", "temporal_rh", "parietal_rh"),
                       each=4),
                   levels = c("frontal_rh", "temporal_rh", "parietal_rh",
                              "occipital_rh", "BG", "occipital_lh", "parietal_lh",
                              "temporal_lh", "frontal_lh"))
    depth=rep(c("1","2","3","4"),9)
    res_long$region=rep(regions,2*length(unique(res$pseudonym)))
    res_long$depth=rep(depth,2*length(unique(res$pseudonym)))
    
    
    
    
    meanTIV=mean(res_long$EstimatedTotalIntraCranialVol, na.rm=T)
    res_long = res_long %>% mutate(WMH_vol_adj=val*meanTIV/EstimatedTotalIntraCranialVol)
    
    ##summarise data for repeating plot from paper
    res_sum = res_long %>% group_by(region,depth) %>% summarise(med_WMH_vol_adj=median(WMH_vol_adj, na.rm=T))
    

}else{
colnames=c("undefined",
             "BG_1","BG_2", "BG_3", "BG_4",
             "frontal_lh_1","frontal_lh_2","frontal_lh_3","frontal_lh_4",
             "occipital_lh_1","occipital_lh_2","occipital_lh_3","occipital_lh_4",
             "temporal_lh_1","temporal_lh_2","temporal_lh_3","temporal_lh_4",
             "parietal_lh_1","parietal_lh_2","parietal_lh_3","parietal_lh_4",
             "frontal_rh_1","frontal_rh_2","frontal_rh_3","frontal_rh_4",
             "occipital_rh_1","occipital_rh_2","occipital_rh_3","occipital_rh_4",
             "temporal_rh_1","temporal_rh_2","temporal_rh_3","temporal_rh_4",
             "parietal_rh_1","parietal_rh_2","parietal_rh_3","parietal_rh_4")

  subj0=sample[1,"pseudonym"]
  if (sample[1,"MR_y_n_fu"]==0){
    res=read.csv(paste0(location,subj0,"/res.txt"))
    res=t(res)}else
    {res=read.csv(paste0(location,subj0,"/_extractparc0/res.txt"))
    res=t(res)}
  
  #then run loop from subject 2 on
  for (subj in sample$pseudonym[2:length(sample$pseudonym)]){
    if (sample[sample$pseudonym==subj,"MR_y_n_fu"]==0){
      tmp=read.csv(paste0(location,subj,"/res.txt"))
      res=rbind(res,t(tmp))
    }
    else{
      tmp=read.csv(paste0(location,subj,"/_extractparc0/res.txt"))
      res=rbind(res,t(tmp))
    }
  }
  
  
  res=as.data.frame(res)
  colnames(res)=colnames
  res$pseudonym=substring(rownames(res),first=4, last=14)
  
  res=merge(res, sample[c("pseudonym", "EstimatedTotalIntraCranialVol")], by="pseudonym")
  
  res_long = res %>% 
    pivot_longer(
      cols = BG_1:parietal_rh_4,
      names_to = c("bullseye_c"),
      values_to = "val",
      values_drop_na = TRUE
    )
  
  
  
  regions=factor(rep(c("BG", "frontal_lh", "occipital_lh", "temporal_lh", "parietal_lh", "frontal_rh", "occipital_rh", "temporal_rh", "parietal_rh"),
                     each=4),
                 levels = c("frontal_rh", "temporal_rh", "parietal_rh",
                            "occipital_rh", "BG", "occipital_lh", "parietal_lh",
                            "temporal_lh", "frontal_lh"))
  depth=rep(c("1","2","3","4"),9)
  res_long$region=rep(regions,length(unique(res$pseudonym)))
  res_long$depth=rep(depth,length(unique(res$pseudonym)))

  meanTIV=mean(res_long$EstimatedTotalIntraCranialVol, na.rm=T)
  res_long = res_long %>% mutate(WMH_vol_adj=val*meanTIV/EstimatedTotalIntraCranialVol)
  
  ##summarise data for repeating plot from paper
  res_sum = res_long %>% group_by(region,depth) %>% summarise(med_WMH_vol_adj=median(WMH_vol_adj, na.rm=T))
  
}

  
  
if (plot==T){
p1=ggplot(res_sum, aes(x = region, y =depth)) +
  geom_tile(aes(fill=med_WMH_vol_adj),
            color = "gray50",
            lwd = 0.3,
            linetype = 1)+
  coord_curvedpolar(theta= "x")+
  scale_fill_gradient(low = "white", high = "red")+
    theme_bw() +
    theme(panel.border = element_blank(),
          axis.text.x = element_text(size = 14),
          axis.title.x = element_blank(),
          axis.title.y = element_blank(),
          axis.ticks.y=element_blank(),
          axis.text.y = element_blank(),
          panel.grid.major = element_blank())
ggsave(filename, width = 8, height=5)
return(list(res_long, p1))}
return(res_long)
}
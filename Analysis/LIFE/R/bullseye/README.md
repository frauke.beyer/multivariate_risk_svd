# Scripts for creating PCA decompositions of WMH Bullseye segmentations

## Main functions
create_PCA.R: create PCA from Bullseye segmentation data, used in scripts in PCA folder
create_bullseye_plot.R: create Bullseye results from outputs of bullseye pipeline by looping over subjects
calc_cog_outcomes.R: calculate cognitive outcomes for LIFE from raw data

## PCA 
All scripts to generate PCA decompositions from different samples.
## Main script for creating 7comp, oblimin, rotation in 3C & apply to LIFE
```create_MAIN_PCA_3C_7comp_oblimin.R``` 
Script is sourced in main manuscript creation

## Supplementary analysis: creating 7comp, no rotation in 3C & apply to LIFE
```create_PCA_3C_7comp_rotnone.R```

## Supplementary analysis: creating 7comp, oblimin rotation in LIFE & apply to 3C
```create_PCA_LIFE_oblimin.R```

## Not Used: apply 3C oblimin PCA to UKBB
```apply_PCA_3C_to_UKBB.R```


## Standalones
Functions that are now integrated into Rmarkdown (/Manuscript) but can still be run individually for backwards-compatability.
create_bullseye_segmentation.R: create .Rdata file with bullseye segmentation results from LIFE

## plot_CVR_models
Old functions for plotting CVR models.

## activate plink2 (by typing plink2 to activate environment)


#first version (without rs6797002)
#cd /data/gh_gr_agingandobesity_share/life_shared/Data/Raw/Genetics_raw/2023_25_WMH_Snps/WMH_SNPs/

#second version (with rs6797002)
#cd /data/gh_gr_agingandobesity_share/life_shared/Data/Raw/Genetics_raw/2023_25_WMH_Snps/WMH_SNPs_updated/

#third version (with rs6797002 and dosage information)
cd /data/gh_gr_agingandobesity_share/life_shared/Data/Raw/Genetics_raw/2023_25_WMH_Snps/WMH_SNPs_dosage/

## some Preprocessing so that plink can work ->
cp Beyer_WMH_SNPs_v2_Genotypes_Impute.txt Beyer_WMH_SNPs_v2.gen
## Change the sample file (done in Rmarkdown -> demographics.Rmd)

## loading data
plink2 \
    --data Beyer_WMH_SNPs_v2 'ref-last' \
    --oxford-single-chr 1 \
    --make-pgen \
    --sort-vars \
    --out lifegen 

# PRS score creation (use correct effect size list where rs6540873 (TRANS) is replaced by rs6797002 (EUR/TRANS)
plink2 --pfile lifegen \
       --score /data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Analysis/LIFE/genetics/Murali_WMH_27riskloci_cSVD_SNP_Effect_renamed_correct.txt 1 2 3 header list-variants



# Export variant-major additive component file (indicating allele dosage of minor allele)
# with risk allele being the one that is counted by A-transpose some SNP references
# Not necessary as K Horn provided dosage file.
#plink2 --pfile lifegen  \
#       --ref-allele 'force' "/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Analysis/LIFE/genetics/Murali_WMH_27riskloci_cSVD_SNP_Effect_renamed.txt" 2 1 \
#       --recode 'A-transpose' \
#       --out lifegen_reref



addpath('/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Analysis/NNMF/brainparts')

%run NNMF for fixed number of components for complete sample
outputdir='/data/pt_life_whm/Results/NNMF/K5_binds_min23_fullsample/';

%info on MRI data
load('/data/pt_life_whm/Results/NNMF/input_data/WML_usable_binds_min23_3mm.mat')
maskfile='/data/pt_life_whm/Results/NNMF/input_data/WML_brainmask_binds_min23_3mm.nii';
%load('/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/input_data/WML_usable_brainmask_3mm.mat')
%maskfile='/scratch/beyer/3C/mni_icbm152_nlin_asym_09c/mni_icbm152_t1_tal_nlin_asym_09c_mask_3mm.nii.gz'
brainm=niftiread(maskfile);
brainm=logical(brainm);
mask=brainm;


%info for NNMF algorithm
K=5; %
info = niftiinfo(maskfile);
X=usable_WML_r; 
w0='';
initMeth=1;
max_iter='';
tol='';
iter0='';
save_step='';

%info for CV
dimV=size(X);


if (~isfile(strcat(outputdir,'/res.mat')))
    [W H]=opnmf_mem(X, K, w0, initMeth, max_iter, tol, iter0, save_step, outputdir);
    L=(W'*X)';
    save(strcat(outputdir,'/res.mat'),'W','H', 'L')
else
    load(strcat(outputdir,'/res.mat'),'W','H', 'L')
end

%save components:
for ind=1:K

    if (~isfile(strcat(outputdir, sprintf('/C%i.nii', ind))))
        C=zeros(size(mask));
        C(mask)=W(:,ind);
        niftiwrite(single(C),  strcat(outputdir, sprintf('/C%i.nii', ind)), info)
    end    
end

writematrix(L, strcat(outputdir,"L.txt"));


addpath('/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Analysis/3C/NNMF/brainparts')


% data preparation
inputdir='/data/pt_life_whm/Results/3C/NNMF/3C_LIFE/input_data/';
outputdir="/data/pt_life_whm/Results/3C/NNMF/3C_LIFE/input_data/";
%outputdir="/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/input_data/";

V = niftiread(append(inputdir,'all_merged_3C_LIFE.nii.gz'));
%'mergedfile_N2350_space-MNI_res-3_type-binds'
dimV=size(V);
N=dimV(4);

%select voxels with a lesion in at least 1% (=17) participants.
info= niftiinfo("/data/pt_life_whm/Results/flair2MNI/atlas/MNI152_T1_3mm_brainmask.nii.gz");


%need to set voxels <0 =0 and remove NaNs
V(V<0)=0;
V(isnan(V))=0;

%for lesionfreqmask: binarize probability (17 for 3C, 23 for LIFE-all, X for LIFE-65y)
Vbin=double(V>0.2);
sumV=sum(Vbin, 4);
niftiwrite(single(sumV), append(outputdir + 'WML_3C_LIFE_sum_binds_3mm.nii'), info);

minN=floor(N/100)
mask_lesionfreq=sumV>=minN; %1ß

%for LIFE:
%2024 voxels in lesionfreq mask at 3mm with 0.2 prob with Nmin=23
%3573 voxels in lesionfreq mask at 3mm with 0.2 prob with Nmin=10
%16325 voxels in lesionfreq mask with bin + downsampling at 3mm with 0.2 prob with Nmin=10
%10910 voxels in lesionfreq mask with bin + downsampling at 3mm with 0.2
%prob with Nmin=23
%77779 voxels in brainmask with bin + downsampling at 3mm with 0.2 prob
%X voxels in lesionfreq mask at 1.5 mm with 0.2 prob

niftiwrite(single(mask_lesionfreq), append(outputdir, sprintf('WML_brainmask_binds_min%i_3mm.nii', minN), info));

usable_WML_r=make_usable_WML(V,mask_lesionfreq);
save(outputdir + sprintf('WML_brainmask_binds_min%i_3mm.mat', minN), 'usable_WML_r')

%select only brain voxels to allow for larger mask
%brainm=niftiread('/scratch/beyer/3C/mni_icbm152_nlin_asym_09c/mni_icbm152_t1_tal_nlin_asym_09c_mask_3mm.nii.gz');
%brainm=niftiread('/data/pt_life_whm/Results/flair2MNI/atlas/MNI152_T1_3mm_brainmask.nii.gz');
%brainm=niftiread('/scratch/beyer/3C/mni_icbm152_nlin_asym_09c/mni_icbm152_t1_tal_nlin_asym_09c_mask_15mm_sub015_applyxsqform_bin.nii');
%brainm=logical(brainm);
%usable_WML_r=make_usable_WML(V,brainm);

%save(append(outputdir,'WML_65_usable_binds_brainmask_3mm.mat'), 'usable_WML_r', '-v7.3')

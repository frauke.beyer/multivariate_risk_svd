addpath('/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Analysis/NNMF/brainparts')

%run NNMF for fixed number of components for complete sample
outputdir='/data/pt_life_whm/Results/3C/NNMF/3C_LIFE/min41_binds_K4';

%info on MRI data
load('/data/pt_life_whm/Results/3C/NNMF/3C_LIFE/input_data/WML_brainmask_binds_min41_3mm.mat');
maskname='/data/pt_life_whm/Results/3C/NNMF/3C_LIFE/input_data/WML_brainmask_binds_min41_3mm.nii';
brainm=niftiread(maskname);
brainm=logical(brainm);
mask=brainm;


%info for NNMF algorithm
K=4; %8 components according to knee_pt.. 5 components according to own check of RE error
info = niftiinfo(maskname);
X=usable_WML_r; 
w0='';
initMeth=1;
max_iter='';
tol='';
iter0='';
save_step='';

%info for CV
dimV=size(X);


if (~isfile(strcat(outputdir,'/res.mat')))
    [W H]=opnmf_mem(X, K, w0, initMeth, max_iter, tol, iter0, save_step, outputdir);
    L=(W'*X)';
    save(strcat(outputdir,'/res.mat'),'W','H', 'L')
else
    load(strcat(outputdir,'/res.mat'),'W','H', 'L')
end

%save components:
for ind=1:K

    if (~isfile(strcat(outputdir, sprintf('/C%i.nii', ind))))
        C=zeros(size(mask));
        C(mask)=W(:,ind);
        niftiwrite(single(C),  strcat(outputdir, sprintf('/C%i.nii', ind)), info)
    end    
end

writematrix(L, strcat(outputdir,"/L.txt"));


cd /data/pt_life_whm/Results/flair2MNI/merged/

fslsplit /data/pt_life_whm/Results/flair2MNI/merged/mergedfile_age65y_qa_checked_space-MNI_res-1.nii.gz

for elem in `ls vol*`
do
echo $elem
#first binarize 1mm images to have more appropriate values after downsampling (which can be thresholded at 0.2 in matlab script)
#fslmaths $elem -thr 0.1 $elem

#second (better approach when staying in 1mm): binarize at threshold 0.1 as suggested in Tsuchida et al.
fslmaths $elem -thr 0.1 -bin ${elem}_thr0.1bin

#flirt -in $elem -ref  $elem \
#-applyisoxfm 3 -out $elem
done


#fslmerge -t mergedfile_age65y_qa_checked_space-MNI_res-1_bin_thr0.1.nii.gz vol*.nii.gz
fslmerge -t mergedfile_age65y_qa_checked_space-MNI_res-1_bin_thr0.1.nii.gz vol*_thr0.1bin*




fsl=6.0.3_fbeyer@protactinium:/data/pt_life_whm/Results/flair2MNI/merged > fslmaths /data/pt_life_whm/Results/flair2MNI/flair2MNI/0CF8489B67/stable2MNI/LCL_ples_lpa_mFLAIR_bl_ples_lpa_mFLAIR_fu_maths_warped_trans.nii.gz -bin /data/pt_life_whm/Results/flair2MNI/flair2MNI/0CF8489B67/stable2MNI/LCL_ples_lpa_mFLAIR_bl_ples_lpa_mFLAIR_fu_maths_warped_trans_bin.nii.gz
fsl=6.0.3_fbeyer@protactinium:/data/pt_life_whm/Results/flair2MNI/merged > fslstats -K  /data/pt_life_whm/Results/flair2MNI/flair2MNI/0CF8489B67/stable2MNI/LCL_ples_lpa_mFLAIR_bl_ples_lpa_mFLAIR_fu_maths_warped_trans_bin.nii.gz /data/pt_life_whm/Results/flair2MNI/flair2MNI/0CF8489B67/stable2MNI/LCL_ples_lpa_mFLAIR_bl_ples_lpa_mFLAIR_fu_maths_warped_trans.nii.gz -V
4490 4490.000000 


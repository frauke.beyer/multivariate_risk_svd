# Preprocessing for NNMF

- merging warped raw WML probabilities (cross) and stable voxel probabilities (long) to be used (all/aged>65y) using `mergeWML.py` to prepare the merge_command.sh file
- run merge_command.sh
- use downsample_mergedWML.sh to split merged file, binarize (without threshold) and downsample to 3 x 3 mm, merge again
- in NNMF preparation in matlab, thresholding of downsampled maps is done at 0.2 (Vbin=double(V>0.2);)

# Preprocessing for combining LIFE and 3C
- conform 3C to LIFE image size by splitting mergedfile_N1781_space-MNI_res-3.nii.gz and using `for elem in vol*; do echo $elem; flirt -in $elem -ref /data/pt_life_whm/Results/flair2MNI/merged/vol0000.nii.gz -out ${elem}_re_wqform -applyxfm -usesqform; done`
- merge all conformed files to merged_N1781_space-MNI_res-3_conformedtoLIFE.nii.gz, then merge with mergedfile_N2350_space-MNI_res-3_type-binds.nii.gz to all_merged_3C_LIFE.nii.gz


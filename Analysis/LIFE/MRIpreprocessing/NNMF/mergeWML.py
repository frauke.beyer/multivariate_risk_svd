# -*- coding: utf-8 -*-
"""
Modified on Oct. 27 2022

@author: fbeyer
"""
import csv 
import os
import shutil
import re
import gzip
import os

i=0

if os.path.isfile("merge_command.sh"):
    os.remove("merge_command.sh")

cmd="fslmerge -t /data/pt_life_whm/Results/flair2MNI/merged/mergedfile_age65y_qa_checked_space-MNI_res-1.nii.gz"
subj=""
with open('/data/pt_life_whm/Results/Tables/2mni/final_usable_NNMF.csv') as subjfile:
    csv_reader = csv.reader(subjfile, delimiter=',')
    line_count = 0
    next(csv_reader)
    for row in csv_reader:
    	print(row[20])
    	if int(row[20])<65:
            #only select older participants
            continue
    	else:
    		print(row[34])
    		if row[34]=="error":
    		    continue
    		else:
    		    subj += "%s\n" %(row[0])
    		    if (row[10]=='99'): #check row qa check -> 99 for cross, 0 or 2 for long subjects
            			print("merge only baseline subjects")
            			cmd+=" /data/pt_life_whm/Results/flair2MNI/flair2MNI/%s/lesion2MNI/ples_lpa_mFLAIR_bl_warped_trans.nii.gz" %(row[0])
            			#cmd+=" /data/pt_life_whm/Results/flair2MNI/flair2MNI/%s/flair2MNI/mFLAIR_bl_warped_trans.nii.gz" %(row[1])
            			i+=1
    			
    		    else:
            			print("merge baseline of those who have also followup (stable2MNI)")
            			cmd+=" /data/pt_life_whm/Results/flair2MNI/flair2MNI/%s/stable2MNI/LCL_ples_lpa_mFLAIR_bl_ples_lpa_mFLAIR_fu_maths_warped_trans.nii.gz" %(row[0])
            			#cmd+=" /data/pt_life_whm/Results/flair2MNI/flair2MNI/%s/flair2MNI/lmFLAIR_bl_warped_trans.nii.gz" %(row[1])
            			i+=1


text_file = open("merge_command_ab65_test.sh", "w")
n = text_file.write(cmd)
text_file.close()

subj_file=open("subj_file.txt", "w")
m=subj_file.write(subj)
subj_file.close()
print(i)


#!/bin/bash
# Command to launch the extraction of WMH volumes from MNI-based BE segmentation

#############################################################################################################
#####													#####
#####					PATHS THAT MUST BE CHANGED					#####
#####													#####
#############################################################################################################

# Path to the provided directory /SPATIALWMH_GWAS_COMPUTE_PHENOTYPES_scripts/:
PATH_SCRIPT=/data/pt_life_whm/Analysis/multivariate_risk_svd/LIFE/MRIpreprocessing/bullseye/MNI_space/

# Path to the log output: defaults inside /SPATIALWMH_GWAS_COMPUTE_PHENOTYPES_scripts/ but can be changed
log_out=${PATH_SCRIPT}/log

# Number of jobs (subjects) that should be run simultaneously
JOB_ARR_LIMIT=100

# Path to the subjects' list:
sublist_name=/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Analysis/LIFE/MRIpreprocessing/bullseye/MNI_space/participants_rerun.txt #participants_1066

#############################################################################################################

# Create and go to batch_output folder so that out/err go there
if [[ ! -d ${log_out} ]]; then
   mkdir -p ${log_out}
fi

cd $log_out

FILES=($(cat ${sublist_name}))
declare -p FILES > array
 
SIZE=${#FILES[@]}
ZBSIZE=$(($SIZE - 1))

# Create an job aray with sub-jobs for each subj.
echo "Spawning ${SIZE} sub-jobs."
 
if [ $ZBSIZE -ge 0 ]; then
sbatch --array=0-$ZBSIZE%${JOB_ARR_LIMIT} ${PATH_SCRIPT}/extract_bullseye_vols.sh
fi


#!/bin/bash

#!/bin/bash
# Modify/add the following parameters as needed 

#SBATCH -J wmhmni
#SBATCH -t 02:00:00
#SBATCH --output=wmhmni%A_%a.out
#SBATCH --error=wmhmni%A_%a.err
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=2500 #following https://www.mail-archive.com/freesurfer@nmr.mgh.harvard.edu/msg57788.html
#SBATCH --mail-user fbeyer@cbs.mpg.de
#SBATCH --mail-type ARRAY_TASKS


#apply bullseye representation to LIFE-Adult
#file1="/data/pt_life_whm/Results/flair2MNI/merged/mergedfile_age65y_qa_checked_space-MNI_res-1.nii.gz" #merged file without thresholding of WML maps
#file2="/data/pt_life_whm/Results/flair2MNI/merged/mergedfile_age65y_qa_checked_space-MNI_res-1_bin_thr0.1.nii.gz" #merged file with thresholding at probability of 0.1 for WML maps
#fslstats -t -K $MNImask $file2 -V >> /data/pt_life_whm/Results/multivariate_cSVD/bullseye/res_thr0.1_MNImask.txt


source ./array
subj=${FILES[$SLURM_ARRAY_TASK_ID]}

MNImask="/data/pt_life_whm/Analysis/ukbb_mni/bullseyeWMparc_Hammers_MNIspace_1mm.nii.gz" #"/data/pt_life_whm/Analysis/UKBB_spatialWMH/bullseye_MNIatlas_1mm.nii.gz"


if [ -f /data/pt_life_whm/Results/flair2MNI/flair2MNI/$subj/lesion2MNI/ples_lpa_mFLAIR_bl_warped_trans.nii.gz ];
then 
echo $subj "cross"
echo $subj>> /data/pt_life_whm/Results/multivariate_cSVD/bullseye/LIFE/MNI/done.txt
wmh_file="/data/pt_life_whm/Results/flair2MNI/flair2MNI/$subj/lesion2MNI/ples_lpa_mFLAIR_bl_warped_trans.nii.gz"

#Average of WMH > 0
fslstats -K ${MNImask} $wmh_file -M -m >> /data/pt_life_whm/Results/multivariate_cSVD/bullseye/LIFE/MNI/${subj}_mean_WMHweight.txt

#Volume of WMH > 0
fslstats -K ${MNImask} $wmh_file -V >> /data/pt_life_whm/Results/multivariate_cSVD/bullseye/LIFE/MNI/${subj}_volume_WMH.txt




elif [ -f /data/pt_life_whm/Results/flair2MNI/flair2MNI/$subj/lesion2MNI_bl/ples_lpa_mFLAIR_bl_warped_trans.nii.gz ];
then
echo $subj "cross repeated"
echo $subj>> /data/pt_life_whm/Results/multivariate_cSVD/bullseye/LIFE/MNI/done.txt
wmh_file="/data/pt_life_whm/Results/flair2MNI/flair2MNI/$subj/lesion2MNI_bl/ples_lpa_mFLAIR_bl_warped_trans.nii.gz"

#Average of WMH > 0
fslstats -K ${MNImask} $wmh_file -M -m >> /data/pt_life_whm/Results/multivariate_cSVD/bullseye/LIFE/MNI/${subj}_mean_WMHweight.txt

#Volume of WMH > 0
fslstats -K ${MNImask} $wmh_file -V >> /data/pt_life_whm/Results/multivariate_cSVD/bullseye/LIFE/MNI/${subj}_volume_WMH.txt

elif [ -f /data/pt_life_whm/Results/flair2MNI/flair2MNI/$subj/stable2MNI/LCL_ples_lpa_mFLAIR_bl_ples_lpa_mFLAIR_fu_maths_warped_trans.nii.gz ];
then
echo $subj "long"
echo $subj>> /data/pt_life_whm/Results/multivariate_cSVD/bullseye/LIFE/MNI/done_long.txt
else echo $subj "not done"
fi



##Testing 
#fslstats -K /data/pt_life_whm/Data/WMparcellations_indiv/3EE8CC0D23/bullseye_wmparc.nii.gz /data/pt_life_whm/Data/WMparcellations_indiv/3EE8CC0D23/bullseye_wmparc.nii.gz >> /data/pt_life_whm/Data/WMparcellations_indiv/3EE8CC0D23/res_be_vol_fststats.txt






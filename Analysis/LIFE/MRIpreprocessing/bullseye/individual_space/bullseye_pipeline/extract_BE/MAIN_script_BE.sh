#!/bin/bash
# Modify/add the following parameters as needed 

#SBATCH -J spatialwmh
#SBATCH -t 02:00:00
#SBATCH --output=spatialwmh_%A_%a.out
#SBATCH --error=spatialwmh_%A_%a.err
#SBATCH --nodes=1
#SBATCH --ntasks=1
#SBATCH --tasks-per-node=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=2500 #following https://www.mail-archive.com/freesurfer@nmr.mgh.harvard.edu/msg57788.html
#SBATCH --mail-user fbeyer@cbs.mpg.de
#SBATCH --mail-type ARRAY_TASKS,FAIL

# This is a script to run 
# FLAIR to T1 registration using FreeSurfer's bbregister
# Bullseye WM segmentation,
# extraction of 36 (Bullseye regions) + 5 (corpus callosum) spatial WMH volumes and
# extraction of TIV from FreeSurfer.


source ./array
sj_id=${FILES[$SLURM_ARRAY_TASK_ID]}


#sj_id=$5
PATH_SCRIPT=$1
PATH_OUT=$2
PATH_DATA=$3


###################################################################################################
#####										              #####
#####					PATHS THAT MUST BE CHANGED			      #####
#####											      #####
###################################################################################################				        

# File patterns to get transformed FLAIR/WMH maps for each subjct:	

if [ -d /data/pt_life_whm/Data/WMparcellations_indiv/blwmh/${sj_id} ];
then
LESION=${PATH_DATA}/blwmh/${sj_id}/ples_lpa_mFLAIR_bl_warped.nii.gz #LESION map warped to Bullseye
BE=${PATH_DATA}/blwmh/${sj_id}/bullseye_wmparc.nii.gz #Bullseye map
PATH_OUT=${PATH_DATA}/blwmh
else            
LESION=${PATH_DATA}/${sj_id}/ples_lpa_mFLAIR_bl_warped.nii.gz #LESION map warped to Bullseye
BE=${PATH_DATA}/${sj_id}/bullseye_wmparc.nii.gz #Bullseye map
PATH_OUT=${PATH_DATA}
fi

###################################################################################################
# Run only volume extraction step
#use singularity to execute the python file with the appropriate files
cd ${PATH_SCRIPT}
#singularity exec bullseye.sif python --version

python $PATH_SCRIPT/python/run_bullseye_wmvol.py ${sj_id} $PATH_OUT $LESION $BE

###################################################################################################
# Copy EstimatedTotalIntraCranialVolume into results file
#cat $PATH_FREESURFER/${sj_id}/stats/aseg.stats | grep EstimatedTotalIntraCranialVol |  awk -F, '{ print $4 }' >> $PATH_OUT/${sj_id}/res_sum.txt

###################################################################################################
# Removing intermediate files 

rm -rf $PATH_OUT/${sj_id}/bullseyevol

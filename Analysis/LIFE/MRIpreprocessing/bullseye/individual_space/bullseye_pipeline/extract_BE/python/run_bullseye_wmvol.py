#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
running bullseye segmentation for a list of subjects
prerequisites:
	- freesurfer
	- flair/t2 and wmh probability maps
"""

from nipype import Node, Workflow, Function, SelectFiles
from nipype.interfaces import fsl
from nipype.interfaces.utility import IdentityInterface
import nipype.interfaces.freesurfer as fs
import nipype.interfaces.utility as util
import nipype.interfaces.io as nio    
from utils import extract_parcellation
import numpy as np 
import nibabel as nb
import os
import sys



def extract_be_volumes(subject, base_dir, wmh_file, be_file):
    """
    a workflow to extract bullseye segmented wm volumes
    """

    # main workflow
    bullseye_volume = Workflow(name="bullseyevol")
    bullseye_volume.base_dir=base_dir+'/'+subject
    

    # extract wmh volumes from be segmentation 
    extractparc=Node(interface=util.Function(input_names=['in1_file', 'in2_file', 'subject_id', 'option'], output_names=['out_file', 'out_be_vol'],
                                               function=extract_parcellation), name='extractparc')
    extractparc.inputs.option="sum" 
    extractparc.inputs.subject_id=subject      
    extractparc.inputs.in1_file=wmh_file   
    extractparc.inputs.in2_file=be_file                          

    
    # generate datasink
    datasink=Node(name="datasink", interface=nio.DataSink())
    datasink.inputs.base_directory = base_dir
    datasink.inputs.container=subject
    datasink.inputs.substitutions = [('_subject_', '')]

    # connect all nodes
    bullseye_volume.connect([
        (extractparc, datasink,[( 'out_be_vol', '@bevol')])
    ])
   
    
    
    return bullseye_volume



subject=sys.argv[1] 
base_dir=sys.argv[2] 
wmh_file=sys.argv[3] 
be_file=sys.argv[4] 

# run workflow in single-thread mode
bullseye_volume=extract_be_volumes(subject, base_dir, wmh_file, be_file)
#bullseye_lesion.write_graph(graph2use='colored', simple_form=True)
bullseye_volume.run()





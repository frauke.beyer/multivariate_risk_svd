SUBJECTS_DIR="/data/pt_life_freesurfer/freesurfer_all"

# Script to create Bullseye lobar segmentation using FreeSurfer command -> not to be used for anything, just for illustration purposes

cd $SUBJECTS_DIR

for subj in 
do

mkdir $subj/bullseye_rep

for hemi in lh rh
do

mri_annotation2label --subject $subj --hemi $hemi --annotation aparc --outdir $subj/bullseye_rep


 #Frontal
# • Superior Frontal
# • Rostral and Caudal Middle Frontal
# • Pars Opercularis, Pars Triangularis, and Pars Orbitalis
# • Lateral and Medial Orbitofrontal
# • Precentral
# • Paracentral
# • Frontal Pole
# • Rostral Anterior Cingulate
# • Caudal Anterior Cingulate


mri_mergelabels -i $subj/bullseye_rep/$hemi.caudalmiddlefrontal.label -i $subj/bullseye_rep/$hemi.superiorfrontal.label -i $subj/bullseye_rep/$hemi.rostralmiddlefrontal.label -i $subj/bullseye_rep/$hemi.rostralanteriorcingulate.label -i $subj/bullseye_rep/$hemi.frontalpole.label -i $subj/bullseye_rep/$hemi.precentral.label -i $subj/bullseye_rep/$hemi.parstriangularis.label -i $subj/bullseye_rep/$hemi.parsorbitalis.label -i $subj/bullseye_rep/$hemi.parsopercularis.label -i $subj/bullseye_rep/$hemi.paracentral.label -i $subj/bullseye_rep/$hemi.medialorbitofrontal.label -i $subj/bullseye_rep/$hemi.lateralorbitofrontal.label -i $subj/bullseye_rep/$hemi.caudalanteriorcingulate.label -o $subj/bullseye_rep/$hemi.frontalc

#> Parietal
#> • Superior Parietal
#> • Inferior Parietal
#> • Supramarginal
#> • Postcentral
#> • Precuneus
#> • Posterior Cingulate
#> • Isthmus

mri_mergelabels -i $subj/bullseye_rep/$hemi.superiorparietal.label -i $subj/bullseye_rep/$hemi.inferiorparietal.label -i $subj/bullseye_rep/$hemi.postcentral.label -i $subj/bullseye_rep/$hemi.precuneus.label -i $subj/bullseye_rep/$hemi.posteriorcingulate.label -i $subj/bullseye_rep/$hemi.isthmuscingulate.label -o $subj/bullseye_rep/$hemi.parietalc

#> Temporal
#> • Superior, Middle, and Inferior Temporal
#> • Banks of the Superior Temporal Sulcus
#> • Fusiform
#> • Transverse Temporal
#> • Ento$hemiinal
#> • Temporal Pole
#> • Parahippocampal

mri_mergelabels -i $subj/bullseye_rep/$hemi.inferiortemporal.label -i $subj/bullseye_rep/$hemi.middletemporal.label -i $subj/bullseye_rep/$hemi.entorhinal.label -i $subj/bullseye_rep/$hemi.parahippocampal.label -i $subj/bullseye_rep/$hemi.superiortemporal.label -i $subj/bullseye_rep/$hemi.transversetemporal.label -i $subj/bullseye_rep/$hemi.fusiform.label -o $subj/bullseye_rep/$hemi.temporalc

#> Occipital
#> • Lateral Occipital
#> • Lingual
#> • Cuneus
#> • Pericalcarine

mri_mergelabels -i $subj/bullseye_rep/$hemi.cuneus.label -i $subj/bullseye_rep/$hemi.lingual.label -i $subj/bullseye_rep/$hemi.pericalcarine.label -i $subj/bullseye_rep/$hemi.lateraloccipital.label -o $subj/bullseye_rep/$hemi.occipitalc


mris_label2annot --s $subj --h rh --l $subj/bullseye_rep/$hemi.frontalc --l $subj/bullseye_rep/$hemi.temporalc --l $subj/bullseye_rep/$hemi.parietalc --l $subj/bullseye_rep/$hemi.occipitalc --a myannot --ctab /data/pt_life_whm/Data/segmented_MNI1mm/UKBB_spatialWMH/myctab.ctab 

done

mri_aparc2aseg --s $subj --labelwm --hypo-as-wm --rip-unknown \
   --volmask --wmparc-dmax 20 --o $subj/bullseye_rep/wmparc.lobes.mgz \
   --annot myannot --base-offset 200
   
   
   
done

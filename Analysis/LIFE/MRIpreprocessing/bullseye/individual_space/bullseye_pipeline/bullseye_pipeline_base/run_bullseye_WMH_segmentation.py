#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May 21 10:16:37 2019
#eddy quality control
@author: fbeyer
"""

from nipype import Node, Workflow, Function
from nipype.interfaces import fsl
from nipype.interfaces.utility import IdentityInterface
import nipype.interfaces.freesurfer as fs
import nipype.interfaces.utility as util
import nipype.interfaces.io as nio    
from bullseye_pipeline import create_bullseye_pipeline
from create_flairreg_pipeline import create_flairreg_pipeline
from utils import extract_parcellation
import numpy as np 
import nibabel as nb
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import os



def create_bullseye_lesion(subjectlist):
    # workflow to extract bullseye parcellation in individual space for LIFE


    # Specify the location of the preprocessed data    
    working_dir="/data/pt_life_whm/Data/wd/" #MODIFY
    freesurfer_dir="/data/pt_life_freesurfer/freesurfer_all"
    flairdir="/data/pt_life_whm/Data/LST/"
    regdir="/data/pt_life_whm/Results/flair2MNI/bbreg/out_reg/"
    outdir="/data/pt_life_whm/Data/WMparcellations_indiv/"

    os.environ['SUBJECTS_DIR'] = freesurfer_dir

    identitynode = Node(util.IdentityInterface(fields=['subject']),
                    name='identitynode')
    identitynode.iterables = ('subject', subjectlist)

    #Main workflow
    bullseye_lesion = Workflow(name="bullseyelesion_WMHproject")
    bullseye_lesion.base_dir=working_dir
    
    #Bullseye WM segmentation part    
    bullseye=create_bullseye_pipeline()
    bullseye.inputs.inputnode.scans_dir=freesurfer_dir

    #Lesion registration and volume extraction part
    lesionreg=create_flairreg_pipeline()
    lesionreg.inputs.inputnode.freesurfer_dir=freesurfer_dir
    lesionreg.inputs.inputnode.flair_dir=flairdir

    #extract parcellation 
    extractparc=Node(interface=util.Function(input_names=['in1_file', 'in2_file', 'subject_id'], output_names=['out_file'],
                                               function=extract_parcellation), name='extractparc')
    
    #Datasink
    datasink=Node(name="datasink", interface=nio.DataSink())
    datasink.inputs.base_directory = outdir
    datasink.inputs.substitutions = [('_subject_', '')]

    bullseye_lesion.connect([
        (identitynode, bullseye, [("subject", "inputnode.subject_id")]),
        (identitynode, lesionreg,[( 'subject', 'inputnode.subject_id')]),
        (bullseye, extractparc, [( 'outputnode.out_bullseye_wmparc', 'in2_file')]),
        (lesionreg, extractparc, [('outputnode.lesion2anat', 'in1_file')]),
        (bullseye, datasink,[( 'outputnode.out_bullseye_wmparc', '@bullseye')]),
        (lesionreg, datasink,[( 'outputnode.lesion2anat', '@lesion2anat')]),
        (lesionreg, datasink,[( 'outputnode.flair2anat', '@flair2anat')]),
        (identitynode, extractparc,[( 'subject', 'subject_id')]),
        (extractparc, datasink,[( 'out_file', '@lesionparc')]),
    ])
   
    
    
    return bullseye_lesion


## Run those for WMH bullseye analysis who did not have a registration and extract cross values based on probabilistic maps
mode="cross"
df=pd.read_csv('/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Results/LIFE/bullseye/LIFE_sample_wcompletedData.csv', sep=',')
subj=df['pseudonym'].values
#without those who do not have bbregister
dat=pd.read_csv("/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Analysis/LIFE/MRIpreprocessing/bullseye/individual_space/bullseye_pipeline/bullseye_pipeline_base/rerun_with_this_pipeline.txt",
header=None)
subj_base=dat[0].values


runsubj=[x for x in subj if x in subj_base]
print("running %s subjects with new bbregister" %(len(runsubj)))

#runsubj=["74EA9755AF"]
bullseye_lesion=create_bullseye_lesion(runsubj)
bullseye_lesion.write_graph(graph2use='colored', simple_form=True)
bullseye_lesion.run()    #plugin='MultiProc', plugin_args={'n_procs' : 16}

## Rerun all who do not have bbregister registration done
#dat=pd.read_csv("/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Analysis/LIFE/MRIpreprocessing/bullseye/individual_space/bullseye_pipeline/bullseye_pipeline_base/rerun_with_this_pipeline.txt",
#header=None)

#subj=dat[0].values



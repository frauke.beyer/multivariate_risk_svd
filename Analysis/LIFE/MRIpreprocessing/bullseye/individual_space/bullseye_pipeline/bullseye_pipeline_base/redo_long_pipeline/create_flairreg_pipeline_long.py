from __future__ import division

import nipype.pipeline.engine as pe
from nipype import SelectFiles
import nipype.interfaces.utility as util
from nipype.interfaces.freesurfer import ApplyVolTransform, BBRegister
from utils import make_list

from nipype import IdentityInterface, DataSink

# from .utils import *
from utils import *

import os

def create_flairreg_pipeline():

    name='flairreg_pipeline'
    flairreg = pe.Workflow(name=name)
    
    inputnode = pe.Node(interface=IdentityInterface(fields=['subject_id', 'freesurfer_id', 'freesurfer_dir', 'flair_dir', 'reg_dir', 'work_dir', 'bullseye']), name='inputnode')


    template_flair = {"FLAIR":  "sub-{subject_id}/lmFLAIR_bl.nii.gz",
                      "LESIONbl": "sub-{subject_id}/lples_lpa_mFLAIR_bl.nii.gz",
    		      "LESIONfu":"sub-{subject_id}/lples_lpa_mFLAIR_fu.nii.gz"}
        
              

    fileselector_lesion = pe.Node(SelectFiles(template_flair), name='fileselect_lesion')



    bbreg = pe.Node(BBRegister(contrast_type='t2',
                               out_fsl_file='flair2anat.mat',
                               out_reg_file='flair2anat.dat',
                               registered_file='flair2anat_bbreg.nii.gz',
                               args='--init-coreg' #worked well, other option: --init-rr', --init-fsl caused issues for some subjects
                              
                               ),
                               name='bbregister')

    # apply transform to lesionmap
    applyreg = pe.MapNode(ApplyVolTransform(), name="applyreg", iterfield="source_file")
    applyreg.inputs.fs_target=True
    #merge the outputs for long mode
    mklist=pe.Node(interface=util.Function(input_names=['in1', 'in2'], output_names=['lesions'],
                                                    function=make_list), name='mklist')


    # outputnode
    outputnode = pe.Node(IdentityInterface(fields=[
        'lesion2anat', 'flair2anat' ]),
        name='outputnode')


    ##### CONNECTIONS #####

    flairreg.connect(inputnode        , 'subject_id',      fileselector_lesion,'subject_id')
    flairreg.connect(inputnode        , 'freesurfer_id',      bbreg, 'subject_id')
    flairreg.connect(inputnode        , 'flair_dir',      fileselector_lesion,'base_directory')
    flairreg.connect(fileselector_lesion     , 'FLAIR',         bbreg, 'source_file')
    flairreg.connect(bbreg     , 'out_reg_file',         applyreg, 'reg_file')    
    flairreg.connect(fileselector_lesion     , 'LESIONbl',         mklist, 'in1')
    flairreg.connect(fileselector_lesion     , 'LESIONfu',         mklist, 'in2')
    flairreg.connect(mklist     , 'lesions',         applyreg, 'source_file')
    flairreg.connect(applyreg     , 'transformed_file',         outputnode,  'lesion2anat') 
    flairreg.connect(bbreg     , 'registered_file',         outputnode,  'flair2anat') 

    return(flairreg)

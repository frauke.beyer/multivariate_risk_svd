from __future__ import division

import nipype.pipeline.engine as pe
from nipype import SelectFiles
import nipype.interfaces.utility as util
from nipype.interfaces.freesurfer import ApplyVolTransform, BBRegister
from nipype.interfaces.fsl import ImageStats

from nipype import IdentityInterface, DataSink

# from .utils import *
from utils import *

import os

def create_flairreg_pipeline():

    name='flairreg_pipeline'
    flairreg = pe.Workflow(name=name)
    mode="cross"
    inputnode = pe.Node(interface=IdentityInterface(fields=['subject_id', 'flair_dir', 'freesurfer_dir', 'work_dir', 'bullseye']), name='inputnode')


    template_flair = {"LESION":"sub-{subject_id}/ples_lpa_mFLAIR_bl.nii.gz", #previously used thresholded WMH maps: thr0.1_bin 
                      "FLAIR":"sub-{subject_id}/mFLAIR_bl.nii.gz"}
              

    fileselector_lesion = pe.Node(SelectFiles(template_flair), name='fileselect_lesion')
    
    
    bbreg = pe.Node(BBRegister(contrast_type='t2',
                               out_fsl_file='flair2anat.mat',
                               out_reg_file='flair2anat.dat',
                               registered_file='flair2anat_bbreg.nii.gz',
                               #init='fsl' causes problems for some subjects
                               ),
                               name='bbregister')

    # apply transform to lesionmap
    applyreg = pe.Node(ApplyVolTransform(), name="applyreg")
    applyreg.inputs.fs_target=True


    # outputnode
    outputnode = pe.Node(IdentityInterface(fields=[
        'lesion2anat', "flair2anat" ]),
        name='outputnode')


    ##### CONNECTIONS #####

    flairreg.connect(inputnode        , 'subject_id',      fileselector_lesion,'subject_id')
    flairreg.connect(inputnode        , 'flair_dir',      fileselector_lesion,'base_directory')
    flairreg.connect(inputnode        , 'freesurfer_dir',      fileselector_lesion,'subjects_dir')
    flairreg.connect(fileselector_lesion     , 'FLAIR',         bbreg, 'source_file')
    flairreg.connect(inputnode     , 'subject_id',         bbreg, 'subject_id')
    flairreg.connect(bbreg     , 'out_reg_file',         applyreg, 'reg_file')
    flairreg.connect(fileselector_lesion     , 'LESION',         applyreg, 'source_file')
    flairreg.connect(applyreg     , 'transformed_file',         outputnode,  'lesion2anat') 
    flairreg.connect(bbreg     , 'registered_file',         outputnode,  'flair2anat') 

    return(flairreg)

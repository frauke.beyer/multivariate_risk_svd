from __future__ import division

import nipype.pipeline.engine as pe
from nipype import SelectFiles
import nipype.interfaces.utility as util
from nipype.interfaces.freesurfer import ApplyVolTransform
from utils import make_list

from nipype import IdentityInterface, DataSink

# from .utils import *
from utils import *

import os

def create_flairreg_pipeline(mode="cross"):

    name='flairreg_pipeline'
    flairreg = pe.Workflow(name=name)
    
    inputnode = pe.Node(interface=IdentityInterface(fields=['subject_id', 'flair_dir', 'reg_dir', 'work_dir', 'bullseye']), name='inputnode')


    if mode=="cross":
        template_flair = {"LESION":"sub-{subject_id}/ples_lpa_mFLAIR_bl.nii.gz"} #old: previously _thr0.1_bin binarized WMH probability map
        template_reg = {"bbreg": "{subject_id}/flair2anat.dat"}


    else:
        template_flair = {"LESIONbl": "sub-{subject_id}/lples_lpa_mFLAIR_bl.nii.gz",
                          "LESIONfu":"sub-{subject_id}/lples_lpa_mFLAIR_fu.nii.gz"}
        template_reg = {"bbreg": "{subject_id}/flair2anat.dat"}
        
              

    fileselector_lesion = pe.Node(SelectFiles(template_flair), name='fileselect_lesion')

    fileselector_reg = pe.Node(SelectFiles(template_reg), name='fileselect_reg')


    # apply transform to lesionmap
    if mode == "cross":
        applyreg = pe.Node(ApplyVolTransform(), name="applyreg")
        applyreg.inputs.fs_target=True
    else: 
        applyreg = pe.MapNode(ApplyVolTransform(), name="applyreg", iterfield="source_file")
        applyreg.inputs.fs_target=True

        #merge the outputs for long mode
        mklist=pe.Node(interface=util.Function(input_names=['in1', 'in2'], output_names=['lesions'],
                                                    function=make_list), name='mklist')


    # outputnode
    outputnode = pe.Node(IdentityInterface(fields=[
        'lesion2anat' ]),
        name='outputnode')


    ##### CONNECTIONS #####

    flairreg.connect(inputnode        , 'subject_id',      fileselector_lesion,'subject_id')
    flairreg.connect(inputnode        , 'flair_dir',      fileselector_lesion,'base_directory')
    flairreg.connect(inputnode        , 'subject_id',      fileselector_reg,'subject_id')
    flairreg.connect(inputnode        , 'reg_dir',      fileselector_reg,'base_directory')

    if mode=="cross":
        flairreg.connect(fileselector_lesion     , 'LESION',         applyreg, 'source_file')
        flairreg.connect(fileselector_reg     , 'bbreg',         applyreg, 'reg_file')
        flairreg.connect(applyreg     , 'transformed_file',         outputnode,  'lesion2anat') 
    else: 
        flairreg.connect(fileselector_lesion     , 'LESIONbl',         mklist, 'in1')
        flairreg.connect(fileselector_lesion     , 'LESIONfu',         mklist, 'in2')
        flairreg.connect(mklist     , 'lesions',         applyreg, 'source_file')
        flairreg.connect(fileselector_reg     , 'bbreg',         applyreg, 'reg_file')
        flairreg.connect(applyreg     , 'transformed_file',         outputnode,  'lesion2anat') 

    return(flairreg)

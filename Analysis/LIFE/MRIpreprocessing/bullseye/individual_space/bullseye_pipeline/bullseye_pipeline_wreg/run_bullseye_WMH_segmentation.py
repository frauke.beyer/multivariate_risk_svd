#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue May 21 10:16:37 2019
#eddy quality control
@author: fbeyer
"""

from nipype import Node, Workflow, Function, MapNode
from nipype.interfaces import fsl
from nipype.interfaces.utility import IdentityInterface
import nipype.interfaces.freesurfer as fs
import nipype.interfaces.utility as util
import nipype.interfaces.io as nio    
from bullseye_pipeline import create_bullseye_pipeline
from create_flairreg_pipeline import create_flairreg_pipeline
from utils import extract_parcellation, fs_temp_name
import numpy as np 
import nibabel as nb
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import os



def create_bullseye_lesion(subjectlist, mode):
    # workflow to extract bullseye parcellation in individual space for LIFE


    # Specify the location of the preprocessed data    
    working_dir="/data/pt_life_whm/Data/wd/" #MODIFY
    freesurfer_dir="/data/pt_life_freesurfer/freesurfer_all"
    flairdir="/data/pt_life_whm/Data/LST/"
    regdir="/data/pt_life_whm/Results/flair2MNI/bbreg/out_reg/"
    outdir="/data/pt_life_whm/Data/WMparcellations_indiv/"

    os.environ['SUBJECTS_DIR'] = freesurfer_dir

    identitynode = Node(util.IdentityInterface(fields=['subject']),
                    name='identitynode')
    identitynode.iterables = ('subject', subjectlist)

    #Main workflow
    if mode!="cross":
        fs_temp=Node(util.Function(input_names=['input_id'],
                            output_names=['output_id'],
                            function = fs_temp_name), name="fs_temp")

    bullseye_lesion = Workflow(name="bullseyelesion") #_long407 suffix only for those who are in BE sample & long subjects and are rerun separately
    bullseye_lesion.base_dir=working_dir
    
    #Bullseye WM segmentation part    
    bullseye=create_bullseye_pipeline()
    bullseye.inputs.inputnode.scans_dir=freesurfer_dir

    #Lesion registration and volume extraction part
    lesionreg=create_flairreg_pipeline(mode=mode)
    lesionreg.inputs.inputnode.reg_dir=regdir
    lesionreg.inputs.inputnode.flair_dir=flairdir

    #extract parcellation 
    if mode=="cross":
        extractparc=Node(interface=util.Function(input_names=['in1_file', 'in2_file', 'subject_id', 'option'], output_names=['out_file'],
                                               function=extract_parcellation), name='extractparc')
        extractparc.inputs.option="new"
    else:
        extractparc=MapNode(interface=util.Function(input_names=['in1_file', 'in2_file', 'subject_id','option'], output_names=['out_file'],
                                               function=extract_parcellation), iterfield=["in1_file"], name='extractparc')
        extractparc.inputs.option="old"
                                               
    
    #Datasink
    datasink=Node(name="datasink", interface=nio.DataSink())
    datasink.inputs.base_directory = outdir
    datasink.inputs.substitutions = [('_subject_', '')]



    if mode=="cross":
        bullseye_lesion.connect(identitynode, 'subject', bullseye,'inputnode.subject_id')
        
    else:
        bullseye_lesion.connect(identitynode, 'subject', fs_temp,'input_id')
        bullseye_lesion.connect(fs_temp, 'output_id', bullseye,'inputnode.subject_id')
    bullseye_lesion.connect(identitynode, 'subject', lesionreg,'inputnode.subject_id')          
        
    bullseye_lesion.connect([
        (bullseye, extractparc, [( 'outputnode.out_bullseye_wmparc', 'in2_file')]),
        (lesionreg, extractparc, [('outputnode.lesion2anat', 'in1_file')]),
        (bullseye, datasink,[( 'outputnode.out_bullseye_wmparc', '@bullseye')]),
        (lesionreg, datasink,[( 'outputnode.lesion2anat', '@lesion2anat')]),
        (identitynode, extractparc,[( 'subject', 'subject_id')]),
        (extractparc, datasink,[( 'out_file', '@lesionparc')]),
    ])
   
    
    
    return bullseye_lesion




## For WMH bullseye analysis use participants included in BE sample & with long data using old way (option="old")(N=407) -> wd = /data/pt_life_whm/Data/wd/bullseyelesion_long407
mode="long"
df=pd.read_csv('/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Results/LIFE/bullseye/LIFE_sample_wcompletedData.csv', sep=',')
df=df[df["MR_y_n_fu"]==1]
subj=df['pseudonym'].values
#without those who do not have bbregister & were run elsewhere
dat=pd.read_csv("/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Analysis/LIFE/MRIpreprocessing/bullseye/individual_space/bullseye_pipeline/bullseye_pipeline_base/rerun_with_this_pipeline.txt",
header=None)
subj_base=dat[0].values
subj_base_w=np.append(subj_base,'1F692731FF')


runsubj=[x for x in subj if x not in subj_base_w ]

runsubj=["0B8D78060D", "057E23BE66", "1991FE3DFC", "25AECB1C43", "4365FBD38B", "230DE6F5BE"] #'1FCACC7C95' from N=407 BE longitudinal sample

print("N=%i are rerun", len(runsubj))

bullseye_lesion=create_bullseye_lesion(runsubj, mode)
bullseye_lesion.write_graph(graph2use='colored', simple_form=True)
bullseye_lesion.run(plugin='MultiProc', plugin_args={'n_procs' : 16})    



### Previously extracted data from those with either cross or long acquisitions (based on thr 0.1 maps or binary maps from longitudinal stream). (option="old")
df=pd.read_csv('/data/pt_life_whm/Results/Tables/crossvols_thr0.1_w_pseudonym_qa.csv', sep=',')
df_fs=pd.read_csv("/data/gh_gr_agingandobesity_share/life_shared/Data/Preprocessed/derivatives/FreeSurfer/QA_followup/freesurfer_qa_baseline_and_followup.csv")

#Merge
merged_df=pd.merge(
     df,
     df_fs,
     how="left",
     left_on="pseudonym",
     right_on="pseudonym")

if mode=="cross":
    ## Only process those with mode cross who have only one scan & who have LST run
    df=merged_df[np.column_stack((merged_df["MR_y_n_fu"]==0,pd.notnull(merged_df["vol"]))).all(axis=1)]
else:
    ## Only process those with mode long who have both scans & who have LST run
    df=merged_df[merged_df["MR_y_n_fu"]==1]

##Exclude everyone with quality issues in FS & with baseline MRI issues or brain pathologies
res = np.column_stack((df["BL_usable"]!="0",df["qa_check"]!=1,df["qa_check"]!=3)).all(axis=1)
df_quality_ok=df[res]
subj=df_quality_ok['pseudonym'].values
# #rerun DDA016C0C9 cross
# #subj=['DDA016C0C9']

#rerun those who had wrong registrations 
#for cross: 
#res=pd.read_csv("/data/pt_life_whm/Analysis/preprocessing/qa_workflow/bbregister_QA/rerun_subjects.txt", header=None)
#subj=res[0].values

#rerun those who had wrong registrations 
#for long: 
#df=pd.read_csv('/data/pt_life_whm/Analysis/preprocessing/register_to_MNI/rerun_long.txt', header=None)
#subj=df[0].values

# bullseye WM parcellation & WML load extraction pipelines
Pipelines for extracting WML after creating a _bullseye parcellation_ of the cerebral whiter matter using part of the FreeSurfer output and commands.

- pipeline `bullseye_pipeline_wreg/` requires the existence of a BBregister registration **finished** to bring FLAIR into FreeSurfer subject space (it has two options "cross" and "long" referring to the input lesion image (from LST cross oder long) and the FreeSurfer image used for registration between FLAIR and T1. In case of cross, the crosssectional FS output was used, in case of long the first timepoint but in longitudinal space is used. For LIFE, this was used for most of the subjects.
- pipeline `bullseye_pipeline_base` performs the BBregister registration in the workflow. This can be used for completely new data. It requires a FreeSurfer folder, a FLAIR image and an aligned binarized lesion map. This was only used for a subset where there were previously errors with BBregister.

### ATTENTION -- there was an error --
Pipeline `bullseye_pipeline_wreg/` was first run with FreeSurfer as input for long individuals, and then in a second step with baseline timepoint for all from the WMH bullseye project. Therefore, wd is not uptodate with long processing right now, but reflects the cross approach! Use results from /data/pt_life_whm/Data/WMparcellations_indiv/*/. 

# WMH spatial pattern study:
# Long subjects in N=1066 (N=407) 
-> baseline timepoint was rerun cross bbregister + summing up probabilities, results are in /data/pt_life_whm/Data/WMparcellations_indiv/blwmh/

-> longitudinal data of those subjects is still in: but bullseye_wmparc file DOES not fit, because it was created from the cross-FreeSurfer for those.
/data/pt_life_whm/Data/WMparcellations_indiv/8641D6BE0B/_extractparc1
/data/pt_life_whm/Data/WMparcellations_indiv/8641D6BE0B/_extractparc0
/data/pt_life_whm/Data/WMparcellations_indiv/8641D6BE0B/_applyreg1
/data/pt_life_whm/Data/WMparcellations_indiv/8641D6BE0B/_applyreg0

# Cross subjects in N=1066 (N=659) are ok (19 of those were anyhow already run with new bbregister, with bullseye_base pipeline)
-> they were just rerun with the option="new" in extract_parcellation where WMH volume is defined as sum of probabilities

# CVR-WMH RR study (N=596 -> 591 with usable FreeSurfer (also some younger than 65 y))
N=300 -> are not in the N=1066 Bullseye sample and have therefore not been checked!
N=291 -> were in the Bullseye sample and have been checked, but due to above issues of not correct working directory, bullseye_wm mask and coregistered FLAIR cannot be checked.


### To update.
- started all baseline "Bullseye" for non-binarized data again
- in "bullseye_reg" working directory
- output to start with: blwmh.

### Selection of participants for WMH bullseye project
- use participants previously selected for the project (```/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Results/LIFE/bullseye/LIFE_sample_wcompletedData.csv``` )
- use baseline probability maps of WMH only, regardless of whether participants have followup data or not. 
- divide into those who had a bbreg registration (run with `bullseye_pipeline_wreg/`) and those who did not have one (rerun those with `bullseye_pipeline_base`)
- results are in ```res_HSB.txt``` 

### General extraction for all
- divide participants into those who have a second MRI (long) and those who don't (cross). **this is crucial to select the correct transform**
- for cross subjets, use thresholded 0.1 probability maps, for long use binary probability maps in intermediate space.. 
- divide into those who had a bbreg registration (run with `bullseye_pipeline_wreg/`) and those who did not have one (rerun those with `bullseye_pipeline_base`)
- results are in ```res.txt``` 

### To run the pipelines:
- have FreeSurfer version 6.0.0p1 or higher loaded
- Python 2 environment with nipype, nibabel (```agewell_nip1.2```)
- change into the respective directory 
- define subjects to run it in ll. 83 ff of `run_bullseye_WMH_segmentation.py`
- type `python run_bullseye_WMH_segmentation.py` to execute

Both pipelines consist of three steps: 
1. the bullseye WM parcellation (`bullseye_pipeline.py` entirely taken from [this github repositoy](https://github.com/gsanroma/bullseye_pipeline/tree/master/bullseye_pipeline))
It provides an (anatomy-independent) spatial localization based on an radial component (ie, lobes) and a depth component.
It can be used to obtain region-specific quantification of white matter parameters (eg, a similar approach has been used to quantify regional white matter hyperintensity load in [this](https://link.springer.com/chapter/10.1007/978-3-030-00919-9_10) and [this](https://doi.org/10.1016/j.neurad.2017.10.001) papers). 

The internals of the process are explained in [this](https://gsanroma.github.io/posts/2019/06/bullseye-parcellation/) blog post.

- the _bullseye parcellation_ is the intersection of a _lobar_ parcellation and a _depth_ parcellation
- the _lobar parcellation_ consists of 4 lobes per hemisphere (_frontal_, _parietal_, _temporal_ and _occipital_) + 1 consisting of the basal ganglia and thalamus as an additional region: (4*2) + 1 = 9 lobes
- the _depth parcellation_ consists of 4 equidistant parcels spanning from the surface of the ventricles to the internal surface of the cortex


### Required FreeSurfer input
However, not all the FreeSurfer output is required.
The following is the _mandatory_ portion of FreeSurfer data that is required by the pipeline:

- `scansdir`  
  - `subject-id1`  
    - `mri`  
      - `aseg.mgz`  
      - `ribbon.mgz`  
    - `label`  
      - `lh.aparc.annot`  
      - `rh.aparc.annot` 
    - `surf`  
      - `lh.white`  
      - `rh.white`  
      - `lh.pial`  
      - `rh.pial`  
  - `subject-id2`  
  ...

2. the coregistration of lesion map to FreeSurfer space (`create_flairreg_pipeline.py`)
**old version**: baseline only WMH probability maps are binarized at 0.1, coregistered to FreeSurfer space and binarized again at 0.1 for counting number of voxels in each parcel. Binary maps from longitudinal pipeline are also thresholded at 0.1 before extrating. 
**latest version (11/23)**: WMH probability maps are coregistered to FreeSurfer space and summed up in the respective parcels.

3. the extraction of bullseye parcellated WML volumes (in `utils.py`)
The bullseye parcellation is masked by the lesion map, a histogram is created and unassigned lesion volume + 36 WM parcellation volumes are extracted and saved in text file.
Order:
    - undefined
    - 51, 52, 53, 54 : BG + depth
    - 111, 112, 113, 114: lh frontal + depth
    - 121, 122, 123, 124: lh occipital + depth
    - 131, 132, 133, 134: lh temporal + depth
    - 141, 142, 143, 144: lh parietal + depth   
    - 211, 212, 213, 214: rh frontal + depth
    - 221, 222, 223, 224: rh occipital + depth
    - 231, 232, 233, 234: rh temporal + depth
    - 241, 242, 243, 244: rh parietal + depth
    
### Output directory structure
After execution of the pipeline a directory `output_dir` is created with the following structure:

- `output_dir`  
  - `subject-id1`  
    - `bullseye_wmparc.nii.gz`  
    - `ples_lpa_mFLAIR_bl_thr0.1_bin_warped.nii.gz`  
    - `ples_lpa_mFLAIR_bl_warped.nii.gz`  
    - `res.txt`
  - `subject-id2`  
  ...
  
containing, respectively, the final bullseye parcellation, the WMH map (thresholded and binarized lesion map) and the results file (res.txt refers to thresholded WMH maps, res_HSB.txt to the sum of probabilities)  



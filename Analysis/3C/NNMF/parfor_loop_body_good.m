function parfor_loop_body_good

data = rand(5,5);
means = zeros(1,5);
parfor i = 1:5
    % Call a function instead
    means(i) = computeMeans(data(:,i));
end
disp(means);

% This function now contains the body
% of the parfor-loop
function means = computeMeans(data)
y.mean = mean(data);
means = y.mean;
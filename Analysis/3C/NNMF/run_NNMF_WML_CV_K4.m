addpath('/scratch/beyer/multivariate_risk_svd/Analysis/NNMF/brainparts')

%run NNMF for determined number of components and calculate reconstruction error + spatial coverage

outputdir='/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/K4_CV5_brainmask_3mm/';

%info on MRI data
%load('/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/input_data/WML_usable_min17_3mm.mat');
%maskfile='/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/input_data/WML_mask_min17_3mm.nii';

%for brainmask
load('/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/input_data/WML_usable_brainmask_3mm.mat')
maskfile='/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/input_data/mni_icbm152_t1_tal_nlin_asym_09c_mask_3mm.nii.gz';

freqmask = niftiread(maskfile);
mask_lesionfreq=logical(freqmask); 
mask=mask_lesionfreq;


%info for NNMF algorithm
K=4; %(optimal number of components determined for minmask)
info = niftiinfo(maskfile);
X=usable_WML_r; 
w0='';
initMeth=1;
max_iter='';
tol='';
iter0='';
save_step='';

%info for CV
dimV=size(X);
nfold=5;

s = RandStream('mt19937ar') %don't fix seed because partition is saved
c = cvpartition(dimV(2),'KFold',nfold,s);
save(strcat(outputdir,'/partition.mat'),'c')
%load(strcat(outputdir,'/partition.mat'),'c')

%some empty files to fill
perCov=zeros(K-1,nfold);
RE_train=zeros(nfold,1);
RE_test=zeros(nfold,1);
L_train=zeros(max(c.TrainSize),4, nfold);
L_test=zeros(max(c.TestSize),4, nfold);

for j=1:nfold
    fprintf("fold %i\n", j)
    idxTrain=training(c,j);
    XTrain=X(:,idxTrain);
    idxTest=test(c,j);
    XTest=X(:,idxTest);
    
    fprintf("components: %i\n", K)
    outputdir_tmp=strcat(outputdir, sprintf('/f%i', j));

    if (~isfile(strcat(outputdir_tmp,'/res.mat')))
        [W H]=opnmf_mem(XTrain, K, w0, initMeth, max_iter, tol, iter0, save_step, outputdir_tmp);
        save(strcat(outputdir_tmp,'/res.mat'),'W','H')
        
        % calculate reconstruction error for component:
        RE_train(j)=norm(XTrain-W*(W'*XTrain), 'fro');
        L_train(1:c.TrainSize(j),:,j)=(W'*XTrain)';
    else
        load(strcat(outputdir_tmp,'/res.mat'),'W','H')
    end


    %save components:
    for ind=1:K

        if (~isfile(strcat(outputdir_tmp, sprintf('/C%i.nii', ind))))
            C=zeros(size(mask));
            C(mask)=W(:,ind);
            niftiwrite(single(C),  strcat(outputdir_tmp, sprintf('/C%i.nii', ind)), info)
        end    
        % calculate spatial coverage (with close-to-zero value as numeric 0 is not reached)
        perCov(ind,j)=sum(W(:,ind)>1*10^-15)/sum(mask(:));
    end

    % apply components to test dataset
    RE_test(j)=norm(XTest-W*(W'*XTest), 'fro');
    
    %save loadings for test participants in this fold
    L_test(1:c.TestSize(j),:,j)=(W'*XTest)';

end

save(strcat(outputdir,'/res_perCov_RE_K2_16_CV10.mat'),'perCov','RE_train', 'RE_test', 'L_train', 'L_test')




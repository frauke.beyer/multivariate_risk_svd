#spatial correlation of LIFE/3C decompositions

basedir="/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Analysis/LIFE/"
basedir_3C="/data/pt_life_whm/Results/3C/"
basedir_LIFE="/data/pt_life_whm/Results/NNMF/K4_binds_min23_fullsample/"



for j in {1..4} #components
do
#fslroi $basedir_LIFE/C$j.nii $basedir_LIFE/C${j}_padded.nii 4 52 4 63 4 52
#gunzip $basedir_LIFE/C${j}_padded.nii.gz
echo "LIFE" $j >> $basedir/res.txt
for k in {1..4} #components
do
echo "3C" $k >> $basedir/res.txt
echo "3ddot -dodot $basedir_3C/C$k.nii $basedir_LIFE/C${j}_padded.nii"
3ddot -dodot $basedir_3C/C$k.nii $basedir_LIFE/C${j}_padded.nii >> $basedir/res.txt
done
done

#after having determined the optimal number of components, see how well they overlap over the different folds.
#calculate spatial correlation of maps from different folds

module load afni/17

#basedir=/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/CV_minmask_3mm;
basedir=/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/K4_CV5_brainmask_3mm/
for j in {1..4} #components
do
echo $j >> $basedir/res.txt
for i in {1..5} #folds
do
if [ $i -eq 1 ];
   then
   for k in {2..5} #folds
   do
   echo $i $k >> $basedir/res.txt
   echo "3ddot -dodot $basedir/f$i/C$j.nii $basedir/f$k/C$j.nii"
   3ddot -dodot $basedir/f$i/C$j.nii $basedir/f$k/C$j.nii >> $basedir/res.txt
   done
elif [ $i -eq 2 ];
   then
   for k in {3..5} #folds
   do
   echo $i $k >> $basedir/res.txt
   echo "3ddot -dodot $basedir/f$i/C$j.nii $basedir/f$k/C$j.nii"
   3ddot -dodot $basedir/f$i/C$j.nii $basedir/f$k/C$j.nii >> $basedir/res.txt
   done
elif [ $i -eq 3 ];
    then
    for k in {4..5} #folds
    do
    echo $i $k >> $basedir/res.txt
    echo "3ddot -dodot $basedir/f$i/C$j.nii $basedir/f$k/C$j.nii"
    3ddot -dodot $basedir/f$i/C$j.nii $basedir/f$k/C$j.nii >> $basedir/res.txt
    done
elif [ $i -eq 4 ];
    then
    k=5
    echo $i $k >> $basedir/res.txt
    echo "3ddot -dodot $basedir/f$i/C$j.nii $basedir/f$k/C$j.nii"
    3ddot -dodot $basedir/f$i/C$j.nii $basedir/f$k/C$j.nii >> $basedir/res.txt
fi
done
done
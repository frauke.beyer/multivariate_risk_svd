addpath('/scratch/beyer/multivariate_risk_svd/Analysis/NNMF/brainparts')

%load results from fullsample NNMF
load('/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/K212_fullsample_minmask_3mm/results_fullsample.mat');

%RE
plot(2:12,RE_train(:,1),'black','LineWidth',2)
xlabel('# of components')
ylabel('reconstruction error')
exportgraphics(fig,'/scratch/beyer/multivariate_risk_svd/Results/Figures/K212_fullsample_minmask_3mm_REkneeplot.jpg')

plot(2:12,diff(RE_train(:,1))) 
knee_pt(RE_train(:,1)) %4 components is confirmed by the knee_pt function.

%spatial Cov
plot(2:12, perCov(:,1)) 
plot(2:11, diff(perCov(:,1))) %largest change of spatial coverage 
%from component 3 to 4, after that less strong increases, as in Habes et
%al.

%load results from CV run
load('/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/K4_CV5_minmask_3mm/res_perCov_RE_K4_CV5.mat')
load('/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/K4_CV5_minmask_3mm/partition.mat')

%check the correlation of loadings for different components for subjects
L_tr_all=zeros(c.NumObservations,20);

for i=0:4    
    L_tr_all(c.training(i+1),(i*4)+1:(i*4)+4)=L_train(1:c.TrainSize(i+1),:,i+1);
end

%calculate average and sd for components
L_tr_C1=L_tr_all(:,[1,5,9,13,17]); %component 1
L_tr_C1(L_tr_C1==0)=NaN;
L_tr_C1_mean=mean(L_tr_C1,2,'omitnan');
L_tr_C1_sd=std(L_tr_C1','omitnan');
L_tr_C1_sd=L_tr_C1_sd';

e=errorbar(L_tr_C1_mean,L_tr_C1_sd);
e.LineStyle = 'none';
xlabel('# of subjects')
ylabel('C1 loading with errorbar')

%only possible in 2020a later
%exportgraphics(e,'/scratch/beyer/multivariate_risk_svd/Results/Figures/K4_CV5_minmask_3mm_C1loadings.jpg')

L_tr_C2=L_tr_all(:,[2,6,10,14,18]); %component 2

%addpath('/scratch/beyer/multivariate_risk_svd/Analysis/NNMF/brainparts')
addpath(genpath("/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd"))

%run NNMF for different number of components and calculate reconstruction error + spatial coverage

%info on MRI data
%load('/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/input_data/WML_usable_min17_3mm.mat');
%maskname='/scratch/beyer/3C/BIDS/derivatives/WML/NNMF/input_data/WML_mask_min17_3mm.nii';

load('/data/pt_life_whm/Results/NNMF/input_data/WML_usable_binds_min23_3mm.mat');
X=usable_WML_r;
maskname='/data/pt_life_whm/Results/NNMF/input_data/WML_brainmask_binds_min23_3mm.nii';

%mask = logical(niftiread(maskname));

K=12;

%some empty files to fill
perCov=zeros(K-1);
RE_train=zeros(K-1);

parfor i=2:K
    
    
    fprintf("components: %i\n", i)

    % calculate reconstruction error for component:
    [RE_train(i-1), perCov(i-1)]=runNNMF(X,i);
    
end


% This function now contains the body
% of the parfor-loop
function [RE, perCov] = runNNMF(data, K)
outputdir='/data/pt_life_whm/Results/NNMF/min23_binds_fullsample';
w0='';
initMeth=1;
max_iter='';
tol='';
iter0='';
save_step='';
[W H]=opnmf_mem(data, K, w0, initMeth, max_iter, tol, iter0, save_step, outputdir);
RE=norm(data-W*(W'*data), 'fro');
perCov=0;
for ind=1:K
    perCov=perCov+sum(W(:,ind)>1*10^-15);
end
end




addpath('/scratch/beyer/multivariate_risk_svd/Analysis/NNMF/brainparts')

V = niftiread("/scratch/beyer/3C/BIDS/derivatives/WML/merged/mergedfile_N1781.nii.gz");
dimV=size(V);

% prepare input data: use only WM voxels (leave this out for the moment as a lot of periventricular lesions would be omitted like this)
%mask_WM=niftiread("/scratch/beyer/3C/mni_icbm152_nlin_asym_09c/mni_icbm152_wm_tal_nlin_asym_09c_15mm_sub005_applyxsqform_thr0.8_bin.nii.gz");

%repmask_WM = logical(repmat(reshape(mask_WM, size(mask_WM,1), size(mask_WM,2), size(mask_WM,3),1), 1, 1, 1, dimV(4)));
%WML_WM=V(repmask_WM); %119622 voxels in WM mask


%and those with a lesion in at least 1% (=17) participants.
%info = niftiinfo("/scratch/beyer/3C/mni_icbm152_nlin_asym_09c/mni_icbm152_wm_tal_nlin_asym_09c_15mm_sub005_applyxsqform_thr0.8_bin.nii.gz");
%niftiwrite(sumV,  "/scratch/beyer/3C/BIDS/derivatives/WML/merged/sum_mergedfile_N1781.nii", info)

%imwrite(sumV(:,:,40), hot, 'slice40.png')

sumV=sum(V, 4);
mask_lesionfreq=sumV>=17; %116521 voxels in lesionfreq mask

repmask = repmat(reshape(mask_lesionfreq, size(mask_lesionfreq,1), size(mask_lesionfreq,2), size(mask_lesionfreq,3),1), 1, 1, 1, dimV(4));

WML_usable=V(repmask);

%reshape to have number of subjects as second dimension
WML_usable_r=reshape(WML_usable, 116521, 1781);

%replace values smaller than 0 with 0
WML_usable_r(WML_usable_r<0)=0;
WML_usable_r=double(WML_usable_r);

%form of data needed for NNMF: D x N (image dimension x individuals)

%try to run NNMF
K=4; %(as in neurology paper)
outputdir=char("/scratch/beyer/3C/BIDS/derivatives/WML/NNMF");
X=WML_usable_r;
w0='';
initMeth=1;
max_iter='';
tol='';
iter0='';
save_step='';

[W,H]=opnmf_mem(X, K, w0, initMeth, max_iter, tol, iter0, save_step, outputdir);
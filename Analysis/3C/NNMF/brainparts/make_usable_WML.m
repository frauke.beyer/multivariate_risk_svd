function WML_usable_r=make_usable_WML(V, mask)
dimV=size(V);
N=dimV(4);
repmask = single(repmat(reshape(mask, size(mask,1), size(mask,2), size(mask,3),1), 1, 1, 1, N));
repmask=logical(repmask);
WML_usable=V(repmask);
D=size(WML_usable)/N;

%reshape to have number of subjects as second dimension
WML_usable_r=reshape(WML_usable, D(1), N);

%replace values smaller than 0 with 0
WML_usable_r(WML_usable_r<0)=0;
WML_usable_r=double(WML_usable_r);

%replace NaN values
if any(any(~isfinite(WML_usable_r)))
    WML_usable_r(~isfinite(WML_usable_r))=0;
end


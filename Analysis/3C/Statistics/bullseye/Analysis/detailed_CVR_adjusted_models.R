library(dplyr)
library(ggplot2)
library(haven)
library(BayesFactor)
library(psych)

#library(geomtextpath )

library(tidyr)
library(stringr)
library(psych)
library(mice)
library(performance)
library(forcats)
library(haven)
library(lmerTest)
library(lme4)
#library(readxl)
#library(naniar)
library(ggeffects)
library(cowplot)
library(BayesFactor)

setwd("R:/ETUDES/SHIVA_FRAUKE/Frauke/Analysis")

file="R:/ETUDES/SHIVA_FRAUKE/Frauke/Data/OwnDataSets/CVRdata_N1699_HTA_BMI.csv"

for (model in c("3C_on3C_7comp_HTA_bmi_TIV_prob",
  "3C_on3C_7comp_sbp_bmi_TIV_prob",
  "3C_on3C_7comp_dbp_bmi_TIV_prob"
)){
  #runs models for 
  if (model=="3C_on3C_7comp_HTA_bmi_TIV_prob"){
    pred="age0 + sexeb + EstimatedTotalIntraCranialVol + hta0_1 + bmi0" #
  }else if (model=="3C_on3C_7comp_sbp_bmi_TIV_prob"){
    pred="age0 + sexeb + EstimatedTotalIntraCranialVol + pas0 + bmi0"
  }else if (model=="3C_on3C_7comp_dbp_bmi_TIV_prob"){
    pred="age0 + sexeb + EstimatedTotalIntraCranialVol + pad0 + bmi0"
  }

### CVRANALYSIS of 7-PCA components from 3C in 3C
completedData=read.csv(file)


#reintroduce factors
completedData= completedData %>% mutate(antihyp0b=as.factor(antihyp0b),
                                        hta0_1=as.factor(hta0_1),
                                        sexeb=as.factor(sexeb))

#Models for components
source("funcs/plotCVRmodels.R")
source("funcs/runCVRmodels.R")






dir.create(paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/",model))
NE=c("age0", "sexeb", "EstimatedTotalIntraCranialVol")

nfactors=7
res=list()
for (i in seq(1:nfactors)){
  res[[i]]=runmodels(paste0("scores.TC",i), pred=pred, NE=NE, completedData)
  
  
  # p1=plotmodels("age0", comp=c("male","1","1"), paste0("scores.TC",i), completedData, res[[i]])
  # p2=plotmodels("sexeb", comp=c("male","1","1"), paste0("scores.TC",i), completedData, res[[i]])
  # p3=plotmodels("bmi0", comp=c("male","1","1"), paste0("scores.TC",i), completedData, res[[i]])
  # p4=plotmodels("apoe4", comp=c("male","1","1"), paste0("scores.TC",i), completedData, res[[i]])
  # 
  # if (length(strsplit(pred, '\\+')[[1]])==7){
  # p5=plotmodels("pas0", comp=c("female","1","1"), paste0("scores.TC",i), completedData, res[[i]])
  # p6=plotmodels("antihyp0b", comp=c("female","1","1"), paste0("scores.TC",i), completedData, res[[i]])
  # gridplot=gridExtra::grid.arrange(p1, p2, p5, p6, p3,p4,
  #                                  ncol = 5)
  # ggsave(paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/",model,"/res_",model,"_TC_",i,".png"),
  #        gridplot, dpi=300, width = 40, height=20, units="cm")
  # 
  # }else{
  # p5=plotmodels("hta0_1", comp=c("female","1","1"), paste0("scores.TC",i), completedData, res[[i]])
  # gridplot=gridExtra::grid.arrange(p1, p2, p5, p3,p4,
  #                                  ncol = 5)
  # ggsave(paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/",model,"/res_",model,"_TC_",i,".png"),
  #        gridplot, dpi=300, width = 40, height=20, units="cm")
  #}
  #jpeg(file=paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/",model,"res",i,"_avplot.jpeg"),
  #     width=700, height=700, quality=100)
  #car::avPlots(res[[i]][[1]])
  #dev.off()



  write.csv(round(res[[i]]$coeffs,4),
             paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/",model,"/res_",model,"_TC_",i,".csv"))
}

save(res, file=paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/",model,"/res_",model, ".Rdata"))

#All components in one model
model1=paste0(model, "_allComp")
dir.create(paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/",model1))

nfactors=7
res=list()
for (i in seq(1:nfactors)){
  seq=seq(1:nfactors)
  cc=unlist(lapply(seq[-i], function(y) paste("scores.TC", y, sep = "")))
  conf=paste0(cc, sep='+',collapse='')
  NE=c("age0", "sexeb", "EstimatedTotalIntraCranialVol",cc)
  predcc=paste0(conf,pred)
  res[[i]]=runmodels(paste0("scores.TC",i), pred=predcc, NE=NE, completedData)
  write.csv(res[[i]]$coeffs,
            paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/",model1,"/res_",model1,"_TC_",i,".csv"))
  
}
save(res, file=paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/",model1,"/res_",model1, ".Rdata"))


  
#Model for overall WMH load
model2=paste0(model, "_asinhWMH")
dir.create(paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/",model2))

NE=c("age0", "sexeb", "EstimatedTotalIntraCranialVol")
res=runmodels("asinhWMH", pred=pred, NE=NE, completedData)
# p61=plotmodels("Age_all",comp="female","asinhWMH", completedData, res)
# p62=plotmodels("sex",comp="female", "asinhWMH", completedData, res)
# p63=plotmodels("WHR",comp="female", "asinhWMH", completedData, res)
# p64=plotmodels("APO",comp="female", "asinhWMH", completedData, res)
# p65=plotmodels("HTA",comp="female", "asinhWMH", completedData, res)
# gridplot6=gridExtra::grid.arrange(
#   p61, p62, p65,p63,p64,
#   ncol = 5)
# 
# ggsave(paste0("../Results/bullseye/figures/asinhWMH_N1735_TIV.png"), 
#        gridplot6, dpi=300, width = 40, height=20, units="cm")
 write.csv(res$coeffs,
           paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/",model2,"/res_",model2,".csv"))

save(res, file=paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/",model2,"/res_",model2, ".Rdata"))
}

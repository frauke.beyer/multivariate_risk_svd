library(dplyr)
library(ggplot2)
library(haven)
library(psych)
library(tidyr)
library(stringr)
library(psych)
library(haven)
library(lme4)
library(lcmm)

setwd("~/for_Frauke/Analysis")

##load longitudinal data
plotcog=read.csv("../Data/OwnDataSets/Cogdata_N1668.csv")

#t =plotcog %>% group_by(time) %>%  summarise(ts_mean=mean(ts_y, na.rm=T))

#scale some of the data so that convergence is easier
plotcog = plotcog %>% mutate(age_c=(plotcog$age0-mean(plotcog$age0))/10)
plotcog = plotcog %>% mutate(TMTA_rev=-TMTA)
plotcog = plotcog %>% mutate(TMTB_rev=-TMTB)
plotcog = plotcog %>% mutate(ts_y=plotcog$ts/(10)) #to avoid numerical issues
plotcog$sexeb=as.factor(plotcog$sexeb)
plotcog$TIME1=0 #indicator of first timepoint for measurement effects (for everything except TMTB).
plotcog$TIME1_TMT=0 
plotcog[plotcog$time=="T1","TIME1"]=1
plotcog[plotcog$time=="T2","TIME1_TMT"]=1
plotcog$DIPNIV0= as.factor(plotcog$DIPNIV0)

# All models
allsc=paste0(paste0('scores.TC',seq(1:7)), sep='+',collapse='')

##General cognitive function
#Best model (regarding time dependency and TIME1 indicator)
#load("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/lcmm_models_GC_linquad.Rdata")
#mult_splines_lin_TIME2<- multlcmm(TMTA_rev + TMTB_rev + BENTON + ISA ~ TIME1 + age_c + sexeb + ts_y, 
#                                   random = ~ ts_y,
#                                   subject = "num", data = plotcog,
#                                   randomY = FALSE, link='3-quant-splines',
#                              posfix = which(names(mult_splines_lin_TIME1$best)%in%c("I-splines3"))[c(1,2)],
#                              B=mult_splines_lin_TIME1$best)



### GENERAL cognitive function
gc_res=list()
gc_res_adj=list()


i=1
gc_res_tmp <- multlcmm(as.formula(paste0("TMTA_rev + TMTB_rev + BENTON + ISA ~ 
                                   TIME1 + age_c + sexeb + DIPNIV0 +EstimatedTotalIntraCranialVol +
                                   ts_y*scores.TC",i)), 
                    random = ~ ts_y,
                    subject = "num", data = plotcog,
                    randomY = FALSE, link='3-quant-splines')
gc_res_adj_tmp <- multlcmm(as.formula(paste0("TMTA_rev + TMTB_rev + BENTON + ISA ~ 
                                   TIME1 + age_c + sexeb + DIPNIV0 +EstimatedTotalIntraCranialVol + ts_y*asinhWMH +
                                   ts_y*scores.TC",i)), 
                       random = ~ ts_y,
                       subject = "num", data = plotcog,
                       randomY = FALSE, link='3-quant-splines')
for (i in seq(1:7)){
  #simple model for tmtb
  gc_res[[i]] <- multlcmm(as.formula(paste0("TMTA_rev + TMTB_rev + BENTON + ISA ~ 
                                   TIME1 + age_c + sexeb + DIPNIV0 +EstimatedTotalIntraCranialVol +
                                   ts_y*scores.TC",i)), 
                                   random = ~ ts_y,
                                   subject = "num", data = plotcog,
                                   randomY = FALSE, link='3-quant-splines',
                                   posfix = which(names(gc_res_tmp$best)%in%c("I-splines3"))[c(1,2)],
                                   B=gc_res_tmp$best)
  gc_res_adj[[i]] <- multlcmm(as.formula(paste0("TMTA_rev + TMTB_rev + BENTON + ISA ~ 
                                   TIME1 + age_c + sexeb + DIPNIV0 +EstimatedTotalIntraCranialVol + ts_y*asinhWMH+
                                   ts_y*scores.TC",i)), 
                          random = ~ ts_y,
                          subject = "num", data = plotcog,
                          randomY = FALSE, link='3-quant-splines',
                          posfix = which(names(gc_res_adj_tmp$best)%in%c("I-splines3"))[c(1,2)],
                          B=gc_res_adj_tmp$best)
}

gc_res_all_tmp<-multlcmm(as.formula(paste0("TMTA_rev + TMTB_rev + BENTON + ISA ~
                                      TIME1 + age_c + sexeb + DIPNIV0+ EstimatedTotalIntraCranialVol+
                                      ts_y*scores.TC1+
                                      ts_y*scores.TC2+
                                      ts_y*scores.TC3+
                                      ts_y*scores.TC4+
                                      ts_y*scores.TC5+
                                      ts_y*scores.TC6+
                                      ts_y*scores.TC7")),
                  random = ~ ts_y,
                  subject = "num", data = plotcog,
                  link='3-quant-splines')
gc_res[[8]]<-multlcmm(as.formula(paste0("TMTA_rev + TMTB_rev + BENTON + ISA ~
                                      TIME1 +age_c + sexeb + DIPNIV0+ EstimatedTotalIntraCranialVol+
                                      ts_y*scores.TC1+
                                      ts_y*scores.TC2+
                                      ts_y*scores.TC3+
                                      ts_y*scores.TC4+
                                      ts_y*scores.TC5+
                                      ts_y*scores.TC6+
                                      ts_y*scores.TC7")), 
                    random = ~ ts_y,
                    subject = "num", data = plotcog,
                    link='3-quant-splines',
                    posfix = which(names(gc_res_all_tmp$best)%in%c("I-splines3"))[c(1,2)],
                    B=gc_res_all_tmp$best)

gc_res[[9]]<-multlcmm(as.formula(paste0("TMTA_rev + TMTB_rev + BENTON + ISA ~  
                                    TIME1 +age_c + sexeb + DIPNIV0+
                                    ts_y*asinhWMH + EstimatedTotalIntraCranialVol")),
                    random = ~ ts_y,
                    subject = "num", data = plotcog,
                    link='3-quant-splines',
                  posfix = which(names(gc_res_tmp$best)%in%c("I-splines3"))[c(1,2)],
                  B=gc_res_tmp$best)

res=list(gc_res_tmp,gc_res_adj_tmp, gc_res_all_tmp, gc_res, gc_res_adj)

save(res, file=paste0("../Results/bullseye/figures/gc_res_lcmm.Rdata"))



# # # executive function: check whether linear or quadratic function is more appropriate
# exec_splines_lin <- lcmm(TMTB_rev ~  TIME1_TMT + age_c + sexeb + ts_y,
#                           random = ~ ts_y,
#                           subject = "num", data = plotcog,
#                           link='3-quant-splines')
# 
# exec_splines_quad <- lcmm(TMTB_rev ~ age_c+ sexeb + ts_y + I(ts_y^2 / 10),
#                          random = ~ ts_y + I(ts_y^2 / 10),
#                          subject = "num", data = plotcog,
#                           link='3-quant-splines')
# summarytable(exec_splines_lin, exec_splines_quad, which=c("loglik", "conv", "npm", "AIC"))
# #linear outperformed quadratic
# 
# res=list(exec_splines_lin, exec_splines_quad)
# 
# save(res, file=paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/lcmm_models_exec_linquad.Rdata"))
# 
# 
# i=1
# tmtb_res_tmp <- lcmm(as.formula(paste0("TMTB_rev ~  TIME1_TMT +  age_c + sexeb + DIPNIV0+
#                                         ts_y*scores.TC", i, 
#                                         "+ EstimatedTotalIntraCranialVol")), 
#                                         random = ~ ts_y,maxiter = 50,
#                                         subject = "num", data = plotcog,
#                                         link='3-quant-splines') 
# 
# tmtb_res_tmp_be <- lcmm(as.formula(paste0("TMTB_rev ~ TIME1_TMT + age_c + sexeb + DIPNIV0+
#                                         ts_y*scores.TC", i, 
#                                        "+ EstimatedTotalIntraCranialVol")), 
#                      random = ~ ts_y,maxiter = 50,
#                      subject = "num", data = plotcog,
#                      link='beta')
# 
# #splines outperformed beta
#summarytable(tmtb_res_tmp, tmtb_res_tmp_be, which=c("loglik", "conv", "npm", "AIC"))

tmtb_res=list()
tmtb_adj_res=list()
for (i in seq(1:7)){
tmtb_res[[i]] <- lcmm(as.formula(paste0("TMTB_rev ~  TIME1_TMT +age_c + sexeb + DIPNIV0+
                                          ts_y*scores.TC", i,
                                           "+ EstimatedTotalIntraCranialVol")),
                         random = ~ ts_y,
                         subject = "num", data = plotcog,
                         link='3-quant-splines')
tmtb_adj_res[[i]] <- lcmm(as.formula(paste0("TMTB_rev ~  TIME1_TMT +age_c + sexeb + DIPNIV0+ asinhWMH+
                                          ts_y*scores.TC", i,
                                        "+ EstimatedTotalIntraCranialVol")),
                      random = ~ ts_y,
                      subject = "num", data = plotcog,
                      link='3-quant-splines')
}

#converges without setting splines-3 to zero:
tmtb_res[[8]]<-lcmm(as.formula(paste0("TMTB_rev ~  TIME1_TMT +age_c + sexeb + DIPNIV0+
                                      ts_y*scores.TC1+
                                      ts_y*scores.TC2+
                                      ts_y*scores.TC3+
                                      ts_y*scores.TC4+
                                      ts_y*scores.TC5+
                                      ts_y*scores.TC6+
                                      ts_y*scores.TC7 + EstimatedTotalIntraCranialVol")),
                                     random = ~ ts_y,
                                     subject = "num", data = plotcog,
                                     link='3-quant-splines')

tmtb_res[[9]]<-lcmm(as.formula(paste0("TMTB_rev ~  TIME1_TMT +age_c + sexeb + DIPNIV0+
                                      ts_y*asinhWMH + EstimatedTotalIntraCranialVol")),
                    random = ~ ts_y,
                    subject = "num", data = plotcog,
                    link='3-quant-splines')

save(tmtb_res, file=paste0("../Results/bullseye/figures/tmtb_lcmm.Rdata"))
save(tmtb_adj_res, file=paste0("../Results/bullseye/figures/tmtb_adj_lcmm.Rdata"))

#load("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/tmtb_lcmm.Rdata")
##plotting trajectories -> this gives really weird plots, somehow, try simpler figure?
# datnew <- data.frame(ts_y=seq(0.08,1,length=100))
# datnew$age_init<-seq(65,95, length=100)
# datnew$age_c <- ((datnew$age_init -  mean(plotcog$age0))/10)
# datnew$sexeb <- "female"
# datnew$TIME1_TMT <- 0 #for all later timepoints
# datnew$TIME1 <- 0 #for all later timepoints
# datnew$scores.TC3 <- seq(-3,3,length=100)
# datnew$scores.TC7 <- seq(-3,3,length=100)
# datnew$DIPNIV0 <- 2
# datnew$EstimatedTotalIntraCranialVol=mean(plotcog$EstimatedTotalIntraCranialVol)
# tc3_0<- predictY(tmtb_res[[3]], newdata=datnew, var.time="ts_y", draws=FALSE)
# 
# tc3_1<- predictY(tmtb_res[[3]], newdata=datnew, var.time="ts_y", draws=FALSE)
# 
# 
# plot(tc3_0, lwd=c(2,1), 
#      type="l", col=6,# ylim=c(-160,-120), 
#      xlab="time in decade",ylab="CES-D",bty="l", 
#      legend=NULL, shades = TRUE)
# plot(tc3_1, add=TRUE, col=4, lwd=c(2,1), shades=TRUE)
# 
# 
# plot(tmtb_res[[3]], which="fit", 
#      var.time="ts_y", bty="l", xlab="ts in decades", 
#      break.times=8, ylab="latent process", 
#      lwd=2, marg=FALSE, shades=TRUE, col=2)
# 
# 
# #link function
# linktmtb_res <- predictlink(tmtb_res[[3]],ndraws=2000)
# plot(linktmtb_res, col=2, lty=2, shades=TRUE)
# 
# 
# # memory function: check whether linear or quadratic function is more appropriate
# mem_splines_lin <- lcmm(BENTON ~ TIME1 + age_c + ts_y,
#                          random = ~ ts_y,
#                          subject = "num", data = plotcog,
#                          link='3-quant-splines')
# 
# mem_splines_quad <- lcmm(BENTON ~ TIME1 + age_c + ts_y + I(ts_y^2 / 10),
#                           random = ~ ts_y + I(ts_y^2 / 10),
#                           subject = "num", data = plotcog,
#                           link='3-quant-splines')
# 
# summarytable(mem_splines_lin, mem_splines_quad, which=c("loglik", "conv", "npm", "AIC"))
# res=list(mem_splines_lin, mem_splines_quad)
# 
# save(res, file=paste0("R:/ETUDES/SHIVA_FRAUKE/Frauke/Results/bullseye/figures/lcmm_models_mem_linquad.Rdata"))
# summarytable(res[[1]],res[[2]], which=c("loglik", "conv", "npm", "AIC"))
# #quadratic outperformed linear -> U-shaped
# #plot(datnew$ts_y, -0.79753*datnew$ts_y+8.01987*(datnew$ts_y^2/10))


mem_res=list()
mem_adj_res=list()
for (i in seq(1:7)){
  #simple model for tmtb
  mem_res[[i]] <- lcmm(as.formula(paste0("BENTON ~ TIME1 + age_c + sexeb + DIPNIV0+
                                         (ts_y+ I(ts_y^2 / 10))* scores.TC", i,
                                         "+ EstimatedTotalIntraCranialVol")),
                        random = ~ ts_y + I(ts_y^2 / 10),
                        subject = "num", data = plotcog,
                        link='3-quant-splines')
  mem_adj_res[[i]] <- lcmm(as.formula(paste0("BENTON ~ TIME1 + age_c + sexeb + DIPNIV0+asinhWMH+
                                         (ts_y+ I(ts_y^2 / 10))* scores.TC", i,
                                         "+ EstimatedTotalIntraCranialVol")),
                       random = ~ ts_y + I(ts_y^2 / 10),
                       subject = "num", data = plotcog,
                       link='3-quant-splines')
}

mem_res[[8]]<-lcmm(as.formula(paste0("BENTON ~ TIME1 + age_c + sexeb + DIPNIV0+
                                      (ts_y+ I(ts_y^2 / 10))*scores.TC1+
                                      (ts_y+ I(ts_y^2 / 10))*scores.TC2+
                                      (ts_y+ I(ts_y^2 / 10))*scores.TC3+
                                      (ts_y+ I(ts_y^2 / 10))*scores.TC4+
                                      (ts_y+ I(ts_y^2 / 10))*scores.TC5+
                                      (ts_y+ I(ts_y^2 / 10))*scores.TC6+
                                      (ts_y+ I(ts_y^2 / 10))*scores.TC7 + EstimatedTotalIntraCranialVol")),
                                     random = ~ ts_y+ I(ts_y^2 / 10),
                                     subject = "num", data = plotcog,
                                     link='3-quant-splines')

mem_res[[9]]<-lcmm(as.formula(paste0("BENTON ~ TIME1 + age_c + sexeb + DIPNIV0+
                                      (ts_y+ I(ts_y^2 / 10))*asinhWMH + EstimatedTotalIntraCranialVol")),
                   random = ~ ts_y+ I(ts_y^2 / 10),
                   subject = "num", data = plotcog,
                   link='3-quant-splines')

save(mem_res, file=paste0("../Results/bullseye/figures/mem_lcmm.Rdata"))
save(mem_adj_res, file=paste0("../Results/bullseye/figures/mem_adj_lcmm.Rdata"))




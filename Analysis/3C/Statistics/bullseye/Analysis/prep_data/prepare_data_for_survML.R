library(haven)
library(psych)
library(ggplot2)

#Dataset from 3C database
setwd("/home/fbeyer/for_Frauke/Analysis")

df=read_sas("../Data/fraukeds4shivawp4251122.sas7bdat")
colnames(df)=paste(colnames(df),'_as', sep="")
df$participant_id=paste("sub-",substr(df$num_as,2,5), sep="")
#df$participant_id=paste("sub-",substr(df$num,2,5), sep="")

#MRI markers from own evaluation
mri=read.csv("../Data/MRI_markers/baseline_only_MRI_marker_data_for_fb.csv")
mri=mri[,c(1:163)]
colnames(mri)=paste(colnames(mri),'_fb', sep="")
colnames(mri)[1]="participant_id"

#MRI markers from Ariane
ab=read.table("../Data/MRI_markers/Ariane/troisC_Bdx_Dij_Montp_AB.txt", header=T)
ab_dijon=ab[ab$CENTRE==2&!is.na(ab$dateIRM),]
colnames(ab_dijon)=paste(colnames(ab_dijon),'_ab', sep="")
ab_dijon$participant_id=paste('sub-',substr(ab_dijon$NUM_ab,2,5), sep="")

#Merge all tables and do some checks
tmp=merge(df, mri, by="participant_id", all.x=T)
#dfa=merge(tmp, ab_dijon, by="participant_id", all.x=T)

#Compare HC measures
#plot(dfa$HIPP_L_ab, dfa$Left.Hippocampus_fb)
#plot(dfa$VHIPPOCAMPUS_L_cr_as, dfa$Left.Hippocampus_fb)
#plot(dfa$VHIPPOCAMPUS_L_cr_as, dfa$HIPP_L_ab) #hippocampus volume measure is the same

#Compare TIV
#plot(dfa$tiv_fb, dfa$TIV0cr_as)
#plot(dfa$tiv_fb, dfa$LCR_ab+dfa$SB_ab+dfa$SG_ab)
#plot(dfa$TIV0cr_as, dfa$LCR_ab+dfa$SB_ab+dfa$SG_ab)#TIV is also the same

#Select variables for model:
#.	Age at entry
#.	Sex (binary)
#.	Education (five levels): no or only primary education, middle school or short professional education, high school or longer professional education, higher studies
#.	APOE-e4 status (binary): presence of at least one APOE4 allele 
#.	Hypertension(binary):  SBP>= 140 mmHg and or DBP>=90mm Hg and or use of antihypertensive drugs
#.	Diabetes(binary):  diabetes if fasting glucose >= 7 mmmok/L and or antidiabetic drugs
#.	Cardiovascular disease(binary):  history of cardiovascular disease (self-reported history of myocardial infarction, bypass cardiac surgery, angioplasty, stroke or peripheral vascular disease)
#.	MMST: 30-item questionnaire assessing global cognitive abilities
#.	Isaac set test; assesses verbal semantic fluency (score: total number of words listed)
#.	Benton Visual Retention Test: immediate visual memory test (score: 0 - 15)
#.	Trail Making Test: Time B/Time A

demo=tmp[,c("participant_id", "age0_as", "sexeb_as", "DIPNIV0_as",
         "GLYCEMIE0_as", "hta0_1_as", "cardvas0_as", "apoe4_as",
         "MMSTT0_as", "ISA0_15_as", "BENTON0_as", "TM0_1_TE_as", "TM0_2_TE_as")]
#recode diabetes to have no and mild glycemia in reference category
# demo[!is.na(demo$GLYCEMIE0_as), "diab"]=0
# demo[!is.na(demo$GLYCEMIE0_as)&demo$GLYCEMIE0_as>1,"diab"]=1
# 
# hist(demo$DIPNIV0_as)
# table(demo$hta0_1_as)
# table(demo$cardvas0_as)
# table(demo$apoe4_as)
# 
# hist(demo$MMSTT0_as)
# hist(demo$ISA0_15_as)
# hist(demo$BENTON0_as)
# 
# #calculate TMTB/TMTA ratio
# demo$TMT_ratio=as.numeric(demo$TM0_2_TE_as)/as.numeric(demo$TM0_1_TE_as)
# hist(demo$TMT_ratio)

#Add outcome relevant measures: 
#dementia status at baseline: DEMT0,
#dementia status after 12y followup: DEM0_6
#type of dementia: DEMDIA0_6bis
#age of dementia: agedem6 (calculated for demented as lying between last visit without
#and first visit with dementia, for undemented: age at last visit without dementia)
#variable datedem1: Date (dd/mm/yyyy) at the last visit without disease for demented and non-demented: 
#variable datedem2:	First Date (dd/mm/yyyy) of the where positive diagnosis was made (completed only for demented):
#DC6_as: deceased
#DATDN6_as": date of death
#"agedc6_as: age at death

#compare with AB measures:
#DEMT0_ab (one measure only)
#AGEDEM_ab (at each SUIVI)
#DELAI_DEM_ab: time between first visit with dementia and inclusion
#DC_ab:death
#AGEFIN_ab: age at death
#Indic_CR2_ab
#Age_CR2_ab


tt=merge(demo, tmp[,c("participant_id", "date0_as", "age0_as", "demt0_as", "dem0_6_as", 
                      "DEMDIA0_6bis_as", "agedem6_as", 
                      "datedem1_as", "datedem2_as", "DC6_as", "DATDN6_as", "agedc6_as")])

#Now add info from AB (is recorded in long format for each suivi)
test=merge(tt, ab_dijon[,c("participant_id", "DEMT0_ab","AGEDEM_ab", "DELAI_DEM_ab", "DC_ab",
                      "AGEFIN_ab","Indic_CR2_ab", "AGE_CR2_ab")])

#data are in accordance, just AGEDEM_ab and AGEFIN_ab are taken for each SUIVI and therefore not to be used
head(test[, c("participant_id", "age0_as","dem0_6_as", "agedem6_as", "AGEDEM_ab", "DC6_as", "agedc6_as", "AGEFIN_ab", 
              "Indic_CR2_ab", "AGE_CR2_ab")])

#Continue with tt table!
#Exclude participants with prevalent dementia at baseline/unclassified or
#those for who follow-up is unclassified
#demt0=1
#demt0=9
#dem0_6=9

ds=tt[tt$demt0_as==0&tt$dem0_6_as!=9,]

#Create survival object with time to first observation with dementia or last dementia-free
#observation (like Ariane did) & status
#There are some differences between AB and my dataset because of considering death 
#For now, just use simple survival model (even though Coxboost allows for competing events)
#single risk: dementia
ds$status_cph=ds$dem0_6_as
ds$time_cph=ds$datedem1_as-ds$date0 #time to last visit without dementia for all
#time until diagnosis was made for cases:
ds[!is.na(ds$datedem2_as),"time_cph"]=ds[!is.na(ds$datedem2_as),"datedem2_as"]-
                                  ds[!is.na(ds$datedem2_as),"date0_as"]

#checking the data
head(ds[, c("participant_id", "time_cph", "date0_as", "dem0_6_as", "agedem6_as", "datedem1_as", "datedem2_as")])

#there is one participant with no time of dementia
head(ds[ds$dem0_6_as==1&is.na(ds$datedem2_as),c("dem0_6_as", "time_cph", "agedem6_as", "date0_as","datedem1_as", "datedem2_as")])

#competing risk: dementia (absorbing state) OR death
ds$status_cr=ds$dem0_6_as
ds[ds$DC6_as==1&ds$status_cr==0,"status_cr"]=2
table(ds$status_cr)

#time is shortest time to dementia, death or censoring
ds$time_cr=ds$datedem1_as-ds$date0
#if dementia occurs before death:
ds[ds$status_cr==1&!is.na(ds$datedem2_as),"time_cr"]=ds[ds$status_cr==1&!is.na(ds$datedem2_as),"datedem2_as"]-
  ds[ds$status_cr==1&!is.na(ds$datedem2_as),"date0_as"]
ds[ds$status_cr==2,"time_cr"]=ds[ds$status_cr==2,"DATDN6_as"]-
                                                    ds[ds$status_cr==2,"date0_as"]


head(ds[, c("participant_id", "time_cph", "date0_as", "dem0_6_as", "agedem6_as", "datedem1_as", "datedem2_as", "time_cr", "status_cr")])

table(ds$status_cph,ds$status_cr)

#one participant without date of first visit with dementia
head(ds[ds$dem0_6_as==1&is.na(ds$datedem2_as),c("participant_id", "time_cph", "date0_as", "dem0_6_as", "agedem6_as", "datedem1_as", "datedem2_as", "time_cr", "status_cr")])

write.csv(ds, "../Data/OwnDatasets/final_dataframe_for_survival.csv", row.names = F)

#add MRI data
#hippocampal volume (adjusted for TIV), total gray matter volume (adjusted for TIV),, WML volume (adjusted for TIV),
#four WML components, 59 GMV regions-of-interest, BG and DWM dPVS cluster count, lacune(y_n)

#[x] Calculate average of GM volumes (hippocampus take assymetry into account?)
#First add all self-derived volumes after averaging over hemispheres
mri2av= mri[,c(1, 25:28, 30:37, 39:54, 59:158)]
            #c(1,9:16,18:21,23:162)]
mri2av_long <- mri2av %>%                                   # Apply gather function
  gather(variable, value, - c(1))

mri2av_long = mri2av_long %>% separate(variable, c("hemi", "area"), extra="merge")
summrilong=mri2av_long %>% group_by(participant_id, area) %>% 
  summarize(Mean = mean(value, na.rm=TRUE))

mri2av_wide=summrilong  %>%                                   # Apply gather function
  spread(area, Mean)

#add averaged volumes
dm=merge(ds, mri2av_wide,  by="participant_id", all.x=T)

#also add other non-lateralized self-derived outputs

dm=merge(dm, mri[,c(1,9:21,23,24,38,56:58, 159:162)], by="participant_id", all.x=T)

#Add dPVS automated segmentation and lacune indicator from AB
dm=merge(dm, ab_dijon[ab_dijon$SUIVI_ab==0,c("participant_id", 
                                             "DWM_PVS_cluster7voxel_AVR_ab",
                                             "BG_PVS_cluster_AVR_ab",
                                             "lacInf_0_ab")])
#Finally add WML and further dPVS indicators from AS:
dm=merge(dm,tmp[,c("participant_id", "Volmask0cr_as", "WMH0cr_as", "PWMH0cr_as", "DWMH0cr_as",
                   "PVLENT_as", "PVSUBCOR_as", "LpvsBG_as", "Lpvssubcor_as")] )
#PVLENT & PVSUBCOR: presence and severity of PVS
#LpvsBG, Lpvssubcor: presence of large (>3mm PVS) in BG/subcortex


#normalize all GMVs and total GMV by TIV (don't perform regression)
dm_n = dm %>% 
  mutate(across(Accumbens.Area_fb:Ventral.DC_fb, ~.x/tiv_fb))

dm_n = dm_n %>% 
  mutate(gmv_fb = gmv_fb/tiv_fb)

dm_n = dm_n %>% 
  mutate(across(Cerebellar.Vermal.Lobules.I.V_fb:Cerebellar.Vermal.Lobules.VIII.X_fb,
                ~ .x/tiv_fb))


#normalize WMH by WM mask (Volmask0cr_as) and loadings by WMV (wmv)
dm_n = dm_n %>% 
  mutate(WMH0cr_as = WMH0cr_as/Volmask0cr_as)
dm_n = dm_n %>% 
  mutate(across(V1_fb:V4_fb, ~ .x/wmv_fb))

#what about dPVS?
dm_n = dm_n %>% 
  mutate(across(DWM_PVS_cluster7voxel_AVR_ab:BG_PVS_cluster_AVR_ab,
                ~ .x/tiv_fb))
#need to apply one-hot-encoding for categorical variables? 
#only education (DIPNIV0_as) has more than one level, but its ordinal
#so can leave it like this
t3=dm_n[,c("participant_id", "DIPNIV0_as")]
mm=model.matrix(~ DIPNIV0_as, t3, contrasts=contr.treatment(DIPNIV0_as))

#create final dataframe
dm_final = dm_n %>% select(participant_id,
                           status_cph, time_cph,
                           status_cr, time_cr,
                           DEMDIA0_6bis_as,
                           age0_as:DIPNIV0_as, 
                           hta0_1_as:BENTON0_as,
                           diab:TMT_ratio,
                           Accumbens.Area_fb: Caudate_fb,
                           CO.central.operculum_fb:Hippocampus_fb,
                           IOG.inferior.occipital.gyrus_fb:ITG.inferior.temporal.gyrus_fb,
                           LiG.lingual.gyrus_fb:Ventral.DC_fb,
                           Cerebellar.Vermal.Lobules.I.V_fb:
                             Cerebellar.Vermal.Lobules.VIII.X_fb,
                           V1_fb:V4_fb,
                           gmv_fb, 
                           DWM_PVS_cluster7voxel_AVR_ab,
                           BG_PVS_cluster_AVR_ab,
                           lacInf_0_ab,
                           WMH0cr_as,
                           PVLENT_as,
                           PVSUBCOR_as)


#normalize/scale all inputs
dm_fn = dm_final %>% 
  mutate(across(age0_as:PVSUBCOR_as, ~ scale(.x)))

#make all variables numeric
dm_fn = dm_fn %>% mutate(across(age0_as:PVSUBCOR_as, ~as.numeric(.x)))


#test whether normalization worked:
#sd(dm_fn$gmv_fb, na.rm=T)
#mean(dm_fn$gmv_fb, na.rm=T)

#select only those with WML (1773)
dm_fn=dm_fn[!is.na(dm_fn$V1_fb),]


write.csv(dm_fn, "final_dataframe_for_MLsurvival.csv", row.names = F)

# dm_fn=read.csv("final_dataframe_for_MLsurvival.csv")
# 
# #try imputation -> move to analysis script
# missing_pattern=mice::md.pattern(dm_fn[,c(6:16)], rotate=T)
# 
# x.train=dm_fn[,c(6:88)]
# 
# #missing data is only present in clinical variables
# ini=mice::mice(x.train, maxit=0)
# attributes(ini)
# 
# #treat everything as numeric (also education which is the only
# #predictor with > 2 levels) because only numeric
# #values are allowed in CoxBoost
# pred<-ini$predictorMatrix
# impmethod<-c("", "", "pmm", "", "", 
#              "pmm", "pmm", "pmm", "pmm",
#              "pmm", "pmm", "pmm",
#              rep("", 71)) #MRI variables do not need to be imputed
# names(impmethod)=colnames(x.train)
# 
# m=5
# iter=10
# imp<-mice::mice(x.train, 
#                 method=impmethod, 
#                 pred=pred, 
#                 maxit=iter, 
#                 m=m, 
#                 seed=8745)

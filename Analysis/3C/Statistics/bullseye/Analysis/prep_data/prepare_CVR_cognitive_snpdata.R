library(haven)
library(psych)
library(ggplot2)
library(tidyr)
library(dplyr)
library(stringr)
library(naniar)
library(lubridate)

setwd("/home/fbeyer/for_Frauke/Analysis/")
source("./funcs/create_PCA.R")

###Prepare demo data
df=read_sas("../Data/fraukeds4shivawp4251122.sas7bdat")

add=read.table("../Data/CogniFrauke060723.txt", header = T, sep = "\t", strip.white = T)

#Prepare for statistical analysis
dfs=df[,c("num", "age0", "sexeb", "DIPNIV0",
          "hta0_1", "antihyp0b", 
          "bmi0", "apoe4", "A334_472",
          "GLYCEMIE0", "FUME0", "chol1", "medlip0",
          "MMSTT0",  "WMH0cr", "Volmask0cr", "demt0", "avccorr0",
          "EvStroke", "Stroke", "dateStroke", "typavc12", "date0")]
dfs=merge(dfs, add[,c("num", "whr0", "pad0", "pas0")], all.x=T)

N_MRI_age=nrow(dfs)

#exclude demented & stroke
Excl_dement=nrow(dfs[dfs$demt0==1,])
dfs=dfs[dfs$demt0==0,]

Excl_stroke=nrow(dfs[!is.na(dfs$avccorr0)&dfs$avccorr0==1,])
dfs=dfs[dfs$avccorr0==0|is.na(dfs$avccorr0),]

dfs$pseudo_n=as.numeric(str_remove(dfs$num, "^20+|^2"))

# load tiv
wmv_tiv=read.table("../Data/MRI_markers/bullseye/aseg.vol.table", 
                   header=T
)
colnames(wmv_tiv)[1]="pseudo_n"
colnames(wmv_tiv)[54]="CorticalWhiteMatterVol"
wmv_tiv$CorticalWhiteMatterVol=wmv_tiv$CorticalWhiteMatterVol/1000
wmv_tiv$EstimatedTotalIntraCranialVol=scale(wmv_tiv$EstimatedTotalIntraCranialVol)

dfs=merge(dfs, wmv_tiv[,c("pseudo_n", "CorticalWhiteMatterVol", "EstimatedTotalIntraCranialVol")],all.x=T)

##CVR risk factors
dfs$antihyp0b=as.factor(dfs$antihyp0b)
dfs$sexeb=as.factor(dfs$sexeb)
levels(dfs$sexeb)=c("male", "female") #0:male, 1 female

#different APOE models
#dosage of e4/e2 alleles:
dfs$apoe4_dose=dplyr::recode(dfs$A334_472, "22" = "0", 
                             "23" = "0", "24" = "1",
                             "33" = "0", "34" = "1", 
                             "44" = "2",.default = NA_character_)
dfs$apoe4_dose=as.factor(dfs$apoe4_dose)
dfs$apoe2_dose=dplyr::recode(dfs$A334_472, "22" = "2", 
                        "23" = "1", "24" = "1",
                        "33" = "0", "34" = "0", 
                        "44" = "0",.default = NA_character_)
dfs$apoe2_dose=as.factor(dfs$apoe2_dose)

#dominant model for e4 including 24 is provided in dataframe:
dfs$apoe4=as.factor(dfs$apoe4)
dfs$apoe2=dplyr::recode(dfs$A334_472, "22" = "1", 
                        "23" = "1", "24" = "1",
                        "33" = "0", "34" = "0", 
                        "44" = "0",.default = NA_character_)
dfs$apoe2=as.factor(dfs$apoe2)

#dominant model: carrying at least one e2/e4 allele, excluding e24 genotype
dfs$apoe2_ex=dplyr::recode(dfs$A334_472, "22" = "1", 
                             "23" = "1", "24" = "NA",
                             "33" = "0", "34" = "0", 
                             "44" = "0",.default = NA_character_)
dfs$apoe2_ex=as.factor(dfs$apoe2_ex)
dfs$apoe4_ex=dplyr::recode(dfs$A334_472, "22" = "0", 
                           "23" = "0", "24" = "NA",
                           "33" = "0", "34" = "1", 
                           "44" = "1",.default = NA_character_)
dfs$apoe4_ex=as.factor(dfs$apoe4_ex)

#homozygotes versus non-carriers (24,34=NA)
dfs$apoe4_homonon=dplyr::recode(dfs$A334_472, "22" = "0", 
                             "23" = "0", "24" = "NA",
                             "33" = "0", "34" = "NA", 
                             "44" = "1",.default = NA_character_)
dfs$apoe4_homonon=as.factor(dfs$apoe4_homonon)
dfs$apoe2_homonon=dplyr::recode(dfs$A334_472, "22" = "1", 
                             "23" = "NA", "24" = "NA",
                             "33" = "0", "34" = "0", 
                             "44" = "0",.default = NA_character_)
dfs$apoe2_homonon=as.factor(dfs$apoe2_homonon)

#homozygotes versus 33 (22,23,24,34=NA)
dfs$apoe4_v33=dplyr::recode(dfs$A334_472, "22" = "NA", 
                                "23" = "NA", "24" = "NA",
                                "33" = "0", "34" = "NA", 
                                "44" = "1",.default = NA_character_)
dfs$apoe4_v33=as.factor(dfs$apoe4_v33)
dfs$apoe2_v33=dplyr::recode(dfs$A334_472, "22" = "1", 
                                "23" = "NA", "24" = "NA",
                                "33" = "0", "34" = "NA", 
                                "44" = "NA",.default = NA_character_)
dfs$apoe2_v33=as.factor(dfs$apoe2_v33)

###############################################################
dfs$hta0_1=as.factor(dfs$hta0_1)
dfs$FUME0=as.factor(dfs$FUME0)
levels(dfs$FUME0)=c("never smoker", "previous smoker", "current smoker")
dfs$GLYCEMIE0_bin=dplyr::recode(dfs$GLYCEMIE0, "0"="0", "1"="1", "2"="1")
dfs$asinhWMH0cr=asinh(dfs$WMH0cr)
dfs=dfs%>%mutate(DYSLIP = if_else(chol1 > 6.2, 1, 0))
dfs$DYSLIP=as.factor(dfs$DYSLIP)
dfs=dfs%>%mutate(WHR_bin = case_when(whr0 > 0.9 & sexeb=="male" ~1,
                                     whr0 > 0.85 & sexeb=="female" ~1,
                                     is.na(whr0) ~ NA_real_,
                                     TRUE ~ 0))
dfs$WHR_bin=as.factor(dfs$WHR_bin)

#Prepare WMH components data from those who fulfill exclusions!!
res_3C=read.csv("../Data/MRI_markers/bullseye/bullseye_3C_HSB.csv",
                colClasses = c(rep("factor",2),rep("numeric",1),
                               rep("factor",1),rep("numeric",1),
                               rep("factor",2),rep("numeric",1)))
res_3C$pseudo_n=as.numeric(str_remove(res_3C$pseudonym, "^0+"))

res_3C_usable=res_3C %>%
  filter(pseudo_n %in% dfs$pseudo_n)
write.csv(res_3C_usable, "../Data/MRI_markers/bullseye/bullseye_3C_HSB_usable.csv", row.names = F)

res_df_sum = res_3C_usable %>% group_by(pseudonym) %>% 
  summarize(WMH_sum = sum(val, na.rm=TRUE)) %>% 
  mutate(asinhWMH = asinh(WMH_sum))



nfactors=7 #Parallel analysis suggests that the number of factors =  11  and the number of components =  7
pca_3C=create_PCA(res_3C_usable, method="pca", 7, rotation="oblimin", plot=F)
fa.parallel(pca_3C[[1]], n.obs=length(unique(res_3C_usable$pseudonym)))

pscores=data.frame(scores=pca_3C[[2]]$scores,
                   pseudonym=unique(res_3C_usable$pseudonym),
                   num=paste0("2",unique(res_3C_usable$pseudonym)))



##ADD WMH data
#pscores=read.csv("../Data/MRI_markers/bullseye/3C_scores_7comp_oblimin.csv")

dfs=merge(dfs, pscores) #use inner join, only those that are in both datasets should be used
dfs=merge(dfs, res_df_sum)
dfs=merge(dfs, wmv_tiv[,c("pseudo_n", "EstimatedTotalIntraCranialVol", "CorticalWhiteMatterVol")])

N_MRI_available=nrow(dfs)


#exclude extreme outliers and implausible values in SBP (mean + 5SD)
N_excl_SBP=nrow(dfs[dfs$pas0>=250,])
dfs=dfs[dfs$pas0<250,]

#normalize WMH by sex
plot(dfs$sexeb, dfs$whr0)
res_whr=lm(whr0~sexeb, data=dfs[!is.na(dfs$whr0),])
dfs[!is.na(dfs$whr0),"whr_st"]=res_whr$residuals


N_MRI_final=nrow(dfs)

write.csv(dfs, "../Data/OwnDataSets/CVRdata_N1670_all_with_missings_Smoking_Dyslip_Diab.csv")


completedData=subset(dfs, 
                       (!is.na(dfs$hta0_1))&
                       (!is.na(dfs$bmi0))&
                       (!is.na(dfs$CorticalWhiteMatterVol))&
                       (!is.na(dfs$EstimatedTotalIntraCranialVol)))

write.csv(completedData, "../Data/OwnDataSets/CVRdata_N1699_HTA_BMI.csv")



hist(completedData$pad0, breaks = 50)#looks ok


# sum=sumtable(dfs[,c("age0", "sexeb",
#                     "pas0", "pad0","hta0_1","antihyp0b", 
#                     "FUME0", "DYSLIP","GLYCEMIE0_bin",
#                     "bmi0","whr0", "WHR_bin", 
#                     "apoe4_dose", "apoe2_dose", 
#                     "MMSTT0", "WMH_sum")],
#              labels=c("Age (y)", "Sex", 
#                       "SBP (mmHg)", "DBP (mmHg)", "HTN", "BP medi", 
#                       "Smoking", "Hyperlipidemia","Diabetes", 
#                       "BMI", "WHR", "WHR binary",
#                       "ApoE e4", "ApoE e2", "MMSE", "WMH volume (mm^3)"),
#              summ=c('notNA(x)','mean(x)', 'median(x)', 'sd(x)','min(x)','max(x)'),
#              summ.names	=c("N", "Mean", "Median", "SD", "Min", "Max"),
#              digits = 3,
#              out="return",
#              add.median=TRUE)



#save(sum, file="../Results/bullseye/figures/sum_CVR_3C.Rdata")





############################################

#Prepare cognitive data
cog=read.table("../Data/CogniFrauke060723.txt", header = T, sep = "\t", strip.white = T)


#time to first assessment
cog2 = cog %>% mutate(time1 = as.Date(strptime(DATE1, format="%d/%m/%Y"))-
                              as.Date(strptime(DATE0, format="%d/%m/%Y")),
                      time2 = as.Date(strptime(DATE2, format="%d/%m/%Y"))-
                        as.Date(strptime(DATE0, format="%d/%m/%Y")),
                      time4 = as.Date(strptime(DATE4, format="%d/%m/%Y"))-
                        as.Date(strptime(DATE0, format="%d/%m/%Y")),
                      time5 = as.Date(strptime(DATE5, format="%d/%m/%Y"))-
                        as.Date(strptime(DATE0, format="%d/%m/%Y")),
                      time6 = as.Date(strptime(DATE6, format="%d/%m/%Y"))-
                        as.Date(strptime(DATE0, format="%d/%m/%Y")))


cog2 <- replace_with_na_at(cog2, .vars = c("TM0_1_TE", "TM0_2_TE",
                                           "TM2_1_TE", "TM2_2_TE",
                                           "TM4_1_TE", "TM4_2_TE",
                                           "TM5_1_TE", "TM5_2_TE",
                                           "TM6_1_TE", "TM6_2_TE"), 
                          condition = ~.x %in% c(""))


#transform all values to seconds
cog2 <- cog2 %>% mutate_at(vars(ends_with("_TE")), ~as.numeric(hms(.x)))


# calculate TMT B/A and processing speed, log-transform
# and negate it to facilitate later calculations
cog2 = cog2 %>% mutate(tmt0 = -log(TM0_2_TE/TM0_1_TE),
                      tmt2 = -log(TM2_2_TE/TM2_1_TE),
                      tmt4 = -log(TM4_2_TE/TM4_1_TE),
                      tmt5 = -log(TM5_2_TE/TM5_1_TE),
                      tmt6 = -log(TM6_2_TE/TM6_1_TE),
                      proc0=-log(TM0_1_TE),
                      proc2=-log(TM2_1_TE),
                      proc4=-log(TM4_1_TE),
                      proc5=-log(TM5_1_TE),
                      proc6=-log(TM6_1_TE))

#add NAs to tmt and proc for wave1
cog2$TM1_1_TE=NA
cog2$TM1_2_TE=NA
cog2$tmt1=NA
cog2$proc1=NA
cog2$time0=0

#check distributions -> tmt should be log-transformed
hist(cog2$tmt0)
hist(cog2$tmt2)
hist(cog2$tmt4)
hist(cog2$tmt5)
hist(cog2$tmt6)

hist(cog2$proc0)
hist(cog2$proc2)
hist(cog2$proc4)
hist(cog2$proc5)
hist(cog2$proc6)

#ISA is ok distributed
hist(cog2$ISA0_15)
hist(cog2$ISA6_15)

#approximately normal
hist(cog2$BENTON0)
hist(cog2$BENTON1)
hist(cog2$BENTON2)
hist(cog2$BENTON4)
hist(cog2$BENTON5)
hist(cog2$BENTON6)


#scale scores from all waves (tmtb/a, tmta, ISA, BVRT) to baseline mean with baseline variance 
cog2 = cog2 %>% mutate_at(vars(starts_with("tmt")), 
                       funs(scaled=(.-mean(cog2$tmt0, na.rm=T))/sd(cog2$tmt0, na.rm=T)))
cog2 = cog2 %>% mutate_at(vars(starts_with("proc")), 
                          funs(scaled=(.-mean(cog2$proc0, na.rm=T))/sd(cog2$proc0, na.rm=T)))
cog2 = cog2 %>% mutate_at(vars(starts_with("ISA")), 
                          funs(scaled=(.-mean(cog2$ISA0_15, na.rm=T))/sd(cog2$ISA0_15, na.rm=T)))
cog2 = cog2 %>% mutate_at(vars(starts_with("BENTON")), 
                          funs(scaled=(.-mean(cog2$BENTON0, na.rm=T))/sd(cog2$BENTON0, na.rm=T)))


#form composite score for executive function/global function as average of scaled individual scores
cog2 = cog2 %>% mutate(exec0 = rowMeans(select(.,tmt0_scaled,ISA0_15_scaled), na.rm=T),
                       exec1 = NA,
                       exec2 = rowMeans(select(.,tmt2_scaled,ISA2_15_scaled), na.rm=T),
                       exec4 = rowMeans(select(.,tmt4_scaled,ISA4_15_scaled), na.rm=T),
                       exec5 = rowMeans(select(.,tmt5_scaled,ISA5_15_scaled), na.rm=T),
                       exec6 = rowMeans(select(.,tmt6_scaled,ISA6_15_scaled), na.rm=T)
                       )

#form global composite
cog2 = cog2 %>% mutate(glob0 = rowMeans(select(.,tmt0_scaled,ISA0_15_scaled,BENTON0_scaled,proc0_scaled), na.rm=T),
                       glob1 = rowMeans(select(.,ISA1_15_scaled,BENTON1_scaled), na.rm=T),
                       glob2 = rowMeans(select(.,tmt2_scaled,ISA2_15_scaled,BENTON2_scaled,proc2_scaled), na.rm=T),
                       glob4 = rowMeans(select(.,tmt4_scaled,ISA4_15_scaled,BENTON4_scaled,proc4_scaled), na.rm=T),
                       glob5 = rowMeans(select(.,tmt5_scaled,ISA5_15_scaled,BENTON5_scaled,proc5_scaled), na.rm=T),
                       glob6 = rowMeans(select(.,tmt6_scaled,ISA6_15_scaled,BENTON6_scaled,proc6_scaled), na.rm=T))


varying_list=list(c("time0", "time1", "time2", "time4", "time5", "time6"),
                  c("proc0_scaled", "proc1_scaled", "proc2_scaled", "proc4_scaled", "proc5_scaled", "proc6_scaled"),
                  c("exec0", "exec1", "exec2", "exec4", "exec5", "exec6"),
                  c("glob0", "glob1", "glob2", "glob4", "glob5", "glob6"),
                  c("TM0_1_TE", "TM1_1_TE","TM2_1_TE", "TM4_1_TE", "TM5_1_TE", "TM6_1_TE"),
                  c("TM0_2_TE", "TM1_2_TE","TM2_2_TE", "TM4_2_TE", "TM5_2_TE", "TM6_2_TE"),
                  c("ISA0_15", "ISA1_15", "ISA2_15", "ISA4_15", "ISA5_15", "ISA6_15"),
                  c("ISA0_15_scaled", "ISA1_15_scaled", "ISA2_15_scaled", "ISA4_15_scaled", "ISA5_15_scaled", "ISA6_15_scaled"),
                  c("BENTON0", "BENTON1", "BENTON2", "BENTON4", "BENTON5", "BENTON6"),
                  c("BENTON0_scaled", "BENTON1_scaled", "BENTON2_scaled", "BENTON4_scaled", "BENTON5_scaled", "BENTON6_scaled"))
varying_names=c("ts", "proc", "exec", "glob", "TMTA", "TMTB", "ISA", "ISA_scaled", "BENTON", "BENTON_scaled")
cog_long=reshape(cog2[,c("num", "DATE0", "time0", "time1", "time2", "time4", "time5", "time6",
                         "TM0_1_TE", "TM1_1_TE","TM2_1_TE", "TM4_1_TE", "TM5_1_TE", "TM6_1_TE",
                         "TM0_2_TE", "TM1_2_TE", "TM2_2_TE", "TM4_2_TE", "TM5_2_TE", "TM6_2_TE",
                         "proc0_scaled", "proc1_scaled", "proc2_scaled", "proc4_scaled", "proc5_scaled", "proc6_scaled",
                         "ISA0_15", "ISA1_15", "ISA2_15", "ISA4_15", "ISA5_15", "ISA6_15",
                         "ISA0_15_scaled", "ISA1_15_scaled", "ISA2_15_scaled", "ISA4_15_scaled", "ISA5_15_scaled", "ISA6_15_scaled",
                         "BENTON0", "BENTON1", "BENTON2", "BENTON4", "BENTON5", "BENTON6",
                         "BENTON0_scaled", "BENTON1_scaled", "BENTON2_scaled", "BENTON4_scaled", "BENTON5_scaled", "BENTON6_scaled",
                         "glob0", "glob1", "glob2", "glob4", "glob5", "glob6",
                         "exec0", "exec1", "exec2", "exec4", "exec5", "exec6")], varying=varying_list, v.names=varying_names,
                 times=c("T0", "T1", "T2", "T4", "T5", "T6"), idvar="num",
                 direction="long")


dfs = dfs %>% mutate_at(vars(starts_with("scores")),funs(scaled=scale(.)))
cog_long_d=merge(cog_long, dfs[, c("num", "age0", "sexeb", "DIPNIV0", "EstimatedTotalIntraCranialVol",
                                   "CorticalWhiteMatterVol",
                                   "scores.TC1", "scores.TC3","scores.TC4","scores.TC2","scores.TC5",
                                   "scores.TC6","scores.TC7",  
                                   "asinhWMH")])


cog_long_d_c=subset(cog_long_d, 
                      (!is.na(cog_long_d$DIPNIV0))&
                       (!is.na(cog_long_d$EstimatedTotalIntraCranialVol)))
cog_long_d_c$time=as.factor(cog_long_d_c$time)
cog_long_d_c$DIPNIV0=as.factor(cog_long_d_c$DIPNIV0)
cog_long_d_c$ts= cog_long_d_c$ts/365


# sum_BL=sumtable(cog_long_d_c[cog_long_d_c$time=="T0",c("age0", "sexeb", "DIPNIV0")],
#                 labels=c("Age (y)", "Sex", "Education"),
#                 out="return",
#                 add.median=T)
# 
# sumN_tests=sumtable(cog_long_d_c[,c("time","ts", "BENTON", "ISA", "TMTA", "TMTB")], 
#          labels=c("Time (y)", "BENTON", "ISA", "TMTA", "TMTB"),
#          summ=c('notNA(x)','mean(x)', 'median(x)', 'sd(x)','min(x)','max(x)'),
#          summ.names	=c("N", "Mean", "Median", "SD", "Min", "Max"),
#          digits = 3,
#          out="return",
#          add.median=TRUE,
#          group="time", group.long=T)
write.csv(cog_long_d_c, "../Data/OwnDataSets/Cogdata_N1668.csv")

#sum_list=list(sum_BL,sumN_tests)
#save(sum_list, file="../Results/bullseye/figures/cogperformance/sum_Cog_3C.Rdata")


cog2fm=cog2[,c("num", "DATE0", "time0", "time1", "time2", "time4", "time5", "time6",
        "TM0_1_TE", "TM1_1_TE","TM2_1_TE", "TM4_1_TE", "TM5_1_TE", "TM6_1_TE",
        "TM0_2_TE", "TM1_2_TE", "TM2_2_TE", "TM4_2_TE", "TM5_2_TE", "TM6_2_TE",
        "proc0_scaled", "proc1_scaled", "proc2_scaled", "proc4_scaled", "proc5_scaled", "proc6_scaled",
        "ISA0_15", "ISA1_15", "ISA2_15", "ISA4_15", "ISA5_15", "ISA6_15",
        "ISA0_15_scaled", "ISA1_15_scaled", "ISA2_15_scaled", "ISA4_15_scaled", "ISA5_15_scaled", "ISA6_15_scaled",
        "BENTON0", "BENTON1", "BENTON2", "BENTON4", "BENTON5", "BENTON6",
        "BENTON0_scaled", "BENTON1_scaled", "BENTON2_scaled", "BENTON4_scaled", "BENTON5_scaled", "BENTON6_scaled",
        "glob0", "glob1", "glob2", "glob4", "glob5", "glob6",
        "exec0", "exec1", "exec2", "exec4", "exec5", "exec6")]
cogcross=merge(cog2fm, dfs[, c("num", "age0", "sexeb", "DIPNIV0", "EstimatedTotalIntraCranialVol",
                                 "CorticalWhiteMatterVol",
                                 "scores.TC1", "scores.TC3","scores.TC4","scores.TC2","scores.TC5",
                                 "scores.TC6","scores.TC7",  
                                 "asinhWMH")])

N_excl_education=nrow(cogcross[is.na(cogcross$DIPNIV0),])

cogcross_c=subset(cogcross, 
                    (!is.na(cogcross$DIPNIV0))&
                      (!is.na(cogcross$EstimatedTotalIntraCranialVol)))
cogcross_c$DIPNIV0=as.factor(cogcross_c$DIPNIV0)
write.csv(cogcross_c, "../Data/OwnDataSets/Cogdata_N1668_cross.csv")

cflow=list(c(N_MRI_age, Excl_dement, Excl_stroke, N_excl_SBP, N_MRI_available,
             N_MRI_final, N_excl_education))
save(cflow,file="../Results/bullseye/figures/for_flowchart.Rdata")

### Prepare SNP data
snp=read.table("../Data/Genetics/For_Frauke_Sargurupremraj_etal_WMH_27riskloci_NatComm_2020.traw",
               header=T,sep = "\t")

snpinfo=snp[,c(1:6)]

#read info from effects file -> the traw file counts the correct "risk SNPs
effects=read.table("../Data/Genetics/Murali_WMH_27riskloci_cSVD_SNP_Effect_alle_Effect.txt", header=T)
merged_effects=merge(snpinfo, effects)

snp2=t(snp[,c(2,7:ncol(snp))])

snp_df=data.frame(snp2)
colnames(snp_df)=snp_df[1,]
snp_df$id=row.names(snp_df)
snp_df=snp_df[snp_df$id!="SNP",]
snp_df$num=unlist(strsplit(snp_df$id, '\\_'))[seq(2,nrow(snp_df)*2,2)]


## read genetic risk scores
snpscore=read.csv("../Data/Genetics/For_Frauke_GRS_Sargurupremraj_etal_WMH_27riskloci_NatComm_2020.txt",
                    header=T, sep="\t")
colnames(snpscore)[2]="num"

gen=merge(snp_df, snpscore,all.x=T)

#read PCs:
pc=read.table("../Data/Genetics/Cohort_3C_PCs_pca.eigenvec",sep=" ")
colnames(pc)[1]="num"

gen=merge(gen, pc,all.x=T)

#merge basic demographics & TC7
dfsgen=merge(dfs, gen, by="num", all.x=T)
write.csv(dfsgen, "../Data/Genetics/Gendata_N1670.csv")

#prepare stroke data
#Create survival object with time to first observation with dementiastroke or last stroke-free
#observation & status

#For now, just use simple survival model 
#single risk: stroke (fatal + non-fatal)
dfs$status_stroke_cph=dfs$EvStroke
dfs$time_stroke_cph=dfs$dateStroke-dfs$date0 #time between first visit and last visit with/
#without stroke

#only ischemic stroke:
dfs[!is.na(dfs$EvStroke),"status_ich_cph"]=0
dfs[dfs$typavc12==1&!is.na(dfs$typavc12), "status_ich_cph"]=1
#only hemo stroke:
dfs[!is.na(dfs$EvStroke),"status_hem_cph"]=0
dfs[dfs$typavc12==2&!is.na(dfs$typavc12), "status_hem_cph"]=1

write.csv(dfs, "../Data/OwnDataSets/Strokedat_N1670.csv")






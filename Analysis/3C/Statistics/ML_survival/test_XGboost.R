#survival analysis
library(randomForestSRC) 
library(caret)
library(xgboost)
source("/data/tu_fbeyer/R/x86_64-pc-linux-gnu-library/4.2.2/CoxBoost/CoxBoost-master/R/CoxBoost.R")
library(survival)
library(survcomp)
#library(SGL) (LASSO, group LASSO)
#parallel computing
library(foreach)
library(doParallel)
library(doRNG)  
#miscallaneous
library(psych)
library(R.utils)

load("/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Analysis/sparsePLSCox/simulated_data_WMLcomp_GMV.Rdata")
dCox$x.sex=as.factor(dCox$x.sex)
dCox$x.diabetes_y_n=as.factor(dCox$x.diabetes_y_n)
dCox$x.hypertension_composite_score=as.factor(dCox$x.hypertension_composite_score)
dCox$x.ApoE_Genotypebin=as.factor(dCox$x.ApoE_Genotypebin)


nrep=1
n_fold=7

#################################################################
#  Repeated double cross-validation function : result_sampling 
#################################################################
resampling_function = function(data,nb_iterations){
  
  set.seed(1)  #We fix a seed for reproducibility matters
  
  library(foreach)
  library(doParallel)
  library(doRNG)             
  
  #Initialization
  vec_list_var = c()
  vec_nb_var = c()
  iAUC = c()
  coeffs_var = c()
  ncores=16
  cl = makeCluster(ncores)                 #detectCores()       
  registerDoParallel(cl, cores = ncores)   #detectCores()


  result_sampling  =  foreach(i=1:nb_iterations) %dorng% {
  
  #library(R.utils)
    library(glmnet)  # Package to fit lasso/ela
    library(caret) #for creating balanced folds
    library(survival) #for Surv object
    library(survcomp) #for tdROC curve
    library(CoxBoost)
    source("/data/tu_fbeyer/R/x86_64-pc-linux-gnu-library/4.2.2/CoxBoost/CoxBoost-master/R/CoxBoost.R")
    
    #Initialization
    M=7 #number of outer folds
    n_fold=7 #number of inner CV folds for optimizing hyperparameters
    iAUC.vec = c()
    nb_var_vec = c()
    name_var = c()
    Coeffs_var = c()
    numericGroups=5
    
    #caret uses 5 groups for continuous variables-> split survival time into 5 bins
    data$time_bin=cut(data$time,unique(quantile(data$time, probs = seq(0, 1, length = numericGroups))),
                      include.lowest = TRUE)
    #split data into training and test folds (7/10)
    trainIndex <- createFolds(y=paste(data$status, data$time_bin), 
                                      k=M, 
                                      list = T, returnTrain = T
    )
  
    for (j in 1:M){
    
    fold=paste0("Fold",j)
  
    data.train = data[unlist(trainIndex[fold]),]
    data.test = data[-unlist(trainIndex[fold]),]
    x.train=data.train[,c(4:(length(data)-1))]
    time.train=data.train[,2]
    status.train=data.train[,3]
    x.test=data.test[,c(4:(length(data)-1))]
    time.test=data.test[,2]
    status.test=data.test[,3]
    
    #Begin inner loop
    param <- list(max_depth = 2, eta = 1, verbose = 0, nthread = 2,
                  objective = "survival:cox", eval_metric = "auc")
    bst <- xgb.train(param, dtrain, nrounds = 2, watchlist)
    
    booster = "gblinear".
    lambda: 0 -> 50
    
    #plot(coxboost_cv$mean.logplik)
    coxboost_train=CoxBoost(time=time.train,status=status.train,x=data.matrix(x.train),
             penalty=coxboost_cv$penalty,
             stepno=coxboost_cv$cv.res$optimal.step)
    
    coefs_coxboost = coefficients(coxboost_train)[which(coefficients(coxboost_train)!=0)]  #set of coefficients associated to lambda optimal
    names(coefs_coxboost) = names(coefficients(coxboost_train))[which(coefficients(coxboost_train)!=0)]
    nb_var_vec = c(nb_var_vec,length(coefs_coxboost))
    name_var = c(name_var,names(coefs_coxboost))
    
    #End inner loop
    
    #saving predictive score (= linear predictor of Cox model from boosting)
    pred_lp = predict.CoxBoost(coxboost_train,newdata=data.matrix(x.test),
                                type="lp")  
    
    #calculate AUC for 75% quantile of time
    tlong=quantile(data$time)[4]
    tdroc=survcomp::tdrocc(as.vector(pred_lp), time.test, status.test, time = tlong) 
    
    iAUC.vec=c(iAUC.vec,tdroc$AUC)
    
    #plot(x=1-tdroc$spec, y=tdroc$sens, type="l", xlab="1 - specificity",
    #     ylab="sensitivity", xlim=c(0, 1), ylim=c(0, 1))
    #lines(x=c(0,1), y=c(0,1), lty=3, col="red")
    }
    
    return(list(iAUC=iAUC.vec,Nb_var=nb_var_vec,Var_name=name_var, Coeffs_var=coefs_coxboost))
    
  }


#End outerloop
#Formatting the outputs
for(l in 1:nb_iterations){
  vec_list_var = c(vec_list_var,result_sampling[[l]]$Var_name)
  vec_nb_var = c(vec_nb_var,result_sampling[[l]]$Nb_var)
  iAUC = cbind(iAUC,mean(result_sampling[[l]]$iAUC))
  coeffs_var=c(coeffs_var, result_sampling[[l]]$Coeffs_var)
}

return(list(list_var=vec_list_var,nb_var=vec_nb_var,iAUC=iAUC, coeffs_var=coeffs_var))
}
  
  

coxboost_res=resampling_function(dCox, nrep)

# Obtaining variables selection frequency with a threshold = 0.6
sort(table(glmnet_res$list_var)/(nrep*n_fold),decreasing=T)
fqcy_60 = length(table(glmnet_res$list_var)[table(glmnet_res$list_var)/(nrep*n_fold)>=0.6])
list_60 =  names(table(glmnet_res$list_var)[table(glmnet_res$list_var)/(nrep*n_fold)>=0.6])


#variable selection behaves weirdly
#selvar=var.select(Surv(time,status)~., dCox[,c(2:ncol(dCox))], method = "md",
#                  ntree=1000, nsplit=10, nodesize=30, splitrule="logrank", 
#                  conservative = "medium")

#does not select most important predictors as defined by CV
#the difficulty of interpreting the importance/ranking of correlated variables
#is not random forest specific, but applies to most model based feature 
#selection methods. 


res=lm(status~., dCox)
vif_res=vif(res)
vif_res[vif_res== max(vif_res)]
vif_res

#simulate data for our case:

library(bigPLS)
library(dplyr)

#Use life-data set baseline values as ground truth
info=read.csv("/data/pt_life_whm/Results/Tables/final_usable_NNMF.csv" )
info_all=info[info$flair2MNI_reg_comment!="error",]

info_all$lesionvol=info_all$LST_vol_bl
info_all[is.na(info_all$lesionvol),"lesionvol"]=info_all[is.na(info_all$lesionvol),"vol_stable"]

#read loading information
L=read.table("/data/pt_life_whm/Results/NNMF/K4_binds_min23_fullsample/L.txt", sep=',') #/data/pt_life_whm/Results/NNMF/K5_min10_fullsample/L.txt", sep=',')
colnames(L)=c("C1","C2","C3","C4") #, "C5")
L$pseudonym=info_all$pseudonym

infa=merge(info_all,L)

#add information on gray matter thickness & subcort volumes
#subcortical & cortical volumes
subcort=read.csv("/data/gh_gr_agingandobesity_share/life_shared/Data/Preprocessed/derivatives/FreeSurfer/CortParc_SegResults_baseline/FS_results_subcor_LIFE.txt_renamed.csv", header=TRUE)

#recode hemispheres into long format
tidy_data = subcort %>% 
  select(c(5:8,12:13,15,23:29,66,70))%>%
  gather(region, value, -pseudonym)%>%
  separate(region, c("side", "region"))

sum_subcort = tidy_data %>% group_by(pseudonym, region) %>%
  summarize(mean=mean(value))  %>%
  spread(region, mean) 

colnames(sum_subcort)[9]="eTIV"
infa=merge(infa,sum_subcort)

#cortical regions
cort_rh=read.csv("/data/gh_gr_agingandobesity_share/life_shared/Data/Preprocessed/derivatives/FreeSurfer/CortParc_SegResults_baseline/FS_results_cor_LIFE_rh_thickness_renamed.csv", header=TRUE)
cort_lh=read.csv("/data/gh_gr_agingandobesity_share/life_shared/Data/Preprocessed/derivatives/FreeSurfer/CortParc_SegResults_baseline/FS_results_cor_LIFE_lh_thickness_renamed.csv", header=TRUE)

cort=merge(cort_rh, cort_lh)
tidy_data_c = cort %>% 
  select(c(3:37,39:72))%>%
  gather(region, value, -pseudonym)%>%
  separate(region, c("side", "region"))

sum_cort = tidy_data_c %>% group_by(pseudonym, region) %>%
  summarize(mean=mean(value))  %>%
  spread(region, mean) 

infa=merge(infa,sum_cort)

#add info on other covariates: education, APOE, diabetes, hypertension
lifec=read.csv("/data/gh_gr_agingandobesity_share/life_shared/Data/Preprocessed/derivatives/PV168_A1_Pilot_subject_list_inclusion_exclusion29.1.19.csv")
infa=merge(infa, lifec[,c("SIC", 
                          "hypertension_composite_score", 
                          "yearsofeducation",
                          "diabetes_y_n")], all.x=T)
infa$hypertension_composite_score=na_if(infa$hypertension_composite_score, 999999)
infa$hypertension_composite_score=as.factor(infa$hypertension_composite_score)

infa$diabetes_y_n=as.factor(infa$diabetes_y_n)
levels(infa$diabetes_y_n)=c(0,1)

infa$sex=as.factor(infa$sex)
levels(infa$sex)=c(0,1)


infa$ApoE_Genotypebin=recode(infa$ApoE_Genotype, "E2/E2" = "0", "E2/E3" = "0", "E2/E4" = "1",
                               "E3/E3" = "0", "E3/E4" = "1", "E4/E4" = "1",.default = NA_character_)
infa$ApoE_Genotypebin=as.factor(infa$ApoE_Genotypebin)

describe(infa[,c("Age_all",
                 "sex",
                 "yearsofeducation",
                 "diabetes_y_n",
                 "waist2hip",
                 "hypertension_composite_score", 
                 "ApoE_Genotypebin", 
                 "lesionvol",
                 "C1",
                 "Hippocampus",
                 "eTIV")])

#add hypothetical predictors (later)

x=select(infa, colnames(infa)[c(20:24,37:87)])
x=select(x,colnames(x)[c(2,1,54,4,53,55,56,6:52)])

#use only complete cases for simulation
x=x[complete.cases(x)&x$Age_all>60,]

#standardize continuous predictors
xc <- x %>% 
  mutate_at(c(1,3,4,8:54), ~(scale(.) %>% as.vector))


xc_m=data.matrix(xc)

xc_m=xc_m[,c(1:7)]

#regression coefficients
beta = rnorm(mean=0, sd=1, n = 54)

#meaningful coefficients:
beta_demo=c(1, #age
       -0.5, #sex: females (coded 0) are more at risk
       -0.25, #education
       0.3, # waist2hip
       0.9, #hypertension
       0.2, #diabetes
       0.2,#apoe)
       0) #do not take lesion volume into account

beta_wml_comp=c(0.01,0.01,0.08,0.01) #component 3 is the one with highest risk (deep WM)

beta_gmv=rnorm(mean=-0.04,sd=0.0001, n=42) #higher GMV is associated with lower risk
#stronger effects for some regions: 
#hippocampus: 16;  precuneus: 46, enthorinal: 25, posteriorcingulate: 44
#parahippocampal: 38
#no effect for eTIV: 20

colnames(x)[13:54]
beta_gmv[c(4,13, 26,32, 34)]=-0.08
beta_gmv[8]=0

beta=c(beta_demo,beta_wml_comp,beta_gmv)

#number of individuals
n=nrow(xc)

#parameter for hazard function
lambda = 3
rho = 2

cens.rate = 8 #in the Poisson sense : mean=1/rate (unit of time)

  
dCox<-bigPLS::dataCox(n = n, 
                lambda = lambda, 
                rho=rho, 
                beta=beta, 
                x=xc_m, 
                cens.rate = cens.rate)

setwd("/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Analysis/sparsePLSCox/")
save(dCox, "dCox", file="simulated_data_WMLcomp_GMV.Rdata")

?coxph
res.cox=coxph(Surv(time, status) ~ x.Age_all +
                           x.sex + 
                           x.waist2hip +
                           x.diabetes_y_n +
                           x.hypertension_composite_score + 
        x.yearsofeducation +
        x.ApoE_Genotypebin,
      data=dCox)


fit <- survfit(Surv(time, status) ~ x.sex, data = dCox)
autoplot(fit)

#test proportional hazards
test.ph <- cox.zph(res.cox)
ggcoxzph(test.ph)

#test influential cases
ggcoxdiagnostics(res.cox, type = "deviance",
                 linear.predictions = FALSE, ggtheme = theme_bw())

#test non-linearity -> there is non-linearity for age if applied to whole sample
ggcoxfunctional(Surv(time, status) ~ x.Age_all, data = dCox)

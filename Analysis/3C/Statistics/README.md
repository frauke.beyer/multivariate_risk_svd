
# Analysis Plan
## Mass-Univariate approach
- bring brain images into dataframe (row = participants, columns = voxels)
- apply mask with WM voxels only (probability > 0.9, for now JHU mask which is smaller) and voxels which have lesions in ~at least 5 participants (as in Lampe, 2019)~~
- rather use in at least 1% of participants (N=19) as in Neurology paper
- models to look at:
    1. res.cox1 <- coxph(survobj ~ vox1 + age + sex, data =  df
    1. res.cox1 <- coxph(survobj ~ vox1 + age + sex + totallesionvol, data =  df

- apply Coxmodel per column (res.cox1 <- coxph(survobj ~ vox1 + age + sex, data =  df)
- use parallel implementation
- identify regions which predict dementia (where RR of vox is above 1)

## Multivariate prediction using survival information
1. CoxPLS -> different models available in plsRCox but issue is interpretability when using more than one component (more than one component was estimated to be optimal by CV)

2. Lasso/elastic net
- [glmnet](https://cran.r-project.org/web/packages/glmnet/vignettes/Coxnet.pdf)
- [x] implemented
**regarding LASSO/ELASTIC NET/RIDGE REGRESSION** 
- L1 tends to shrink coefficients to zero (for only few predictors, LASSO) whereas L2 tends to shrink coefficients evenly (for collinear predictors, Ridge Regression). Thus Lasso can tend to randomly select one of collinear predictors from a set, while dropping the others. Both are linear! assuming linear relationship of predictors and hazard.

3. ranger (survival tree models, suitable for GWAS data) -> random survival forests & boosting in other packages

**regarding boosting and stuff**
- best performing model in [Spooner](https://link.springer.com/content/pdf/10.1038/s41598-020-77220-w.pdf): CoxBoost (feature selection through RF min. depth)
- XGBoost: For the MCI data, model performance was estimated using 10-fold cross-validation, dividing the survival data into 90%training and 10%testing sets. In Xgboost, the hyperparameters were estimated by further dividing the training set into 75%for learning and 25%for validation. We used the model trained on MCI data to stratify risk in the 100 CN subjects and computed the C-index for this cohort.

3. Illness/Death or Fine and Gray models:
- need few predictors (?? 10 ??), how to do that?

### Cross-validation scheme
like [Ajana 2019](https://github.com/SoufianeAjana/BLISAR/blob/master/methods/Lasso.R)
- for i=1:rep (100 times)
    split data into 10 folds
    for j=1:10
        use one fold as test, 9 folds as training data
        optimize model parameter LASSO? or also elastic net possible? in each training data (using leave-one-out CV or another k-fold CV)
        evaluate prediction error on each test fold


### Multivariate feature selection + modeling survival
1. NNMF with matlab script (validation through inflection point of the slope of the reconstruction error or largest spatial coverage with the lowest number of components)
- random half splits to show robustness (inner product of component pairs)
- use four (?) component loads in IllnessDeathModels

2. LinkedICA
- input both WML and GMV maps to LinkedICA
- use component loads in IDM

-> use loads with other predictors in LASSO/similar Coxmodel and propagate to IDM?

# Questions:
- what is the best follow-up time to take for Cox-model? All the data there is right?
- Topic of controlling for total lesion volume
- 15 events per predictor being considered adequate for fitting Coxmodel directly 
- how many events in 3C, so how many predictors can be adequately fitted at all?


# Project Outline
## Sample Selection
3C Dijon participants (N=4931) 
MRI:
T0: 1924 (39%)
T4: 1434 (29%)

Exclusion criteria: 
based on MRI:
- poor scan quality (N=) (Stephan et al.: N=123)
    - defined visually based on CAT report for analysis including GMV
    - defined visually based on warpedT2 + warped WML using slicesdir
- artefacts (N=) (Stephan et al.: N=123), check visually
- no WML maps available (N=122)

based on clinical assessment:
- prevalent dementia (Stephan et al.:N=8)
- individuals with missing dementia status over the 10 years of follow-up (Stephan et al.: n=71) -> is this relevant for survival analysis?


## Variables of interest
- age
- sex
- totallesionvol

For benchmark model (in line with Stephan et al., 2015)
- Sociodemographic factors included age, sex, and educational attainment. 
- Lifestyle factors included smoking and alcohol use.
- Functional assessment: Lawton and Brody scale for instrumental activities of daily living
- Cognition: mini-mental state examination, Benton visual retention test,39 and the digit span test
- Health variables: cardiovascular events (combining self reported history of myocardial infarction, coronary surgery, coronary angioplasty, surgery of the arteries in the legs, or stroke requiring admission to hospital), metabolic disease (diabetes; self reported, high glucose concentration ≥7.0 mmol/L, or receipt of hypoglycaemic treatment including oral diabetic drugs or insulin), and systolic blood pressure (continuous). 
- Genetic risk assessed apolipoprotein e4 status (coded as e4 positive v e4 negative).

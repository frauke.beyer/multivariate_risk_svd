#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Extracting bullseye parcellations from 3C data
Performs internal orientation correction ('FS_correction')
To run with Freesurfer/7.3.2 and FSL version fsl/6.0.4
Created on Wednesday June 7
@author: fbeyer

"""

from nipype import Node, Workflow, Function
from nipype.interfaces import fsl
from nipype.interfaces.utility import IdentityInterface
import nipype.interfaces.freesurfer as fs
import nipype.interfaces.utility as util
import nipype.interfaces.io as nio    
from bullseye_pipeline import create_bullseye_pipeline
from create_T1w_bullseye_reg_pipeline import create_T1w_bullseye_reg_pipeline
from utils import extract_parcellation
import numpy as np 
import nibabel as nb
import matplotlib
import matplotlib.pyplot as plt
import pandas as pd
import os



def create_bullseye_lesion(subjectlist):
    # workflow to extract bullseye parcellation in individual space for 3C


    # Specify the location of the preprocessed data    
    working_dir="/scratch/beyer/3C/BIDS/derivatives/bullseye/wd/" #MODIFY
    freesurfer_dir="/extra/3C/data/preprocessed/Dijon/Dijon1_FS7.3_T1/"
    flairdir="/scratch/beyer/3C/BIDS/"
    lesiondir="/scratch/beyer/3C/BIDS/derivatives/WML/"
    outdir="/scratch/beyer/3C/BIDS/derivatives/bullseye/WMparcellations_indiv/"

    os.environ['SUBJECTS_DIR'] = freesurfer_dir

    identitynode = Node(util.IdentityInterface(fields=['subject']),
                    name='identitynode')
    identitynode.iterables = ('subject', subjectlist)

    #Main workflow
    bullseye_lesion = Workflow(name="bullseyelesion_bbreg")
    bullseye_lesion.base_dir=working_dir
    
    #Bullseye WM segmentation part    
    bullseye=create_bullseye_pipeline()
    bullseye.inputs.inputnode.scans_dir=freesurfer_dir

    #Lesion registration and volume extraction part
    lesionreg=create_T1w_bullseye_reg_pipeline()#create_flairreg_pipeline()
    lesionreg.inputs.inputnode.freesurfer_dir=freesurfer_dir
    lesionreg.inputs.inputnode.flair_dir=flairdir
    lesionreg.inputs.inputnode.lesion_dir=lesiondir

    #extract parcellation 
    extractparc=Node(interface=util.Function(input_names=['in1_file', 'in2_file', 'subject_id'], output_names=['out_file'],
                                               function=extract_parcellation), name='extractparc')
    
    #Datasink
    datasink=Node(name="datasink", interface=nio.DataSink())
    datasink.inputs.base_directory = outdir
    datasink.inputs.substitutions = [('_subject_', ''),
                                     ('_mri_convert', 'FS_conversion')]

    bullseye_lesion.connect([
        (identitynode, bullseye, [("subject", "inputnode.subject_id")]),
        (identitynode, lesionreg,[( 'subject', 'inputnode.subject_id')]),
        (bullseye, lesionreg, [( 'outputnode.out_bullseye_wmparc', 'inputnode.bullseye')]),
        (lesionreg, extractparc, [( 'outputnode.bullseye_reorient', 'in2_file')]),
        (lesionreg, extractparc, [('outputnode.lesion2anat', 'in1_file')]),
        (bullseye, datasink,[( 'outputnode.out_bullseye_wmparc', '@bullseye')]),
        (lesionreg, datasink,[( 'outputnode.lesion2anat', '@lesion2anat')]),
        (lesionreg, datasink,[( 'outputnode.flair2anat', '@flair2anat')]),
        (lesionreg, datasink, [( 'outputnode.bullseye_reorient', '@bullseye_reorient')]),
        (lesionreg, datasink, [( 'outputnode.T1w_reorient', '@T1w_reorient')]),
        (identitynode, extractparc,[( 'subject', 'subject_id')]),
        (extractparc, datasink,[( 'out_file', '@lesionparc')]),
    ])
   
    
    
    return bullseye_lesion

## Prepare data file

bids=pd.read_csv('/scratch/beyer/3C/BIDS/participants.tsv',sep="\t")
#select everyone with T1 & WMH segmentation
select=np.logical_and(bids['time_point_0_WMH']==1,
                                bids['time_point_0_T1']==1)

#Select everyone without missegmentations, head motino issues
select=np.logical_and(select,bids['qa_time_point_0_WML']<=2)
#bids.qa_time_point_0_WML.value_counts()
#0      1514 ok
#2       253 left-right swap, fixed
#999     122 no WMH scan
#5        20 large missegmentation of WMH
#4         9 faulty WMH segmentation in ventricle area
#1         4 #large ventricles(brain atrophy), potentially exclude
#3         1 #strong head motion in MRI


#Select everyone with ok T1 image (none excluded based on this criterion)
select=np.logical_and(select,bids['qa_time_point_0_T1']!=2)
              
bids_usable=bids[select]
len(bids_usable)

#Exclude everyone where FreeSurfer went wrong
df=pd.read_csv('/extra/3C/data/preprocessed/Dijon/Dijon1_FS7.3_T1/CorticalMeasuresENIGMA_ThickAvg.csv', sep=',')
fs_usable=df[df["RThickness"] > 1] 
len(fs_usable)

final=pd.merge(bids_usable, fs_usable, left_on="participant_id_for_match", right_on= "SubjID", how="inner")

subj=final['participant_id'].values
subj_s=np.array([i[4:8] for i in subj])

subj_s=['1792']
#subj_s=['4930'] #0005 (special case), 0006 were run

bullseye_lesion=create_bullseye_lesion(subj_s)
bullseye_lesion.write_graph(graph2use='colored', simple_form=True)
bullseye_lesion.run(plugin='MultiProc', plugin_args={'n_procs' : 16})   #

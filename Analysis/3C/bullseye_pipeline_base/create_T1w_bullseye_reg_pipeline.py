from __future__ import division

import nipype.pipeline.engine as pe
from nipype import SelectFiles
import nipype.interfaces.utility as util
from nipype.interfaces.io import FreeSurferSource
from nipype.interfaces.freesurfer import MRIConvert
from nipype.interfaces.fsl.preprocess import FLIRT,ApplyXFM
from nipype.interfaces.fsl.maths import MathsCommand


from nipype import IdentityInterface, DataSink

# from .utils import *
from utils import *

import os

def create_T1w_bullseye_reg_pipeline():
    '''Pipeline to change orientation of FreeSurfers T1w and resulting
    bullseye parcellation and register to reoriented T2w image'''
    name='T1w_bullseye_reg'
    
    T1w_bullseye_reg = pe.Workflow(name=name)
    mode="cross"
    inputnode = pe.Node(interface=IdentityInterface(fields=['subject_id',
                                                            'flair_dir', 
                                                            'lesion_dir',
                                                            'freesurfer_dir', 
                                                            'work_dir', 
                                                            'bullseye']), name='inputnode')


    template_flair = {"FLAIR":"sub-{subject_id}/ses-t0/anat/sub-{subject_id}_ses-t0_T2w.nii"}
    template_lesion = {"LESION":"sub-{subject_id}/ses-t0/anat/{subject_id}_HSB_reo.nii.gz"} #remove _reo for test purposes           

    fileselector_flair = pe.Node(SelectFiles(template_flair), name='fileselect_flair')
    fileselector_lesion = pe.Node(SelectFiles(template_lesion), name='fileselect_lesion')
    
    #get FS image
    get_FS_T1w = pe.Node(FreeSurferSource(), name="get_FS_T1w")


    #make list of files to have orientation changed
    mlist= pe.Node(interface=util.Function(input_names=['in1', 'in2'], output_names=['out_list'],
                                                   function=make_list), name='mlist')

    ulist = pe.Node(interface=util.Function(input_names=['in_list'], output_names=['out1', 'out2'],
                                                   function=unlist), name='ulist')
        
    # bring mgh to nifti & change orientation
    mri_convert=pe.MapNode(MRIConvert(in_orientation='RIA', out_type = 'niigz'),
                                   iterfield='in_file', name="mri_convert") 


    #register T2 to FreeSurfer T1

    register_T2_T1 = pe.Node(FLIRT(dof=6, cost_func='mutualinfo'),
                             name="register_T2_T1")

    #Threshold WML to only accept value of 4
    #threshold_WML = pe.Node(MathsCommand(args="-uthr 4 -thr 4 -bin"), name="threshold_WML")
    
    #Apply transform to WML
    applyxfm_to_WML = pe.Node(ApplyXFM(apply_xfm=True), 
                              name="applyxfm_to_WML")



    # outputnode
    outputnode = pe.Node(IdentityInterface(fields=[
        'lesion2anat', "flair2anat", "bullseye_reorient", "T1w_reorient"]),
        name='outputnode')


    ##### CONNECTIONS #####
    #Select FLAIR & WML files:
    T1w_bullseye_reg.connect(inputnode        , 'subject_id',      fileselector_lesion,'subject_id')
    T1w_bullseye_reg.connect(inputnode        , 'lesion_dir',      fileselector_lesion,'base_directory')
    T1w_bullseye_reg.connect(inputnode        , 'subject_id',      fileselector_flair,'subject_id')
    T1w_bullseye_reg.connect(inputnode        , 'flair_dir',      fileselector_flair,'base_directory')
    #Select T1w and bullseye and transform
    T1w_bullseye_reg.connect(inputnode     , 'subject_id',         get_FS_T1w, 'subject_id')
    T1w_bullseye_reg.connect(inputnode     , 'freesurfer_dir',         get_FS_T1w, 'subjects_dir')
    T1w_bullseye_reg.connect(get_FS_T1w     , 'T1',        mlist, 'in1')
    T1w_bullseye_reg.connect(inputnode     , 'bullseye',    mlist, 'in2')
    T1w_bullseye_reg.connect(mlist     , 'out_list',    mri_convert, 'in_file')
    T1w_bullseye_reg.connect(mri_convert     , 'out_file',         ulist, 'in_list')
    #Coregister T1w and T2w and apply transform to lesions
    T1w_bullseye_reg.connect(ulist     , 'out1',         register_T2_T1, 'reference')
    T1w_bullseye_reg.connect(fileselector_flair     , 'FLAIR',         register_T2_T1, 'in_file')
    T1w_bullseye_reg.connect(register_T2_T1     , 'out_matrix_file',         applyxfm_to_WML, 'in_matrix_file')
    T1w_bullseye_reg.connect(register_T2_T1     , 'out_file',         outputnode, 'flair2anat')
    T1w_bullseye_reg.connect(ulist     , 'out1',         applyxfm_to_WML, 'reference')              
    T1w_bullseye_reg.connect(fileselector_lesion     , 'LESION',        applyxfm_to_WML, 'in_file')
    #T1w_bullseye_reg.connect(fileselector_lesion     , 'LESION',threshold_WML, 'in_file')
    #T1w_bullseye_reg.connect(threshold_WML, 'out_file',   applyxfm_to_WML, 'in_file')
    T1w_bullseye_reg.connect(applyxfm_to_WML, 'out_file',  outputnode,  'lesion2anat') 
    T1w_bullseye_reg.connect(ulist     , 'out2',         outputnode,  'bullseye_reorient') 
    T1w_bullseye_reg.connect(ulist     , 'out1',         outputnode,  'T1w_reorient') 

    return(T1w_bullseye_reg)

#Only for this subject '0005' there was an initial misalignment of T1w and T2w. 
#This was created by ME accidentally, and the following steps (using FSL) are not necessary anymore now. Anyhow, the final result (T2 and WML map in MNI space) is still accurate.

#Matlab coregister estimate & write did not work, so I used flirt with default parameters (cost function: corratio) to register T2w to T1
# flirt -in /scratch/beyer/3C/BIDS//sub-0005/ses-t0/anat/sub-0005_ses-t0_T2w.nii \
# -ref /scratch/beyer/3C/BIDS//sub-0005/ses-t0/anat/sub-0005_ses-t0_T1w.nii \
# -out /scratch/beyer/3C/BIDS//sub-0005/ses-t0/anat/sub-0005_ses-t0_T2w_to_T1w.nii \
# -omat /scratch/beyer/3C/BIDS//sub-0005/ses-t0/anat/sub-0005_ses-t0_T2w_to_T1w.mat

# cp sub-0005_ses-t0_T2w_to_T1w.nii.gz rnmi_sub-0005_ses-t0_T2w.nii.gz
# gunzip rnmi_sub-0005_ses-t0_T2w.nii.gz

#This transform was then applied to the WML map
# flirt -in /scratch/beyer/3C/BIDS//derivatives/WML/sub-0005/ses-t0/anat/sub-0005_ses-t0_WMH_map_reo.nii \
# -ref /scratch/beyer/3C/BIDS//sub-0005/ses-t0/anat/sub-0005_ses-t0_T1w.nii \
# -init /scratch/beyer/3C/BIDS//sub-0005/ses-t0/anat/sub-0005_ses-t0_T2w_to_T1w.mat \
# -out /scratch/beyer/3C/BIDS//derivatives/WML/sub-0005/ses-t0/anat/rnmi_sub-0005_ses-t0_WMH_map_reo.nii 

#Both resulting files were renamed & unzipped to fit SPM convention.

#SPM was used to bring map into MNI space
matlab;
%% Settings
addpath("/srv/shares/softs/spm12-full")
spm('defaults', 'FMRI');

matlabbatch{1}.spm.spatial.normalise.write.subj.def = {'/scratch/beyer/3C/BIDS/sub-0005/ses-t0/anat/y_sub-0005_ses-t0_T1w.nii'};
matlabbatch{1}.spm.spatial.normalise.write.subj.resample = {'/scratch/beyer/3C/BIDS//derivatives/WML/sub-0005/ses-t0/anat/rnmi_sub-0005_ses-t0_WMH_map_reo.nii'};
matlabbatch{1}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70
                                                        78 76 85];
matlabbatch{1}.spm.spatial.normalise.write.woptions.vox = [1.5 1.5 1.5];
matlabbatch{1}.spm.spatial.normalise.write.woptions.interp = 4;
matlabbatch{1}.spm.spatial.normalise.write.woptions.prefix = 'w';

matlabbatch{2}.spm.spatial.normalise.write.subj.def = {'/scratch/beyer/3C/BIDS/sub-0005/ses-t0/anat/y_sub-0005_ses-t0_T1w.nii'};
matlabbatch{2}.spm.spatial.normalise.write.subj.resample = {'/scratch/beyer/3C/BIDS/sub-0005/ses-t0/anat/rnmi_sub-0005_ses-t0_T2w.nii'};
matlabbatch{2}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70
                                                        78 76 85];
matlabbatch{2}.spm.spatial.normalise.write.woptions.vox = [1.5 1.5 1.5];
matlabbatch{2}.spm.spatial.normalise.write.woptions.interp = 4;
matlabbatch{2}.spm.spatial.normalise.write.woptions.prefix = 'w';

spm_jobman('run', matlabbatch);
%Extract TIV and total GMV
%% Settings
addpath("/srv/shares/softs/spm12-full")
spm('defaults', 'FMRI');
clear matlabbatch

%%Find all files to process with cross-sectional VBM
searchdir="/scratch/beyer/3C/BIDS/derivatives/CAT12_cross"
outdir="/scratch/beyer/3C/BIDS/derivatives/CAT12_cross/CAT12_tabular"


filePattern = sprintf('%s/sub-*/qa/cat_sub-*_ses-t0_T1w.xml', searchdir);
allFileInfo = dir(filePattern);

%%Process first three as a test
filesToProcess=allFileInfo %(1:2)

%%Format the input
InFiles=strcat({ filesToProcess.folder }, '/', { filesToProcess.name })
InFiles_tr=InFiles';

%-----------------------------------------------------------------------
% Job saved on 16-Feb-2023 14:42:29 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7771)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.tools.cat.tools.calcvol.data_xml = InFiles_tr;
matlabbatch{1}.spm.tools.cat.tools.calcvol.calcvol_TIV = 0;
matlabbatch{1}.spm.tools.cat.tools.calcvol.calcvol_name = outdir+'TIV.txt';

%% Run batch
spm_jobman('run', matlabbatch);
# -*- coding: utf-8 -*-
"""
Modified on Oct. 27 2022

@author: fbeyer
"""
import csv 
import os
import shutil
import re
import gzip
from nipype.algorithms.misc import Gzip

subjs=[]
line_count=0
with open("/scratch/beyer/3C/BIDS/participants.tsv") as subjfile:
    csv_reader = csv.reader(subjfile, delimiter='\t')
    for row in csv_reader:
        if (row[4]=="1"): #if T1 at timepoint 0 is available
            subjs.append(row[0]) #append sub-ID to be processed
      

basedir="/scratch/beyer/3C/BIDS/"
bids_outputdir="/scratch/beyer/3C/BIDS/derivatives/CAT12_cross/"

def create_bids(subjlist):

    import os
    import shutil
    import re


    for subj in subjlist:
                
        #create necessary folders and copy files.
        if not (os.path.isdir('%s%s/anat' %(bids_outputdir,subj))):
            os.makedirs('%s%s/anat' %(bids_outputdir,subj))
            os.makedirs('%s%s/qa' %(bids_outputdir,subj))

        if os.path.isfile('%s%s/ses-t0/anat/mwp1%s_ses-t0_T1w.nii' %(basedir,subj, subj)):  
            print("Copying all imaging files for %s" %subj)
            
            #with open('%s%s/ses-t0/anat/mwp1%s_ses-t0_T1w.nii' %(basedir,subj, subj), 'rb') as f_in:
            #    with gzip.open('%s%s/anat/%s_ses-t0_space-MNI305_desc-mwp1.nii.gz' %(bids_outputdir,subj, subj), 'wb') as f_out:
            #        shutil.copyfileobj(f_in, f_out)
            
            #Copy mwp1 without compressing because of CAT12 QA
            shutil.copy('%s%s/ses-t0/anat/mwp1%s_ses-t0_T1w.nii' %(basedir,subj, subj), '%s%s/anat/%s_ses-t0_space-MNI305_desc-mwp1.nii' %(bids_outputdir,subj, subj))
            os.remove('%s%s/ses-t0/anat/mwp1%s_ses-t0_T1w.nii' %(basedir,subj, subj))
            
            with open('%s%s/ses-t0/anat/mwp2%s_ses-t0_T1w.nii' %(basedir,subj, subj), 'rb') as f_in:
                with gzip.open('%s%s/anat/%s_ses-t0_space-MNI305_desc-mwp2.nii.gz' %(bids_outputdir,subj, subj), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            os.remove('%s%s/ses-t0/anat/mwp2%s_ses-t0_T1w.nii' %(basedir,subj, subj))
            
            with open('%s%s/ses-t0/anat/wm%s_ses-t0_T1w.nii' %(basedir,subj, subj), 'rb') as f_in:
                with gzip.open('%s%s/anat/%s_ses-t0_space-MNI305_desc-wmT1w.nii.gz' %(bids_outputdir,subj, subj), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            os.remove('%s%s/ses-t0/anat/wm%s_ses-t0_T1w.nii' %(basedir,subj, subj))
            
            with open('%s%s/ses-t0/anat/y_%s_ses-t0_T1w.nii' %(basedir,subj, subj), 'rb') as f_in:
                with gzip.open('%s%s/anat/%s_ses-t0_desc-warp.nii.gz' %(bids_outputdir,subj, subj), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            os.remove('%s%s/ses-t0/anat/y_%s_ses-t0_T1w.nii' %(basedir,subj, subj))
            
            with open('%s%s/ses-t0/anat/p0%s_ses-t0_T1w.nii' %(basedir,subj, subj), 'rb') as f_in:
                with gzip.open('%s%s/anat/%s_ses-t0_space-indivT1_desc-p0.nii.gz' %(bids_outputdir,subj, subj), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            os.remove('%s%s/ses-t0/anat/p0%s_ses-t0_T1w.nii' %(basedir,subj, subj))

            shutil.copy("%s%s/ses-t0/anat/catROI_%s_ses-t0_T1w.mat" %(basedir,subj, subj), '%s%s/anat' %(bids_outputdir,subj)) 
            os.remove("%s%s/ses-t0/anat/catROI_%s_ses-t0_T1w.mat" %(basedir,subj, subj))  
            shutil.copy("%s%s/ses-t0/anat/catROI_%s_ses-t0_T1w.xml" %(basedir,subj, subj), '%s%s/anat' %(bids_outputdir,subj))   
            os.remove("%s%s/ses-t0/anat/catROI_%s_ses-t0_T1w.xml" %(basedir,subj, subj))  
            
        if os.path.isfile('%s%s/ses-t0/anat/rnmi_%s_ses-t0_T2w.nii' %(basedir,subj, subj)):  
            print("Copying all T2 related imaging files for %s" %subj)

            with open('%s%s/ses-t0/anat/rnmi_%s_ses-t0_T2w.nii' %(basedir,subj, subj), 'rb') as f_in:
                with gzip.open('%s%s/anat/%s_ses-t0_space-indivT1_desc-alignT2w.nii.gz' %(bids_outputdir,subj, subj), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)            
            os.remove('%s%s/ses-t0/anat/rnmi_%s_ses-t0_T2w.nii' %(basedir,subj, subj))
            
            with open('%s%s/ses-t0/anat/wrnmi_%s_ses-t0_T2w.nii' %(basedir,subj, subj), 'rb') as f_in:
                with gzip.open('%s%s/anat/%s_ses-t0_space-MNI305_desc-warpT2w.nii.gz' %(bids_outputdir,subj, subj), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)     
            os.remove('%s%s/ses-t0/anat/wrnmi_%s_ses-t0_T2w.nii' %(basedir,subj, subj))
            

        if os.path.isfile('%s%s/ses-t0/anat/catlog_%s_ses-t0_T1w.nii.txt' %(basedir,subj, subj)):  
            print("Copyin or removing all CAT quality files for %s" %subj)
            shutil.copy("%s%s/ses-t0/anat/catlog_%s_ses-t0_T1w.nii.txt" %(basedir,subj, subj), '%s%s/qa' %(bids_outputdir,subj))
            shutil.copy("%s%s/ses-t0/anat/catreport_%s_ses-t0_T1w.pdf" %(basedir,subj, subj), '%s%s/qa' %(bids_outputdir,subj))
            shutil.copy("%s%s/ses-t0/anat/cat_%s_ses-t0_T1w.xml" %(basedir,subj, subj), '%s%s/qa' %(bids_outputdir,subj))
            shutil.copy("%s%s/ses-t0/anat/cat_%s_ses-t0_T1w.mat" %(basedir,subj, subj), '%s%s/qa' %(bids_outputdir,subj))
            
            os.remove("%s%s/ses-t0/anat/catreportj_%s_ses-t0_T1w.jpg" %(basedir,subj, subj))
            os.remove("%s%s/ses-t0/anat/catreport_%s_ses-t0_T1w.pdf" %(basedir,subj, subj))
            os.remove("%s%s/ses-t0/anat/cat_%s_ses-t0_T1w.xml" %(basedir,subj, subj))
            os.remove("%s%s/ses-t0/anat/cat_%s_ses-t0_T1w.mat" %(basedir,subj, subj))
            os.remove("%s%s/ses-t0/anat/catlog_%s_ses-t0_T1w.nii.txt" %(basedir,subj, subj))


     


   

    return 1


#run copying from subjs[769] == sub-1378 on:
create_bids(subjs[1801:])




# import json
# data = {'Name': '3C anatomical data and WML', 'BIDSVersion': "1.7.0"}
# with open('/scratch/beyer/3C/BIDS/dataset_description.json', 'w') as ff:
#     json.dump(data, ff)


# data = {'participant_id_for_match': 'for matching with clinical data', 
# 'time_point_0_WMH': "whether data is available",
# 'time_point_1_WMH': "whether data is available",
# 'time_point_0_T1': "whether data is available",
# 'time_point_1_T1': "whether data is available",
# 'time_point_0_T2': "whether data is available",
# 'time_point_1_T2': "whether data is available"}
# with open('/scratch/beyer/3C/BIDS/participants.json', 'w') as ff:
#     json.dump(data, ff)


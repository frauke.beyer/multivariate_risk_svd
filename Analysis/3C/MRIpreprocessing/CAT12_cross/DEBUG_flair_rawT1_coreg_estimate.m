%Script used to correct wrong registration which occured in sub-0015 (probably because it was a training subject)

%% Settings
addpath("/srv/shares/softs/spm12-full")
spm('defaults', 'FMRI');

%%All participants with T0 WML maps
fid = fopen("/scratch/beyer/3C/BIDS/participants.tsv");
C = textscan(fid, '%s %s %f %f %f %f %f %f', 'HeaderLines', 1);
fclose(fid);

SubjsWML=C{2}(C{3}==1);

%% Define folders and filenames for SPM batch
searchdir="/scratch/beyer/3C/BIDS/"

WML_templ = strcat(searchdir,'/derivatives/WML/sub-%s/ses-t0/anat/sub-%s_ses-t0_space-indivT1_desc-nmireoWMH.nii');
WMLreo_templ = strcat(searchdir,'/derivatives/WML/sub-%s/ses-t0/anat/rnmi_sub-%s_ses-t0_space-indivT1_desc-nmireoWMH.nii');
T2_templ = strcat(searchdir, '/sub-%s/ses-t0/anat/sub-%s_ses-t0_T2w.nii');
T2reo_templ=strcat(searchdir, '/sub-%s/ses-t0/anat/rnmi_2%sDD_T2_reo.nii');
T1_templ = strcat(searchdir, '/sub-%s/ses-t0/anat/sub-%s_ses-t0_T1w.nii');
Def_templ= strcat(searchdir, 'derivatives/CAT12_cross/sub-%s/anat/sub-%s_ses-t0_desc-warp.nii');

WML_files={};
T2_files={};
T1_files={};
WMLreo_files={};
T2reo_files={};
Def_files={};

SubjsWML={'0015'}

for i=1:numel(SubjsWML)
    %have to loop over subjects to make spm coreg/estwrite work
    %in case of first participant ('0005') T1w and T2w did not overlay initially and were coregistered with flirt default options + spm normalizem
    %outputs look like from any other subject
    
    clear matlabbatch

    WML_files=cellstr(sprintf(WML_templ, SubjsWML{i},SubjsWML{i}));
    WMLreo_files=cellstr(sprintf(WMLreo_templ, SubjsWML{i},SubjsWML{i}));
    T2_files=cellstr(sprintf(T2_templ, SubjsWML{i},SubjsWML{i}));
    T2reo_files=cellstr(sprintf(T2reo_templ, SubjsWML{i},SubjsWML{i}));
    T1_files=cellstr(sprintf(T1_templ, SubjsWML{i},SubjsWML{i}));
    Def_files=cellstr(sprintf(Def_templ, SubjsWML{i},SubjsWML{i}));
    %-----------------------------------------------------------------------
    % Job saved on 27-Oct-2022 13:38:27 by cfg_util (rev $Rev: 6942 $)
    % spm SPM - SPM12 (7219)
    % cfg_basicio BasicIO - Unknown
    %-----------------------------------------------------------------------
    matlabbatch{1}.spm.spatial.coreg.estwrite.ref = T1_files;
    matlabbatch{1}.spm.spatial.coreg.estwrite.source = T2_files;
    matlabbatch{1}.spm.spatial.coreg.estwrite.other = WML_files;
    matlabbatch{1}.spm.spatial.coreg.estwrite.eoptions.cost_fun = 'nmi';
    matlabbatch{1}.spm.spatial.coreg.estwrite.eoptions.sep = [4 2];
    matlabbatch{1}.spm.spatial.coreg.estwrite.eoptions.tol = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
    matlabbatch{1}.spm.spatial.coreg.estwrite.eoptions.fwhm = [7 7];
    matlabbatch{1}.spm.spatial.coreg.estwrite.roptions.interp = 4;
    matlabbatch{1}.spm.spatial.coreg.estwrite.roptions.wrap = [0 0 0];
    matlabbatch{1}.spm.spatial.coreg.estwrite.roptions.mask = 0;
    matlabbatch{1}.spm.spatial.coreg.estwrite.roptions.prefix = 'rnmi_'; 

    matlabbatch{2}.spm.spatial.normalise.write.subj.def = Def_files;
    matlabbatch{2}.spm.spatial.normalise.write.subj.resample = WMLreo_files;
    matlabbatch{2}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70
                                                             78 76 85]; 
    matlabbatch{2}.spm.spatial.normalise.write.woptions.vox = [1.5 1.5 1.5];
    matlabbatch{2}.spm.spatial.normalise.write.woptions.interp = 4;
    matlabbatch{2}.spm.spatial.normalise.write.woptions.prefix = 'w';

    matlabbatch{3}.spm.spatial.normalise.write.subj.def = Def_files;
    matlabbatch{3}.spm.spatial.normalise.write.subj.resample = T2reo_files;
    matlabbatch{3}.spm.spatial.normalise.write.woptions.bb = [-78 -112 -70
                                                             78 76 85];
    matlabbatch{3}.spm.spatial.normalise.write.woptions.vox = [1.5 1.5 1.5];
    matlabbatch{3}.spm.spatial.normalise.write.woptions.interp = 4;
    matlabbatch{3}.spm.spatial.normalise.write.woptions.prefix = 'w'; 

    %% Run batch
    spm_jobman('run', matlabbatch);
end




% List of open inputs
% Estimate volumes inside ROI: XML files - cfg_files
% Estimate TIV and global tissue volumes: XML files - cfg_files
nrun = X; % enter the number of runs here
jobfile = {'/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Analysis/MRIpreprocessing/CAT12_cross/extract_TIV_job.m'};
jobs = repmat(jobfile, 1, nrun);
inputs = cell(2, nrun);
for crun = 1:nrun
    inputs{1, crun} = MATLAB_CODE_TO_FILL_INPUT; % Estimate volumes inside ROI: XML files - cfg_files
    inputs{2, crun} = MATLAB_CODE_TO_FILL_INPUT; % Estimate TIV and global tissue volumes: XML files - cfg_files
end
spm('defaults', 'FMRI');
spm_jobman('run', jobs, inputs{:});

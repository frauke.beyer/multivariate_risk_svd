%Check data quality for cross-sectional CAT

%% Settings
addpath("/srv/shares/softs/spm12-full")
spm('defaults', 'FMRI');
clear matlabbatch

%%Find all files to qa check with cross-sectional VBM
searchdir="/scratch/beyer/3C/BIDS/derivatives/CAT12_cross/"

filePattern = sprintf('%s/*/anat/*_ses-t0_space-MNI305_desc-mwp1.nii', searchdir);
allFileInfo = dir(filePattern);

%%Format the input
InFiles=cellstr(strcat({ allFileInfo.folder }, '/', { allFileInfo.name }))
InFiles_tr=transpose(InFiles)

filePattern = sprintf('%s/*/qa/*.xml', searchdir);
allFileInfo = dir(filePattern);

%%Format the input
xmlFiles=cellstr(strcat({ allFileInfo.folder }, '/', { allFileInfo.name }))
xmlFiles_tr=transpose(xmlFiles)

%-----------------------------------------------------------------------
% quality control for large samples
%-----------------------------------------------------------------------
matlabbatch{1}.spm.tools.cat.tools.quality_measures.data = InFiles_tr;
matlabbatch{1}.spm.tools.cat.tools.quality_measures.globals = 0;
matlabbatch{1}.spm.tools.cat.tools.quality_measures.csv_name = '/scratch/beyer/3C/BIDS/derivatives/CAT12_cross/Quality_measures.csv';

%-----------------------------------------------------------------------
% calculate TIV
%-----------------------------------------------------------------------
matlabbatch{2}.spm.tools.cat.tools.calcvol.data_xml = xmlFiles_tr;
matlabbatch{2}.spm.tools.cat.tools.calcvol.calcvol_TIV = 1;
matlabbatch{2}.spm.tools.cat.tools.calcvol.calcvol_savenames = 1;
matlabbatch{2}.spm.tools.cat.tools.calcvol.calcvol_name = '/scratch/beyer/3C/BIDS/derivatives/CAT12_cross/TIV.txt';

%-----------------------------------------------------------------------
% regular quality control 
%-----------------------------------------------------------------------
%matlabbatch{3}.spm.tools.cat.tools.check_cov.data_vol = {};
%matlabbatch{3}.spm.tools.cat.tools.check_cov.data_xml = {''};
%matlabbatch{3}.spm.tools.cat.tools.check_cov.gap = 3;
%matlabbatch{3}.spm.tools.cat.tools.check_cov.c = {};

%% Run batch
spm_jobman('run', matlabbatch);

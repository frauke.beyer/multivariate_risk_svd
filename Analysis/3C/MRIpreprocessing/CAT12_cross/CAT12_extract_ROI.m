
%Extract GMV from neuromorphometrics atlas

%% Settings
addpath("/srv/shares/softs/spm12-full")
spm('defaults', 'FMRI');
clear matlabbatch

%%Find all files to process with cross-sectional VBM
searchdir="/scratch/beyer/3C/BIDS/derivatives/CAT12_cross"
outdir=cellstr("/scratch/beyer/3C/BIDS/derivatives/CAT12_cross/CAT12_tabular")


filePattern = sprintf('%s/sub-*/anat/catROI_sub-*_ses-t0_T1w.xml', searchdir);
allFileInfo = dir(filePattern);

%%Process first three as a test
filesToProcess=allFileInfo %(1:2)

%%Format the input
InFiles=strcat({ filesToProcess.folder }, '/', { filesToProcess.name })
InFiles_tr=InFiles';
%-----------------------------------------------------------------------
% Job Definition
%-----------------------------------------------------------------------

matlabbatch{1}.spm.tools.cat.tools.calcroi.roi_xml = InFiles_tr;
matlabbatch{1}.spm.tools.cat.tools.calcroi.point = '.';
matlabbatch{1}.spm.tools.cat.tools.calcroi.outdir = outdir;
matlabbatch{1}.spm.tools.cat.tools.calcroi.calcroi_name = 'ROI';

%% Run batch
spm_jobman('run', matlabbatch);

cd /scratch/beyer/3C/BIDS/derivatives/WML/merged/

fslsplit mergedfile_N1781_space-MNI_res-1.5.nii.gz

for elem in `ls vol*`
do
echo $elem
#first binarize 1mm images to have more appropriate values after downsampling (which can be thresholded at 0.2)
#fslmaths $elem -bin $elem

flirt -in $elem -ref  $elem \
-applyisoxfm 3 -out $elem
done

fslmerge -t mergedfile_N1781_space-MNI_res-3.nii.gz `ls vol*`

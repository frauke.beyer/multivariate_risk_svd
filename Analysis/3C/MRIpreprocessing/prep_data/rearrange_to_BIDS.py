# -*- coding: utf-8 -*-
"""
Modified on Oct. 27 2022

@author: fbeyer
"""
import csv 
import os
import shutil
import re

subjs=[]
with open("/scratch/beyer/3C/tables/data_info.csv") as subjfile:
    csv_reader = csv.reader(subjfile, delimiter=',')
    line_count = 0
    for row in csv_reader:
        subjs.append(row[1])

subjs=subjs[1:]

def create_bids(subjlist):

    import os
    import shutil
    import re


    for subj in subjlist:
        pseudo=subj[1:]
        for seq in ['T1','T2','WMH']:
            if seq=='WMH':
                    basedir="/scratch/beyer/3C/"
                    suffix="hsbfinal"
                    bids_outputdir="/scratch/beyer/3C/BIDS/preprocessed/"
                    subj=subj[1:]             
            elif seq=="T1":
                    basedir="/scratch/beyer/3C/raw_data/"
                    bids_outputdir="/scratch/beyer/3C/BIDS/"
                    suffix="DD_reo"
            else:
                    basedir="/scratch/beyer/3C/raw_data/"
                    bids_outputdir="/scratch/beyer/3C/BIDS/"
                    suffix="DD_T2_reo"

            for ses in ['0', '1']:

                           
                if (seq=="T1")&(ses=="0"):
                    suffix="DD_reo"
                elif (seq=="T1")&(ses=="1"):
                    suffix="DD2_reo"
                if (seq=="T2")&(ses=="0"):
                    suffix="DD_T2_reo"
                elif (seq=="T2")&(ses=="1"):
                    suffix="DD2_T2_rawseg"
         
                print('%s/%s/timepoint%s/%s%s.nii.gz with pseudonym%s' %(basedir,seq,ses,subj,suffix,pseudo))

                #create necessary folders and copy files.
                #if not (os.path.isdir('%ssub-%s/ses-t%s/anat' %(bids_outputdir,pseudo,ses))):
                #    os.makedirs('%ssub-%s/ses-t%s/anat' %(bids_outputdir,pseudo,ses))
    
                #if os.path.isfile('%s/%s/timepoint%s/%s%s.nii.gz' %(basedir,seq,ses,subj,suffix)):  
                #    print("Copying file")
                #    shutil.copyfile('%s/%s/timepoint%s/%s%s.nii.gz' %(basedir,seq,ses,subj,suffix), '%ssub-%s/ses-t%s/anat/sub-%s_ses-t%s_%sw.nii.gz' %(bids_outputdir,pseudo,ses,pseudo,ses,seq))  
                #else:
                #    print("Error:File %s/%s/timepoint%s/%s%s.nii.gz does not exist" %(basedir,seq,ses,subj,suffix))
                #    continue

                # correct wrong sequence names for T1 and T2
                if (seq!="WML"):
                    if os.path.isfile('%ssub-%s/ses-t%s/anat/sub-%s_ses-t%s_%s.nii.gz' %(bids_outputdir,pseudo,ses,pseudo,ses,seq)):
                        print("renaming %s for sequence %s and timepoint %s" %(pseudo, seq, ses))
                        os.rename('%ssub-%s/ses-t%s/anat/sub-%s_ses-t%s_%s.nii.gz' %(bids_outputdir,pseudo,ses,pseudo,ses,seq),'%ssub-%s/ses-t%s/anat/sub-%s_ses-t%s_%sw.nii.gz' %(bids_outputdir,pseudo,ses,pseudo,ses,seq))
    return 1
       
create_bids(subjs)




import json
data = {'Name': '3C anatomical data and WML', 'BIDSVersion': "1.7.0"}
with open('/scratch/beyer/3C/BIDS/dataset_description.json', 'w') as ff:
    json.dump(data, ff)


data = {'participant_id_for_match': 'for matching with clinical data', 
'time_point_0_WMH': "whether data is available",
'time_point_1_WMH': "whether data is available",
'time_point_0_T1': "whether data is available",
'time_point_1_T1': "whether data is available",
'time_point_0_T2': "whether data is available",
'time_point_1_T2': "whether data is available"}
with open('/scratch/beyer/3C/BIDS/participants.json', 'w') as ff:
    json.dump(data, ff)


# -*- coding: utf-8 -*-
"""
Modified on Oct. 27 2022

@author: fbeyer
"""
import csv 
import os
import shutil
import re
import gzip
import os



sfile=""
with open('/scratch/beyer/3C/BIDS/participants.tsv') as subjfile:
    csv_reader = csv.reader(subjfile, delimiter='\t')
    line_count = 0
    for row in csv_reader:
        if (row[2]=='1'):
            if (row[9]!='5'): #merge all subjects who are not labeled 5 or 999
                print("subject %s is merged" %(row[1]))
                sfile+=" /scratch/beyer/3C/BIDS/derivatives/CAT12_cross/%s/anat/%s_ses-t0_space-MNI305_desc-mwp1.nii" %(row[0],row[0])
        else:
            print("subject %s does not have WMH at t0" %(row[1]))


text_file = open("sfile.txt", "w")
n = text_file.write(sfile)
text_file.close()


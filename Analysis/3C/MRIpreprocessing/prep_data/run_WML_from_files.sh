
#!/bash

for elem in ls /scratch/beyer/3C/BIDS/derivatives/bullseye/wd/bullseyelesion_bbreg/T1w_bullseye_reg/_subject_*/threshold_WML/sub-*_ses-t0_WMHw_reo_maths.nii.gz 
do

fslstats $elem -v -V >> /scratch/beyer/3C/BIDS/derivatives/bullseye/WML_vox_vol_from_im.txt

done
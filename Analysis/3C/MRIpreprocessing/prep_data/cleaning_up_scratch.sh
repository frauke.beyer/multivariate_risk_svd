#!/bin/bash


## REMOVE files for everyone who has 
var=1
while read subj
do
    if [ -f /scratch/fbeyer/3C/BIDS/derivatives/WML/sub-$subj/ses-t0/anat/wrnmi_${subj}_HSB_reo.nii ];
    then echo "outcome $subj exists"
    var=$((var + 1))
    rm -rf /scratch/fbeyer/3C/BIDS/derivatives/WML/sub-$subj/ses-t0/anat/${subj}_HSB_reo.nii
    rm -rf /scratch/fbeyer/3C/BIDS/derivatives/WML/sub-$subj/ses-t0/anat/rnmi_${subj}_HSB_reo.nii
    rm -rf /scratch/fbeyer/3C/BIDS/derivatives/CAT12_cross/sub-$subj/anat/
    rm -rf /scratch/fbeyer/3C/BIDS/sub-$subj/ses-t0/anat/

    gzip /scratch/fbeyer/3C/BIDS/derivatives/WML/sub-$subj/ses-t0/anat/wrnmi_${subj}_HSB_reo.nii

    else
    echo "outcome $subj does not exist yet, leaving files"
    fi

done < subjlist.txt


echo $var
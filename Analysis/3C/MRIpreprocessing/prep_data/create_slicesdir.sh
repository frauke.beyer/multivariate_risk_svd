
# Some code snippets in bash to do small things

#Create different overview files to check for quality
# modulated warped T1
cd /scratch/beyer/3C/BIDS/derivatives/CAT12_cross
slicesdir `ls */anat/*_ses-t0_space-MNI305_desc-mwp1.nii`

#WML with underlying warpT2w (there is an issue with slicesdir because of NaN values from warped SPM (https://www.jiscmail.ac.uk/cgi-bin/wa-jisc.exe?A2=ind1808&L=SPM&P=R75434)
#results in /scratch/beyer/3C/BIDS/derivatives/WML/slicesdir_T2_WML_MNIspace
wd=/scratch/beyer/3C/BIDS/derivatives/
cd $wd/WML
echo -n slicesdir -o  >> for_slicesdir.sh
for subj in `ls -d sub*`
do
if [ -f $wd/WML/$subj/ses-t0/anat/wrnmi_${subj}_ses-t0_WMH_map_reo.nii ];
then
echo -n $wd/CAT12_cross/$subj/anat/${subj}_ses-t0_space-MNI305_desc-warpT2w.nii.gz \
$wd/WML/$subj/ses-t0/anat/wrnmi_${subj}_ses-t0_WMH_map_reo.nii >> for_slicesdir.sh
else 
echo no WMH map
fi
done 
chmod +x for_slicesdir.sh

#overlay T2w and WML file in native space to check for alignment (in /scratch/beyer/3C/BIDS/derivatives/WML/slicesdir_T2_WML_nativespace)
wd=/scratch/beyer/3C/BIDS/derivatives
cd $wd/WML
echo -n slicesdir -o  >> for_slicesdir_reo.sh
for subj in `ls -d sub*`
do
if [ -f $wd/WML/$subj/ses-t0/anat/${subj}_ses-t0_WMHw_reo.nii.gz ];
then
echo copying to file
echo -n /scratch/beyer/3C/BIDS/$subj/ses-t0/anat/${subj}_ses-t0_T2w.nii \
$wd/WML/$subj/ses-t0/anat/${subj}_ses-t0_WMHw_reo.nii.gz >> for_slicesdir_reo.sh
else 
echo no WMH map
fi
done 
chmod +x for_slicesdir_reo.sh

# After correction only check those who have been realigned without flipping.
#results in /scratch/beyer/3C/BIDS/derivatives/WML/slicesdir_onlycorrected
wd=/scratch/beyer/3C/BIDS/derivatives/
cd $wd/WML
echo -n "slicesdir -o " >> for_slicesdir_reo_after_correction.sh
for subj in sub-1431 sub-2171 sub-2410 sub-2441 sub-2472 sub-2494 sub-2495 sub-2498 sub-2586 sub-2591 sub-2599 sub-2600 sub-2621 sub-2631 sub-2658 sub-2664 sub-2665 sub-2671 sub-2690 sub-2694 sub-2697 sub-2700 sub-2701 sub-2708 \
sub-2709 sub-2710 sub-2711 sub-2712 sub-2747 sub-2770 sub-2771 sub-2787 sub-2789 sub-2790 sub-2795 sub-2800 sub-2815 sub-2825 sub-2826 sub-2851 sub-2865 sub-2866 sub-2871 sub-2872 sub-2873 sub-2875 sub-2876 sub-2884 \
sub-2885 sub-2892 sub-2897 sub-2898 sub-2901 sub-2916 sub-2919 sub-2920 sub-2922 sub-2926 sub-2931 sub-2932 sub-2936 sub-2943 sub-2944 sub-2951 sub-2960 sub-2962 sub-2965 sub-2978 sub-2979 sub-2980 sub-2981 sub-2982 \
sub-2983 sub-2984 sub-2987 sub-2990 sub-2991 sub-2994 sub-2995 sub-2996 sub-2997 sub-3002 sub-3003 sub-3018 sub-3019 sub-3025 sub-3033 sub-3036 sub-3041 sub-3042 sub-3044 sub-3045 sub-3051 sub-3064 sub-3065 sub-3069 \
sub-3078 sub-3080 sub-3088 sub-3092 sub-3102 sub-3107 sub-3113 sub-3115 sub-3116 sub-3117 sub-3120 sub-3122 sub-3123 sub-3133 sub-3148 sub-3151 sub-3153 sub-3155 sub-3157 sub-3158 sub-3164 sub-3171 sub-3173 sub-3174 \
sub-3177 sub-3181 sub-3182 sub-3183 sub-3188 sub-3196 sub-3197 sub-3198 sub-3199 sub-3207 sub-3208 sub-3215 sub-3217 sub-3225 sub-3226 sub-3228 sub-3231 sub-3232 sub-3233 sub-3234 sub-3238 sub-3239 sub-3246 sub-3247 \
sub-3248 sub-3262 sub-3265 sub-3267 sub-3270 sub-3271 sub-3280 sub-3290 sub-3294 sub-3300 sub-3302 sub-3303 sub-3305 sub-3309 sub-3317 sub-3320 sub-3330 sub-3331 sub-3335 sub-3336 sub-3342 sub-3347 sub-3349 sub-3350 \
sub-3357 sub-3360 sub-3362 sub-3364 sub-3369 sub-3372 sub-3379 sub-3380 sub-3381 sub-3382 sub-3387 sub-3388 sub-3389 sub-3390 sub-3393 sub-3410 sub-3413 sub-3414 sub-3415 sub-3416 sub-3417 sub-3420 sub-3422 sub-3424 \
sub-3425 sub-3426 sub-3429 sub-3436 sub-3453 sub-3454 sub-3459 sub-3460 sub-3461 sub-3469 sub-3473 sub-3475 sub-3486 sub-3532 sub-3533 sub-3534 sub-3565 sub-3600 sub-3605 sub-3622 sub-3667 sub-3687 sub-3726 sub-3730 \
sub-3752 sub-3771 sub-3818 sub-3827 sub-3902 sub-3989 sub-4066 sub-4078 sub-4082 sub-4084 sub-4307 sub-4315 sub-4331 sub-4401 sub-4402 sub-4427 sub-4433 sub-4434 sub-4458 sub-4551 sub-4558 sub-4595 sub-4628 sub-4640 \
sub-4641 sub-4652 sub-4676 sub-4688 sub-4701 sub-4707 sub-4724 sub-4729 sub-4736 sub-4738 sub-4747 sub-4777 sub-4788 sub-4793 sub-4817 sub-4843 sub-4864 sub-4870 sub-4871 sub-4872 sub-4873 sub-4884 sub-4930 sub-1692 sub-2042
do
if [ -f $wd/WML/$subj/ses-t0/anat/${subj}_ses-t0_WMHw_reo.nii.gz ];
then
echo copying to file
echo -n /scratch/beyer/3C/BIDS/$subj/ses-t0/anat/${subj}_ses-t0_T2w.nii \
"$wd/WML/$subj/ses-t0/anat/${subj}_ses-t0_WMHw_reo.nii.gz " >> for_slicesdir_reo_after_correction.sh
else 
echo no WMH map
fi
done 
chmod +x for_slicesdir_reo_after_correction.sh

#g(un)zip warp for subjects who were corrected
for subj in sub-2327 sub-2332 sub-2499 sub-2663 sub-2905 sub-2907 sub-2908
do
gunzip /scratch/beyer/3C/BIDS/derivatives/CAT12_cross/${subj}/anat/${subj}_ses-t0_desc-warp.nii
done


## Create WM mask in correct space (1.5mm isotropic)

#downsample MNI WM template (1mm & 128x152x128) to specific WML image dimension & resolution (1.5mm & 105x127x105)
#it does not matter which subject we use here, as it is assumed that images are in the same space and only header information not intensity is used
#https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT/FAQ#Can_I_register_to_an_image_but_use_higher.2Flower_resolution_.28voxel_size.29.3F
flirt -in mni_icbm152_wm_tal_nlin_asym_09c.nii -ref  /scratch/beyer/3C/BIDS/derivatives/CAT12_cross/sub-0005/anat/sub-0005_ses-t0_space-MNI305_desc-warpT2w.nii.gz -applyxfm -usesqform -out mni_icbm152_wm_tal_nlin_asym_09c_15mm_sub005_applyxsqform.nii

#thresholded map at 0.8 WM probability and binarized
fslmaths mni_icbm152_wm_tal_nlin_asym_09c_15mm_sub005_applyxsqform.nii -thr 0.8 -bin mni_icbm152_wm_tal_nlin_asym_09c_15mm_sub005_applyxsqform_thr0.8_bin.nii

#general brain mask
flirt -in mni_icbm152_t1_tal_nlin_asym_09c_mask.nii -ref  /scratch/beyer/3C/BIDS/derivatives/CAT12_cross/sub-0015/anat/sub-0015_ses-t0_space-MNI305_desc-warpT2w.nii.gz \
-applyxfm -usesqform -out mni_icbm152_t1_tal_nlin_asym_09c_mask_15mm_sub015_applyxsqform.nii

#Downsample WML mask & WML data to 3mm isotropic
flirt -in mni_icbm152_wm_tal_nlin_asym_09c.nii \
-ref  /scratch/beyer/3C/BIDS/derivatives/CAT12_cross/sub-0005/anat/sub-0005_ses-t0_space-MNI305_desc-warpT2w.nii.gz \
-applyisoxfm 3 -usesqform \
-out mni_icbm152_wm_tal_nlin_asym_09c_30mm.nii

fslmaths mni_icbm152_wm_tal_nlin_asym_09c_30mm.nii -thr 0.8 -bin mni_icbm152_wm_tal_nlin_asym_09c_30mm_thr0.8_bin.nii

flirt -in mni_icbm152_t1_tal_nlin_asym_09c_mask.nii -ref  /scratch/beyer/3C/BIDS/derivatives/CAT12_cross/sub-0005/anat/sub-0005_ses-t0_space-MNI305_desc-warpT2w.nii.gz \
-applyisoxfm 3 -usesqform \
-out mni_icbm152_t1_tal_nlin_asym_09c_mask_3mm.nii 



# data has to be split
cd /scratch/beyer/3C/BIDS/derivatives/WML/merged/

fslsplit /scratch/beyer/3C/BIDS/derivatives/WML/merged/mergedfile_N1781_space-MNI_res-1.5.nii.gz

for elem in `ls vol*`
do
flirt -in $elem -ref  $elem \
-applyisoxfm 3 -out $elem
done

fslmerge -t mergedfile_N1781_space-MNI_res-3.nii.gz `ls vol*`

# all versions with flirt did not work properly
#applyxfm4D /scratch/beyer/3C/BIDS/derivatives/WML/merged/mergedfile_N1781_space-MNI_res-1.5.nii.gz /scratch/beyer/3C/mni_icbm152_nlin_asym_09c/mni_icbm152_wm_tal_nlin_asym_09c_30mm_thr0.8_bin.nii \
#/scratch/beyer/3C/BIDS/derivatives/WML/merged/mergedfile_N1781_space-MNI_res-3.nii.gz -applyisoxfm 3 -usesqform 
#--singlematrix 



## Create GM brain mask in correct space

#downsample MNI GM template (1mm & 193x229x193) to specific GM TP dimension & resolution (1.5mm & 113x137x113)
#it does not matter which subject we use here, as it is assumed that images are in the same space and only header information not intensity is used
#https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/FLIRT/FAQ#Can_I_register_to_an_image_but_use_higher.2Flower_resolution_.28voxel_size.29.3F
flirt -in mni_icbm152_gm_tal_nlin_asym_09c.nii -ref  /scratch/beyer/3C/BIDS/derivatives/CAT12_cross/sub-0005/anat/sub-0005_ses-t0_space-MNI305_desc-mwp1.nii.gz \
-applyxfm -usesqform -out mni_icbm152_gm_tal_nlin_asym_09c_15mm_sub005_applyxsqform.nii

#thresholded map at 0.2 WM probability and binarized (in order to have motor cortex covered, a low threshold must be used)
fslmaths mni_icbm152_gm_tal_nlin_asym_09c_15mm_sub005_applyxsqform.nii -thr 0.2 -bin mni_icbm152_gm_tal_nlin_asym_09c_15mm_sub005_applyxsqform_thr0.2_bin.nii
 


sub-1431 sub-2171 sub-2410 sub-2441 sub-2472 sub-2494 sub-2495 sub-2498 sub-2586 sub-2591 sub-2599 sub-2600 sub-2621 sub-2631 sub-2658 sub-2664 sub-2665 sub-2671 sub-2690 sub-2694 sub-2697 sub-2700 sub-2701 sub-2708 \
sub-2709 sub-2710 sub-2711 sub-2712 sub-2747 sub-2770 sub-2771 sub-2787 sub-2789 sub-2790 sub-2795 sub-2800 sub-2815 sub-2825 sub-2826 sub-2851 sub-2865 sub-2866 sub-2871 sub-2872 sub-2873 sub-2875 sub-2876 sub-2884 \
sub-2885 sub-2892 sub-2897 sub-2898 sub-2901 sub-2916 sub-2919 sub-2920 sub-2922 sub-2926 sub-2931 sub-2932 sub-2936 sub-2943 sub-2944 sub-2951 sub-2960 sub-2962 sub-2965 sub-2978 sub-2979 sub-2980 sub-2981 sub-2982 \
sub-2983 sub-2984 sub-2987 sub-2990 sub-2991 sub-2994 sub-2995 sub-2996 sub-2997 sub-3002 sub-3003 sub-3018 sub-3019 sub-3025 sub-3033 sub-3036 sub-3041 sub-3042 sub-3044 sub-3045 sub-3051 sub-3064 sub-3065 sub-3069 \
sub-3078 sub-3080 sub-3088 sub-3092 sub-3102 sub-3107 sub-3113 sub-3115 sub-3116 sub-3117 sub-3120 sub-3122 sub-3123 sub-3133 sub-3148 sub-3151 sub-3153 sub-3155 sub-3157 sub-3158 sub-3164 sub-3171 sub-3173 sub-3174 \
sub-3177 sub-3181 sub-3182 sub-3183 sub-3188 sub-3196 sub-3197 sub-3198 sub-3199 sub-3207 sub-3208 sub-3215 sub-3217 sub-3225 sub-3226 sub-3228 sub-3231 sub-3232 sub-3233 sub-3234 sub-3238 sub-3239 sub-3246 sub-3247 \
sub-3248 sub-3262 sub-3265 sub-3267 sub-3270 sub-3271 sub-3280 sub-3290 sub-3294 sub-3300 sub-3302 sub-3303 sub-3305 sub-3309 sub-3317 sub-3320 sub-3330 sub-3331 sub-3335 sub-3336 sub-3342 sub-3347 sub-3349 sub-3350 \
sub-3357 sub-3360 sub-3362 sub-3364 sub-3369 sub-3372 sub-3379 sub-3380 sub-3381 sub-3382 sub-3387 sub-3388 sub-3389 sub-3390 sub-3393 sub-3410 sub-3413 sub-3414 sub-3415 sub-3416 sub-3417 sub-3420 sub-3422 sub-3424 \
sub-3425 sub-3426 sub-3429 sub-3436 sub-3453 sub-3454 sub-3459 sub-3460 sub-3461 sub-3469 sub-3473 sub-3475 sub-3486 sub-3532 sub-3533 sub-3534 sub-3565 sub-3600 sub-3605 sub-3622 sub-3667 sub-3687 sub-3726 sub-3730 \
sub-3752 sub-3771 sub-3818 sub-3827 sub-3902 sub-3989 sub-4066 sub-4078 sub-4082 sub-4084 sub-4307 sub-4315 sub-4331 sub-4401 sub-4402 sub-4427 sub-4433 sub-4434 sub-4458 sub-4551 sub-4558 sub-4595 sub-4628 sub-4640 \
sub-4641 sub-4652 sub-4676 sub-4688 sub-4701 sub-4707 sub-4724 sub-4729 sub-4736 sub-4738 sub-4747 sub-4777 sub-4788 sub-4793 sub-4817 sub-4843 sub-4864 sub-4870 sub-4871 sub-4872 sub-4873 sub-4884 sub-4930
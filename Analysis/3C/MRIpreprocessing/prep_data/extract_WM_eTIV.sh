#!/bin/bash

LC_NUMERIC=C
SUBJECTS_DIR="/extra/3C/data/preprocessed/Dijon/Dijon1_FS7.3_T1/"
cd $SUBJECTS_DIR

asegstats2table --subjects `ls -d ????` --tablefile /scratch/beyer/3C/BIDS/derivatives/Derivatives/Own_tabular_data/aseg.vol.table --skip

#echo "SubjID,CerebralWhiteMatterVol,ICV" > /scratch/beyer/3C/BIDS/derivatives/Derivatives/Own_tabular_data/aseg.csv
#for subj_id in `ls -d ????`; do 
#printf "%s,"  "${subj_id}" >> /scratch/beyer/3C/BIDS/derivatives/Derivatives/Own_tabular_data/aseg.csv
#printf "%g" `cat ${subj_id}/stats/aseg.stats | grep CerebralWhiteMatterVol | awk -F, '{print $4}'` >> /scratch/beyer/3C/BIDS/derivatives/Derivatives/Own_tabular_data/aseg.csv
#printf "%g" `cat ${subj_id}/stats/aseg.stats | grep IntraCranialVol | awk -F, '{print $4}'` >> aseg.csv
#echo "" >> /scratch/beyer/3C/BIDS/derivatives/Derivatives/Own_tabular_data/aseg.csv
#done

# -*- coding: utf-8 -*-
"""
Modified on Oct. 27 2022

@author: fbeyer
"""
import csv 
import os
import shutil
import re
import gzip
import os
import pandas as pd

#use the same selection (qa != 5 as done for merging the files for NNMF)

df_sel=df[(df["time_point_0_WMH"]==1)&(df["qa_time_point_0_WML"]!=5)]
df_sel.to_csv("/scratch/beyer/multivariate_risk_svd/Results/tabular_results/usable_participants_WML.csv", index=False)

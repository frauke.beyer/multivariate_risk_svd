
#!/bash

cd /extra/3C/data/orig/Dijon/WMH/WMH_proba/timepoint0
for subj in `ls -d *.hdr | cut -c 1-4`
do

echo $subj
fslchfiletype NIFTI_GZ ${subj}HSB.hdr /scratch/beyer/3C/BIDS/derivatives/WML/sub-${subj}/ses-t0/anat/${subj}_HSB.nii

if [ -f /scratch/beyer/3C/BIDS/derivatives/WML/sub-${subj}/ses-t0/anat/sub-${subj}_ses-t0_WMHw.nii.gz ];
then
fslcpgeom /scratch/beyer/3C/BIDS/derivatives/WML/sub-${subj}/ses-t0/anat/sub-${subj}_ses-t0_WMHw.nii.gz /scratch/beyer/3C/BIDS/derivatives/WML/sub-${subj}/ses-t0/anat/${subj}_HSB.nii
else 
echo "WMHw is not available"
fi

done
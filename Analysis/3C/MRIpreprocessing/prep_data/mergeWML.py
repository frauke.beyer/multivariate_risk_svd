# -*- coding: utf-8 -*-
"""
Modified on Oct. 27 2022

@author: fbeyer
"""
import csv 
import os
import shutil
import re
import gzip
import os

os.remove("merge_command.sh")

cmd="fslmerge -t /scratch/beyer/3C/BIDS/derivatives/WML/merged/mergedfile_N1781_space-MNI_res-1.5.nii.gz"
with open('/scratch/beyer/3C/BIDS/participants.tsv') as subjfile:
    csv_reader = csv.reader(subjfile, delimiter='\t')
    line_count = 0
    for row in csv_reader:
        if (row[2]=='1'):
            if (row[9]!='5'): #merge all subjects who are not labeled 5 or 999
                print("subject %s is merged" %(row[1]))
                cmd+=" /scratch/beyer/3C/BIDS/derivatives/WML/%s/ses-t0/anat/%s_ses-t0_space-MNI305_desc-warpWMH.nii" %(row[0],row[0])
        else:
            print("subject %s does not have WMH at t0" %(row[1]))


text_file = open("merge_command.sh", "w")
n = text_file.write(cmd)
text_file.close()


import csv
import os
import matplotlib.pyplot as plt
import nibabel as nib
import numpy as np
from nilearn.plotting import plot_anat

subjs=[]
flip_y_n=[]
with open('/scratch/beyer/3C/BIDS/participants.tsv') as subjfile:
    csv_reader = csv.reader(subjfile, delimiter='\t')
    line_count = 0
    for row in csv_reader:
       
        if (row[2]=='1'):
            print("subject %s does have WMH at t0 %s" %(row[0], row[9]))
            if (row[9]=='9'|row[9]=='9'):
                print("subject %s does not need left right flip" %(row[0]))
                flip_y_n.append(0)
                subjs.append(row[0][4:8])
            else:
                print("subjects need left right flip")
                flip_y_n.append(1)
                subjs.append(row[0][4:8])
        else:
            print("subject %s does not have WMH at t0" %(row[0]))


def realign_to_T2w(subj, flip_y_n):

    if flip_y_n==1:
        
        print("This has been done for all subjects previously")


        #if not (os.path.isfile('/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/sub-%s_ses-t0_WMH_map_reo.nii.gz' %(subj, subj))):
        T2w=nib.load("/scratch/beyer/3C/BIDS/sub-%s/ses-t0/anat/sub-%s_ses-t0_T2w.nii" %(subj, subj))
        #WML=nib.load('/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/sub-%s_ses-t0_WMHw.nii.gz' %(subj, subj)) #binarized WML
        #WML=nib.load('/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/%s_HSB.nii.gz' %(subj, subj)) #probmaps
        WMLdata=WML.get_fdata()

        #reorient WML data to T2w data matrix based on visual comparison
        WMLdata_n=np.flip(np.rot90(np.rot90(np.flip(WMLdata,2))),0)
        
        #for binary maps: select only voxels with value 4 (indicator of WMH)
        #WHM_map=(WMLdata_n==4)
        #WHM_map=WHM_map.astype(np.float64)
        
        #Save the realigned data matrix with the same affine as the original T2w!
        #nib.save(nib.Nifti1Image(WMLdata_n, T2w.affine), '/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/sub-%s_ses-t0_WMHw_reo.nii.gz' %(subj, subj))
        #nib.save(nib.Nifti1Image(WHM_map, T2w.affine), '/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/sub-%s_ses-t0_WMH_map_reo.nii.gz' %(subj, subj))
        nib.save(nib.Nifti1Image(WMLdata_n, T2w.affine), '/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/%s_HSB_reo.nii.gz' %(subj, subj))

    else:
        #if not (os.path.isfile('/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/sub-%s_ses-t0_WMH_map_reo.nii.gz' %(subj, subj))):
        T2w=nib.load("/scratch/beyer/3C/BIDS/sub-%s/ses-t0/anat/sub-%s_ses-t0_T2w.nii" %(subj, subj))
        #WML=nib.load('/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/sub-%s_ses-t0_WMHw.nii.gz' %(subj, subj))
        WML=nib.load('/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/%s_HSB.nii.gz' %(subj, subj)) #probmaps

        WMLdata=WML.get_fdata()

        #reorient WML data to T2w data matrix based on visual comparison (without flip by x-direction)
        WMLdata_n=np.rot90(np.rot90(np.flip(WMLdata,2)))
        
        #select only voxels with value 4 (indicator of WMH)
        #WHM_map=(WMLdata_n==4)
        #WHM_map=WHM_map.astype(np.float64)
        
        #Save the realigned data matrix with the same affine as the original T2w!
        #nib.save(nib.Nifti1Image(WMLdata_n, T2w.affine), '/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/sub-%s_ses-t0_WMHw_reo.nii.gz' %(subj, subj))
        #nib.save(nib.Nifti1Image(WHM_map, T2w.affine), '/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/sub-%s_ses-t0_WMH_map_reo.nii.gz' %(subj, subj))
        nib.save(nib.Nifti1Image(WMLdata_n, T2w.affine), '/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/%s_HSB_reo.nii.gz' %(subj, subj))
        
     

i=0
for subj in subjs:
    print("realigning %s with %i flip" %(subj, flip_y_n[i]))
    realign_to_T2w(subj, flip_y_n[i])
    i+=1

#realign_to_T2w("1692",0)

#Final check to be performed either using nilearn plotting or with freeview etc.
#display=plot_anat(T2w)
#WML_reo=nib.load('/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/sub-%s_ses-t0_WMHw_reo.nii.gz', %(subj, subj))
#display.add_overlay(WML_reo)
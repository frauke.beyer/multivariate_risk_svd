from nipype.interfaces import fsl


import os
import matplotlib.pyplot as plt
import nibabel as nib
import numpy as np
from nilearn.plotting import plot_anat
import csv

subjs=[]
with open('/scratch/beyer/3C/BIDS/participants.tsv') as subjfile:
    csv_reader = csv.reader(subjfile, delimiter='\t')
    line_count = 0
    for row in csv_reader:
       
        if (row[2]=='1'):
            print("subject %s does have WMH at t0" %(row[1]))
            if (row[9]=='5'):
                print("subject %s does need flirt transform" %(row[1]))
                subjs.append(row[1])
        else:
            print("subject %s does not have WMH at t0" %(row[1]))



for sub in subjs:
    WMH='/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/sub-%s_ses-t0_WMHw_reo.nii.gz' %(sub, sub)
    T2wim="/scratch/beyer/3C/raw_data/T2/timepoint0/2%sDD_T2_reo.nii.gz" %sub
    WMH_out='/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/sub-%s_ses-t0_WMHw_flirt.nii.gz' %(sub, sub)
    WMH_map='/scratch/beyer/3C/BIDS/derivatives/WML/sub-%s/ses-t0/anat/sub-%s_ses-t0_WMHw_map_reo.nii.gz' %(sub, sub)
    flt = fsl.FLIRT()
    flt.inputs.in_file = WMH
    flt.inputs.reference = T2wim
    flt.inputs.dof = 6
    flt.inputs.interp="nearestneighbour"
    flt.inputs.output_type = "NIFTI_GZ"
    flt.inputs.out_file=WMH_out
    flt.run() 
    bin=fsl.ImageMaths()
    bin.inputs.op_string= '-thr 4 -uthr 4 -bin'
    bin.inputs.in_file=WMH_out
    bin.inputs.out_file=WMH_map
    bin.run()
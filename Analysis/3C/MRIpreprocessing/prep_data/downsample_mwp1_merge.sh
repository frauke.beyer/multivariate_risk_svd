cd /scratch/beyer/3C/BIDS/derivatives/CAT12_cross/merged

i=0

for elem in `cat /scratch/beyer/multivariate_risk_svd/Analysis/3C/MRIpreprocessing/prep_data/sfile.txt`
do
echo $elem

#cp $elem vol$i.nii.gz

#flirt -in vol$i.nii.gz -ref  vol$i.nii.gz \
#-applyisoxfm 3 -out vol$i.nii.gz

fslmaths vol$i.nii.gz -kernel gauss 2.1233226 -fmean vol${i}_sm.nii.gz
#5mm FWHM smoothing
let i=i+1
done

fslmerge -t mergedfile_N1781_type-sm5mm_space-MNI_res-3.nii.gz `ls vol*_sm.nii.gz`

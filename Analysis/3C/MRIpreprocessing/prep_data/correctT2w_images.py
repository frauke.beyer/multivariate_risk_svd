# -*- coding: utf-8 -*-
"""
Modified on Oct. 27 2022

@author: fbeyer
"""
import csv 
import os
import shutil
import re
import gzip

subjs=[]
with open('/scratch/beyer/3C/BIDS/participants.tsv') as subjfile:
    csv_reader = csv.reader(subjfile, delimiter='\t')
    line_count = 0
    for row in csv_reader:
        print(row[2])
        if (row[2]=='1'):
            if (row[9]=='9'):
                print("subject %s does have WMH at t0 and has been rearranged" %(row[1]))
                subjs.append(row[0]) #need to use subject id with sub-
        else:
            print("subject %s does not have WMH at t0" %(row[1]))


def correct_bids(subjlist):

    import os
    import shutil
    import re

    rawdir="/scratch/beyer/3C/raw_data/T2/timepoint0/"
    basedir="/scratch/beyer/3C/BIDS/"
    cat12dir="/scratch/beyer/3C/BIDS/derivatives/CAT12_cross/"
    wmldir="/scratch/beyer/3C/BIDS/derivatives/WML/"

    for subj in subjlist:
        
        #rename resliced T2w 
        #if os.path.isfile('%s%s/anat/%s_ses-t0_space-indivT1_desc-alignT2w.nii.gz' %(cat12dir,subj, subj)):
        #    print("rename resliced T2w for %s" %subj)
        #    shutil.copy('%s%s/anat/%s_ses-t0_space-indivT1_desc-alignT2w.nii.gz' %(cat12dir,subj, subj), '%s%s/anat/%s_ses-t0_space-indivT1_desc-rnmiT2w.nii.gz' %(cat12dir,subj, subj))
        
        #copy T2w which is now aligned to T1 to derivatives
        #if os.path.isfile('%s%s/ses-t0/anat/%s_ses-t0_T2w.nii' %(basedir,subj, subj)):  
        #    print("Copying T2w to derivatives for %s" %subj)
        #    shutil.copy('%s%s/ses-t0/anat/%s_ses-t0_T2w.nii' %(basedir,subj, subj), '%s%s/anat/%s_ses-t0_space-indivT1_desc-nmiT2w.nii' %(cat12dir,subj, subj))
        
        #remove those files when re-running 244 individuals (because T2-T1 coregistration worked before)
        os.remove('%s%s/ses-t0/anat/%s_ses-t0_T2w.nii' %(basedir,subj, subj))
        os.remove('%s%s/ses-t0/anat/rnmi_%s_ses-t0_T2w.nii' %(basedir,subj, subj))

        #rename resliced WML map to have nmi in name 
        if os.path.isfile('%s%s/ses-t0/anat/rnmi_%s_ses-t0_WMH_map_reo.nii' %(wmldir,subj, subj)):  
            print("Copying resliced WML map to have rnmi in name for %s" %subj)
            shutil.copy('%s%s/ses-t0/anat/rnmi_%s_ses-t0_WMH_map_reo.nii' %(wmldir,subj, subj), '%s%s/ses-t0/anat/%s_ses-t0_space-indivT1_desc-rnmireoWMH.nii' %(wmldir,subj, subj))

        #rename warped resliced WML map to have nmi in name 
        if os.path.isfile('%s%s/ses-t0/anat/wrnmi_%s_ses-t0_WMH_map_reo.nii' %(wmldir,subj, subj)):  
            print("Copying warped WML map to have rnmi in name for %s" %subj)
            shutil.copy('%s%s/ses-t0/anat/wrnmi_%s_ses-t0_WMH_map_reo.nii' %(wmldir,subj, subj), '%s%s/ses-t0/anat/%s_ses-t0_space-MNI305_desc-warpWMH.nii' %(wmldir,subj, subj))
          
        #rename WML map to have nmi in name 
        if os.path.isfile('%s%s/ses-t0/anat/%s_ses-t0_WMH_map_reo.nii' %(wmldir,subj, subj)):  
            print("Copying WML map to have nmi in name for %s" %subj)
            shutil.copy('%s%s/ses-t0/anat/%s_ses-t0_WMH_map_reo.nii' %(wmldir,subj, subj), '%s%s/ses-t0/anat/%s_ses-t0_space-indivT1_desc-nmireoWMH.nii' %(wmldir,subj, subj))
            
        #copy original T2w to BIDS raw
        subjr='2'+subj[4:8]
        if os.path.isfile('%s/%sDD_T2_reo.nii.gz' %(rawdir,subjr)):  
            print("copying original T2w file")
            with gzip.open('%s/%sDD_T2_reo.nii.gz' %(rawdir,subjr), 'rb') as f_in:
                with open('%s%s/ses-t0/anat/%s_ses-t0_T2w.nii' %(basedir,subj, subj), 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            
            
    return 1

#Dont run this for subj 0005       
correct_bids(subjs)

#['sub-1692','sub-2042']) #


# import json
# data = {'Name': '3C anatomical data and WML', 'BIDSVersion': "1.7.0"}
# with open('/scratch/beyer/3C/BIDS/dataset_description.json', 'w') as ff:
#     json.dump(data, ff)


# data = {'participant_id_for_match': 'for matching with clinical data', 
# 'time_point_0_WMH': "whether data is available",
# 'time_point_1_WMH': "whether data is available",
# 'time_point_0_T1': "whether data is available",
# 'time_point_1_T1': "whether data is available",
# 'time_point_0_T2': "whether data is available",
# 'time_point_1_T2': "whether data is available"}
# with open('/scratch/beyer/3C/BIDS/participants.json', 'w') as ff:
#     json.dump(data, ff)


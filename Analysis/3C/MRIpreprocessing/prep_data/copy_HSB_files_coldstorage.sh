#Copy necessary files to own BIDS folder

prov="/scratch/for_fbeyer/BIDS"
dest="/scratch/fbeyer/3C/BIDS/"

#for subj in 1692
while read subj
do

echo "copying ${subj}"

mkdir -p $dest/derivatives/CAT12_cross/sub-${subj}/anat/
mkdir -p $dest/derivatives/WML/sub-${subj}/ses-t0/anat
mkdir -p $dest/sub-${subj}/ses-t0/anat

cp ${prov}/derivatives/CAT12_cross/sub-${subj}/anat/sub-${subj}_ses-t0_desc-warp.nii.gz $dest/derivatives/CAT12_cross/sub-${subj}/anat/sub-${subj}_ses-t0_desc-warp.nii.gz
gunzip $dest/derivatives/CAT12_cross/sub-${subj}/anat/sub-${subj}_ses-t0_desc-warp.nii.gz 
cp ${prov}/derivatives/WML/sub-${subj}/ses-t0/anat/${subj}_HSB_reo.nii.gz $dest/derivatives/WML/sub-${subj}/ses-t0/anat/${subj}_HSB_reo.nii.gz
gunzip $dest/derivatives/WML/sub-${subj}/ses-t0/anat/${subj}_HSB_reo.nii.gz

cp ${prov}/rawdata/sub-${subj}/ses-t0/anat/sub-${subj}_ses-t0_T1w.nii $dest/sub-${subj}/ses-t0/anat/sub-${subj}_ses-t0_T1w.nii
cp ${prov}/rawdata/sub-${subj}/ses-t0/anat/sub-${subj}_ses-t0_T2w.nii $dest/sub-${subj}/ses-t0/anat/sub-${subj}_ses-t0_T2w.nii

done < /scratch/fbeyer/multivariate_risk_svd/Analysis/3C/MRIpreprocessing/prep_data/subjlist.txt

#cd /extra/3C/data/orig/Dijon/WMH/WMH_proba/timepoint0
#ls -d *.hdr | cut -c 1-4 >> /scratch/fbeyer/multivariate_risk_svd/Analysis/3C/MRIpreprocessing/prep_data/subjlist.txt
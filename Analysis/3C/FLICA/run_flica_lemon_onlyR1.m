%% Demo script to run your own date through FLICA.

%% Set up:
addpath('/srv/shares/softs/fsl','/srv/shares/softs/fsl/etc/matlab/', '/srv/shares/softs/MATLAB/R2019a')
addpath('/scratch/beyer/multivariate_risk_svd/Analysis/3C/FLICA/flica')

%% Load data
subjects_to_exclude=[];
%Yfiles = {'/nobackup/aventurin4/data_fbeyer/flica_myelin/flica_inputs/GM_mod_merg_downsampled_s4_masked.nii.gz', '/nobackup/aventurin4/data_fbeyer/flica_myelin/flica_inputs/?h_thickness_10mm.mgh', '/nobackup/aventurin4/data_fbeyer/flica_myelin/flica_inputs/?h_area_10mm.mgh','/nobackup/aventurin4/data_fbeyer/flica_myelin/flica_inputs/t1maps/?h.t1map.fsavg5_222_sm10mm.mgh'};
Yfiles={'/scratch/beyer/3C/BIDS/derivatives/CAT12_cross/merged/mergedfile_N1781_type-sm5mmcropped526352_t_space-MNI_res-3.nii.gz',...
       '/scratch/beyer/3C/BIDS/derivatives/WML/merged/mergedfile_N1781_space-MNI_res-3.nii.gz'};

% NOTE that these should be downsampled to around 20k voxels in the mask,
% per modality... hoping to increase this memory/cpu-related limitation.

transforms = {'nop','auto2','rms','double';'nop','auto2','rms','double'}
[Y, fileinfo] = flica_load(Yfiles, transforms);
fileinfo.shortNames = {'VBM', 'WML'};

% %% Run FLICA
 clear opts
 opts.num_components = 50;
 opts.maxits = 1000;
 opts.plotConvergence=0;


 Morig = flica(Y, opts);
 [M, weights] = flica_reorder(Morig);

% %% Save results
 outdir = '/scratch/beyer/3C/BIDS/derivatives/Derivatives/FLICA/50components/';
 save(outdir+"/all_results.mat")

 flica_save_everything(outdir, M, fileinfo);
%% Produce correlation plots
%design = load('design.txt');
%clear des
%des.ev1 = design(:,1);
%flica_posthoc_correlations(outdir, des);

%% Produce the report using command-line scripts:
cd(outdir)
system('sh /scratch/beyer/multivariate_risk_svd/Analysis/3C/FLICA/flica/render_lightboxes_all.sh');
system('sh /scratch/beyer/multivariate_risk_svd/Analysis/3C/FLICA/flica/flica_report.sh .')

system('open index.html')


%H = read_vest([outdir 'subjectCoursesOut.txt'])'

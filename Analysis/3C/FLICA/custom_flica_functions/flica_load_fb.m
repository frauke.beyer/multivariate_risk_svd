% [Y, fileinfo] = flica_load(Yfiles, transforms)
%
% Loads data from .nii.gz or .mgh files:
% Yfiles{k} is the filename (or pattern) to load from.
% transforms{k,:} = {'' or 'log', '' or subtractThis, '' or divideByThis, 'single' or 'double'}
% Y{k} are the important data.
% masks{k} show which voxels to put these values back into.  In the case of
%   .nii.gz files, this is a 3D boolean matrix. In the case of .mgh files,
%   it's a pair of MRI structures.
% transforms{k} gives the information needed to reverse any
%   transformations applied to the input data (e.g. log then subtract this
%   mean then divide by this RMS).  Format TBA.
% This should provide enough information to reconstru    % Will break for tensor arrangementct a (masked and
% rounded) version of the raw data.

% TODO: Put a "modality_groups" option, with 0 -> omit this; unique
% positive -> flat modality; shared positive -> tensor group; shared
% negative -> concatenation group.

function [Y, Y_orig, fileinfo] = flica_load_fb(Yfiles, subjects_to_exclude,fsav_version, transforms)

%#ok<*AGROW> - Remove annoying MATLAB warnings about preallocating.

%% Parse arguments
if nargin<4
    transforms = repmat({'nop','auto2','rms','double'},length(Yfiles),1);
elseif iscell(transforms) && isequal(size(transforms), [1 4])
    transforms = repmat(transforms, length(Yfiles), 1);
else
    assert(isequal(size(transforms), [length(Yfiles) 4]))
end

%% 
prevMask = [];
for k = 1:length(Yfiles)
    fprintf('Loading "%s"... \n', Yfiles{k}); pause(.01)
    if strfind(Yfiles{k},'.nii.gz')
        
        [vol dims scales bpp endian] = read_avw(Yfiles{k});
        if bpp==32, 
            fprintf('Converting back to single... '); 
            vol=single(vol); 
        end % Save RAM immediately. Should really fix read_avw_img so it uses 'float32=>float32' to output singles directly.
        fprintf('Generating mask... ');
        masks{k} = any(vol,4); % 3D, so can't be sparse.
        fprintf('Flattening data matrix... ');
        rawdata{k} = reshape(vol,[],size(vol,4));
        fprintf("removing subjects from VBM %i", subjects_to_exclude)
        rawdata{k}(:,subjects_to_exclude) = [];
        %rawdata{k} = sparse(double(rawdata{k})); % Save memory in TBSS 1mm case -- but doesn't matter any more because we'll delete rawdata{k} soon.
        fprintf('Freeing memory... ');
        clear vol
        filetype{k} = {'NIFTI',dims,scales,bpp,endian};

        % De-duplication to save memory (MATLAB uses copy-on-write)
        if isequal(prevMask, masks{k})
            masks{k} = prevMask; % So out.masks{mi-1} and masks{k} share memory?
        else
            prevMask = masks{k}; % Ready for the next one
        end
        
    elseif strfind(Yfiles{k},'area_thickness')
        fprintf('Loading "%s"...\n', Yfiles{k}); pause(.01)
        addpath /afs/cbs.mpg.de/software/freesurfer/currentversion.ubuntu-xenial-amd64/ubuntu-xenial-amd64/matlab/
        assert(length(strfind(Yfiles{k},'?'))==1)
        filetype{k}(1) = MRIread(regexprep(Yfiles{k},'?','l'));
        vol = filetype{k}(1).vol;
        %assert(ndims(vol)==2)
        filetype{k}(2) = MRIread(regexprep(Yfiles{k},'?','r'));
        vol = [vol filetype{k}(2).vol];
        
        rawdata{k} = reshape(vol,[],size(vol,4));
        s=size(rawdata{k});

        rawdata{k}(:,subjects_to_exclude) = []; %rawdata{k}
 
        fprintf("modality %i with N_subjects= %i\n", k, s(2));
        masks{k} = any(logical(vol),4); %only take those subjects with non-zero values.
        NvoxPerHemi = size(vol,2)/2;
        switch (NvoxPerHemi) 
            case 2562
                labelSrcDir = '/data/pt_nro109/freesurfer_preprocessed/fsaverage4/label/';
            case 10242
                labelSrcDir = '/data/pt_nro109/freesurfer_preprocessed/fsaverage5/label/';
            case 40962
                labelSrcDir = '/data/pt_nro109/freesurfer_preprocessed/fsaverage6/label/';
            case 163842
                labelSrcDir = '/data/pt_nro109/freesurfer_preprocessed/fsaverage/label/';
                %labelSrcDir = '/vols/Data/oslo/fsaverages/fsaverage_/label/';
            otherwise
                labelSrcDir = '';
                size(vol,2)
                error 'Unrecognized number of vertices'
        end                
                
        clear tmp
        if ~isempty(labelSrcDir)
            for fi = 1:4
                {'lh.cortex.label','lh.Medial_wall.label','rh.cortex.label','rh.Medial_wall.label'}; %#ok<VUNUS>
                tmp{fi} = read_label('', [labelSrcDir ans{fi}]); %#ok<USENS,NOANS>
                tmp{fi} = tmp{fi}(:,1) + 1 + (ans{fi}(1)=='r')*NvoxPerHemi; %#ok<NOANS>
            end
            assert(all(sort([tmp{1};tmp{2}])==(1:NvoxPerHemi)'))
            assert(all(sort([tmp{3};tmp{4}])==(1:NvoxPerHemi)'+NvoxPerHemi))
            assert(all(sort(vertcat(tmp{:}))==(1:NvoxPerHemi*2)'))
            assert(size(filetype{k}(1).vol,2)==NvoxPerHemi)
            masks{k}([tmp{2};tmp{4}]) = 0;
        else
            warning 'Not masking out the medial wall' %#ok<WNTAG>
        end
        filetype{k} = rmfield(filetype{k}, 'vol'); % Big and not needed
    
    elseif strfind(Yfiles{k},'t1')
        fprintf('Loading and inverting and saving "%s"...\n', Yfiles{k}); pause(.01)
        addpath /afs/cbs.mpg.de/software/freesurfer/currentversion.ubuntu-xenial-amd64/ubuntu-xenial-amd64/matlab/
        assert(length(strfind(Yfiles{k},'?'))==1)
        filetype{k}(1) = MRIread(regexprep(Yfiles{k},'?','l'));
        vol = filetype{k}(1).vol;
        %vol_sq=squeeze(vol);
        zero_vals=find(~vol);
        size(zero_vals);%finding vertices with value 0
        nnew=length(zero_vals); %number of vertices with value 0
       
        
        proj=extractBetween(regexprep(Yfiles{k},'?','l'),'proj','.fsav');

        %inverting
        vol_inv=1000./vol; %unit of Hz
        %vol_inv=1./vol; %unit of 1/ms
        
        %replacing resulting NANs with 0 and apply --prune to mask out zero
        %voxels in smoothing
        vol_inv(zero_vals)=0;
        sum(sum(isnan(vol_inv)))
        %saving back
        fname_inv_lh = sprintf('/data/pt_life/data_fbeyer/flica_myelin/flica_inputs/lsd_lemon/t1maps/lh.t1map.proj%s.%s_inv.mgh',proj{1},fsav_version);
        fname_sm_lh = sprintf('/data/pt_life/data_fbeyer/flica_myelin/flica_inputs/lsd_lemon/t1maps/lh.t1map.proj%s.%s_inv_smoothed10mm.mgh',proj{1},fsav_version);
              
        %replace elements in old filetype
        filetype{k}.vol=vol_inv;
        filetype{k}.fspec=fname_inv_lh;
        MRIwrite(filetype{k},fname_inv_lh);
        
        %smoothing the output
        formatSpec='mri_surf2surf --srcsubject fsaverage --trgsubject %s --hemi lh --prune --sval %s --tval %s --fwhm-trg 10';
        
        cmd= sprintf(formatSpec, fsav_version, fname_inv_lh, fname_sm_lh);
        system(cmd)
        
        %%FOR right hemisphere
        filetype{k}(2) = MRIread(regexprep(Yfiles{k},'?','r'));
        vol = filetype{k}(2).vol;
        
        %mask zero voxels before inverting
        vol_sq=squeeze(vol);
        zero_vals=find(~vol);
        size(zero_vals);%finding vertices with value 0
        nnew=length(zero_vals);

        %inverting
        vol_inv=1000./vol;
        %vol_inv=1./vol;
        
        %replacing resulting NANs with 0 and apply --prune to mask out zero
        %voxels in smoothing
        vol_inv(zero_vals)=0;%could also just replace NaNs
        sum(sum(isnan(vol_inv)))
        
        %saving back
        fname_inv_rh = sprintf('/data/pt_life/data_fbeyer/flica_myelin/flica_inputs/lsd_lemon/t1maps/rh.t1map.proj%s.%s_inv.mgh',proj{1},fsav_version);
        fname_sm_rh = sprintf('/data/pt_life/data_fbeyer/flica_myelin/flica_inputs/lsd_lemon/t1maps/rh.t1map.proj%s.%s_inv_smoothed10mm.mgh',proj{1},fsav_version);
              
        %replace elements in old filetype
        filetype{k}(2).vol=vol_inv;
        filetype{k}(2).fspec=fname_inv_rh;
        MRIwrite(filetype{k}(2),fname_inv_rh);
        
        %smoothing the output
        formatSpec='mri_surf2surf --srcsubject fsaverage --trgsubject %s --hemi rh --prune --sval %s --tval %s --fwhm-trg 10';     
        cmd= sprintf(formatSpec, fsav_version, fname_inv_rh, fname_sm_rh);
        system(cmd)
        
        
        %%%NOW_read again
        filetype{k}(1) = MRIread(fname_sm_lh);
        vol = filetype{k}(1).vol;
        size(vol)
        %assert(ndims(vol)==2)
        filetype{k}(2) = MRIread(fname_sm_rh);
        size(filetype{k}(2).vol)
        vol = [vol filetype{k}(2).vol];
        size(vol)

        rawdata{k} = reshape(vol,[],size(vol,4));
        
        s=size(rawdata{k});

        rawdata{k}(:,subjects_to_exclude) = []; %rawdata{k}
         
        fprintf("modality %i with N_subjects= %i\n", k, s(2));
        masks{k} = any(logical(vol)&~isinf(vol),4); %only take those vertices that ...
                                                    %are not zero or inf.
        NvoxPerHemi = size(vol,2)/2;
        switch (NvoxPerHemi) 
            case 2562
                labelSrcDir = '/vols/Data/oslo/fsaverages/fsaverage4/label/';
            case 10242
                labelSrcDir = '/data/pt_nro109/freesurfer_preprocessed/fsaverage5/label/';
            case 40962
                labelSrcDir = '/vols/Data/oslo/fsaverages/fsaverage6/label/';
            case 163842
                labelSrcDir = '/data/pt_nro109/freesurfer_preprocessed/fsaverage/label/';
                %labelSrcDir = '/vols/Data/oslo/fsaverages/fsaverage_/label/';
            otherwise
                labelSrcDir = '';
                size(vol,2)
                error 'Unrecognized number of vertices'
        end                
                
        clear tmp
        if ~isempty(labelSrcDir)
            for fi = 1:4
                {'lh.cortex.label','lh.Medial_wall.label','rh.cortex.label','rh.Medial_wall.label'}; %#ok<VUNUS>
                tmp{fi} = read_label('', [labelSrcDir ans{fi}]); %#ok<USENS,NOANS>
                tmp{fi} = tmp{fi}(:,1) + 1 + (ans{fi}(1)=='r')*NvoxPerHemi; %#ok<NOANS>
            end
            assert(all(sort([tmp{1};tmp{2}])==(1:NvoxPerHemi)'))
            assert(all(sort([tmp{3};tmp{4}])==(1:NvoxPerHemi)'+NvoxPerHemi))
            assert(all(sort(vertcat(tmp{:}))==(1:NvoxPerHemi*2)'))
            assert(size(filetype{k}(1).vol,2)==NvoxPerHemi)
            masks{k}([tmp{2};tmp{4}]) = 0;
        else
            warning 'Not masking out the medial wall' %#ok<WNTAG>
        end
        filetype{k} = rmfield(filetype{k}, 'vol'); % Big and not needed
         
        %set inf values to zero
        rawdata{k}(isinf(vol))=0;
        
    else
        error('Unrecognized file type: %s', Yfiles{k})
    end
    
   
    Y{k} = rawdata{k}(masks{k},:);
    Y_orig{k}=Y{k};
    switch transforms{k,4}
        case {'single'}
            Y{k} = single(Y{k});
        case {'double', ''}
            Y{k} = double(Y{k});
        otherwise
            error('Undefined data type "%s"', transforms{k,4})
    end
    rawdata{k} = []; % Free the memory
    fprintf('\n')
end


%% Apply transformations to rawdata to get Y
for k = 1:length(Yfiles)
    fprintf('CALCULATING TRANSFORMS FOR MOD')
    % Check for "missing data" volumes
    missingSubjects = all(isnan(Y{k}) | (Y{k}==0));
    Y{k} = Y{k}(:,~missingSubjects); % Squash them out, temporarily.
    % Nonlinear rescaling?
    switch transforms{k,1}
        case {'','nop'}
            % Do nothing
        case 'log'
            %assert(all(Y{k}(:)>0)) % Only for hard log, below
            %Y{k} = log(Y{k});
            % Use soft log instead:
            Y{k} = log(max(Y{k},mean(Y{k}(:)).^2/max(Y{k}(:))));
        otherwise
            transforms{k,1} %#ok<NOPRT>
            error('Unrecognized transformation!')
    end
    % De-meaning?
    if isnumeric(transforms{k,2}) && numel(transforms{k,2})>1
        % Mismatched size: is useless, should be auto
        % Same size: why are you doing this?
        error 'Unimplemented or illogical!'
    end
    switch transforms{k,2}
        case {'auto12'} % Demean in both directions
            tmp = matched_subtract(Y{k}, mean(Y{k}, 2));
            tmp = matched_subtract(tmp, mean(tmp, 1));
            transforms{k,2} = Y{k} - tmp; clear tmp
        case {'', 'auto2'}  % Have to remove the mean subject map
            transforms{k,2} = mean(Y{k},2);
            figure; imagesc(mean(Y{k},2))
        case {0}
            % nop
        otherwise
            transforms{k,2} %#ok<NOPRT>
            error('Unrecognized transformation!')
    end
    Y{k} = bsxfun(@minus, Y{k}, transforms{k,2});
    
    % De-scaling?
    switch transforms{k,3}
        case {'', 'rms'}
            transforms{k,3} = rms(Y{k});
            figure; imagesc(rms(Y{k}))
        case {1}
            % nop
        otherwise
            if isnumeric(transforms{k,3}) && numel(transforms{k,3})==1
                % ok, fixed rescaling
            else
                transforms{k,3} %#ok<NOPRT>
                error('Unrecognized transformation!')
            end
    end
    Y{k} = bsxfun(@rdivide, Y{k}, transforms{k,3});

    % Re-expand to put back all-zeroes volumes for missing subjects
    if any(missingSubjects)
	warning 'UNTESTED CODE: Dealing with missing data in some subjects'
	disp(find(missingSubjects))
        %tmp = Y{k};
        %Y{k} = zeros(size(tmp,1), length(missingSubjects));
        %Y{k}(:,~missingSubjects) = tmp;
	%clear tmp;
        Y{k}(:,~missingSubjects) = Y{k};
	Y{k}(:,missingSubjects) = 0;
	if size(transforms{k,2},2) == sum(missingSubjects)
	    transforms{k,2}(:,~missingSubjects) = transforms{k,2};
	    transforms{k,2}(:,missingSubjects) = 0;
	end
    end
end

fileinfo.masks = masks;
fileinfo.transforms = transforms;
fileinfo.filetype = filetype;
Yfiles = regexprep(Yfiles, '^.*/', '');
Yfiles = regexprep(Yfiles, '\.[^.]*$', '');
fileinfo.shortNames = Yfiles;




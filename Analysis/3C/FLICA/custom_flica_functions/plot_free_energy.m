%load('/scratch/beyer/3C/BIDS/derivatives/Derivatives/FLICA/70components//all_results.mat')
F_history=M.F_history;
a=[1:length(F_history);F_history];
a(:,isnan(a(2,:))) = []; 
a(1,2:end);
diff(a(2,:))./diff(a(1,:));
figure;
loglog(a(1,2:end), diff(a(2,:))./diff(a(1,:)), '.-')
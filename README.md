# multivariate_risk_SVD

This repository is about my project "Developing a predictive model of progression to dementia based on high-dimensional cSVD imaging markers" at Bordeaux Population Health Center (Bordeaux University). 


# Timeline:
**1.10.22 - 31.10.22**
- started in Bordeaux, got to know a lot of people, read about survival analysis & possible models, wrote R scripts to run univariate Cox models, simulated survival data based on LIFE Adult baseline, prepared and gave presentations on the project, got access to the data (25.10.22), organized data in BIDS format

**2.11.22**
- finalized scripts for intermodal (T1w-T2w) and MNI space registration with SPM
- double-checked with Ami and Iana that label 4 describes WMH
- wrote READMEs for BIDS data and this folder
- running CAT12_cross for the last 300 participants
- think about how to deal with <1 values in WMH maps in MNI space? 
- additional quality control necessary? 

**3.11.22**
- found that 2 files were missing (sub-2774 and sub-2775), run those. Now 1923 runs for CAT12-cross [done]
- reorganize CAT12 outputs to comply with BIDS format [run from 3 on (sub-0016), rearange sub-0006 to be correct]
- read papers by Phuah et al. and Habes et al. using unsupervised clustering on WMH distributions 
    - Phuah
        - ADNI 2D-Flair 
        - N=1043
        - downsampled to 2x2x2 mm, excluded population-wise WMH probability <1%
        - vector length of 30353 per participant
        - spectral decomposition + eigengap method to determine number of clusters
        - 5 clusters were found
    - Habes
        - SHIP: FLAIR 0.90.9 mm in-plane spatial resolution, slice thickness=3.0 mm
        - N=1836
        - registration to common template (through RAVENS procedure)
        - non-negative matrix factorization (is implemented in [scikit-learn] and in [matlab](https://github.com/asotiras/brainparts)
        - components determined as those with greatest change in spatial coverage of components

**4.11.22**
- run coregistration of WMH maps with MNI space for everyone from sub-0006) [done]
- prepare scripts to perform quality control of cross-sectional CAT [open]
- read about CV schemes and prepared for analysis
- wrote to Morgane about data access [ongoing]

**8.11.22**
- fixed TigerVNC to show whats happening [done]
- define WM mask in [MNI305 space](mni_icbm152_wm_tal_nlin_asym_09c_15mm_sub005_applyxsqform_thr0.8_bin.nii) (resliced to fit voxel dimensions, thresholded at 0.8 WMP and binarized)[done]
- define brain mask for VBM analysis in [MNI305 space](mni_icbm152_wm_tal_nlin_asym_09c_15mm_sub005_applyxsqform_thr0.8_bin.nii) (resliced to fit voxel dimensions, thresholded at 0.2 GMP and binarized)[done]

**9.11.22**
- run BIDS organization of outputs for everyone [done]: **1801** with WML, **1923** with CAT12
- run slicesdir separately for mwp1, warpT2 and WML_reo [has to be done]
- mark problematic subjects in `participants.tsv`: 0=ok, 1=needs to be checked, 2=needs to be excluded
    - done for SPM (not saved, unfortunately)
    - when inspecting WML images in slicesdir, some segmentations seemed weird: no underlying T2 was displayed or clear displacement
- I got convinced there was an error in reorientation. It turned out that SPM modified some images "silently" (`sub-%s_ses-t0_WMH_map_reo.nii` and `sub-%s_ses-t0_T2w.nii`) to be realigned to the T1w (but not resliced yet). That's why some overlays displayed slight shifts. Played around with `sub-0005`and `sub-0015`to see..

**10.11.22**
- tried to understand the issue about reorienting further. Rechecked in some subjects, and found warped overlays to be fine :)
- modified `rearrange_to_BIDS.py` to copy files to appropriate name and locations, refetching raw T2 from folder `raw_data`
- reran slicesdir for WML leaving out subjects without WML, but there is a problem with NaN values from SPM, rather use stacked WML file.
- rechecked `sub-0005` because at least `sub-0005` is a special case where raw T1 and raw T2 have a large special discard. It is ok, just the realign/reslice step was not taken as in the others because it was done with FLIRT, but results in MNI space are fine) 
- rechecked  `sub-0015` where misalignment between lesion and T2w was visible in MNI: probably something went wrong during playing around, reran and now it matches other subjects
- outliers defined based on CAT12 quality measures mean(Zscore)+3*sd=0.17 -> 17 outliers by definition from CAT12

**14.11.22**
- finished QA for CAT12. No subjects who need to be excluded (label 2) have WML data. 
- check copied files: everything is there, deleted files with old names
- QA-check of stacked file overlaid on MNI template:
    - issue of hypointense ventricular tissue labeled as WML (the case in at least 10 participants)
    - misorientation of the WML map and T2 in quite a lot of subjects (looks like left-right shifted)
- performed slicesdir to see quickly who is concerned..

**15.11.22**
- misalignment is present in 273 individuals
- re-run `realign_WML_to_T2.py` for wrongly registered subjects + qa check those reoriented
- one flip less of original image and everything looks ok :) except for 20 subjects (marked with `5`in `qa_time_point_0_WML`)
- for those, additional flirt registration was run with `realign_problematic_subjects.py` but although alignment is fine, there is severe missegmentation (large WML where normal appearing WM or GM is)

**16.11.22**
- run `flair_rawT1_coreg_estimate`(without rerunning T2 to MNI space transform) on the remaining 246 participants who have been reoriented
- move files into correct BIDS order and stack WML files, QA check briefly and found 2 + 7 additional cases.

**17.11.22**
- create stacked WMH map & thresholded with >1% of participants (N=17) criterion 
- ignore WM mask so far because lesions around ventricles are not included (maybe because of MNI space normalization..)
- run NNMF with 4 components (Habes et al.), there are some reasonable components

**18.11.22**
- wrote script to run 10-fold CV and estimate spatial coverage/reconstruction error criterion
- see no convergence of reconstruction error and spatial coverage is hard to define as there are no zero values in the components -> stupid error of commenting out the optimization part :/
- maybe this is because of prior selection of only lesion voxels > 17 participants? rerun with "whole brain" maps.

**20.11.22**
- run 12K max & 5fold CV for minimal & whole brain mask. Also adopted spatial coverage to use "true" null in this setting (1*10^-15)
- primary results look okish, not yet visible how many components.
- started to prepare presentation for SHIVA meeting

**21.11.22**
- continue to run brainmask & minmask decompositions.
- how to quantify overlap of maps: Habes: inner product of matching component pairs from the two splits -> use AFNI 3ddot with ddot option.
- downsample WML input to 3x3x3 mm so as to save compute time and space.
- save original & downsampled WML as Rdata file to use with other server
- use [knee](https://www.mathworks.com/matlabcentral/fileexchange/35094-knee-point) to determine inflection point of reconstruction error

**22.11.22**
- rerun with downsampled lesion mask (3mm), first determine number of components in whole sample via RE/spatial coverage, then perform CV to see component stability. 
- Started file to document methods snippets.

**23.11.22**
- check results, evaluate component stability 
- worked on presentations for tomorrow & next week

**24.11.22**
- read more about dementia prediction vs. etiological models to prepare.
- check loadings and compared with WML in several subjects
- got Windows access through CREDIM
- met with Ami & Iana (informally) -> OHBM?
- met with Carole & Stephanie:
    - both prediction and association is interesting (what's the benefit in detecting people earlier = improving prediction)
    - it's important which baseline risk model to compare to
    - use also stroke as outcome
    - perform similar analyses in UKBB and MEMENTO
    - investigate genetics of phenotypes

**25.11.22**
- run 2-12 components for brainmask at 3mm
- run CV5 for brainmask
- generate final loadings for N=1781 with 4 components

**28-29.11.22**
- finished presentation
- tried to get remote access

**5-9.12.22**
- tried to get computer for remote access to Bordeaux
- started to normalize remaining 1520 LIFE-baseline individuals, changed scripts to use baseline transform of resting state to MNI, not recalculate. Running from 08.12. on comps06h12 as screen session
- set up scripts to run NNMF on LIFE participants
- organized data for LIFE analysis & prepared analysis script
- started draft for OHBM abstract
- checked that those who do not have long/cross-vols are really not available, due to missing data. should incorporate info on which data is available in which subjects in `participants.tsv` -> Laurenz? 

**11.12-15.12.22**
- noticed that downsampling etc. created very large WML in 3mm, try to first binarize lesions in 1mm and then downsample to 3mm.
- write up results and create figures
- continue on OHBM abstract

**27.12.22-06.01.23**
- send OHBM abstract to coauthors
- received comments from Marc
  - title
  - naming of components deep/peri + location
  - rename components and order them similarly
  - decomposition on full sample?‚
- access to data finally worked
- saw that only hypertension (binary), BMI are available/have been asked in 3C
- rerun analysis in 3C adjusting for WM volume, APOE with two categories
- rerun analysis in LIFE with BMI instead of WHR, hypertension binary and APOE with two categories only and adjusting for WM volume

**07.01.23 - 30.01.23**
- worked on study proposal regarding variables to include etc.
- simulated data based on LIFE baseline data (to have accurate covariance structure of predictors)(54 predictors, 42 brain measures, N=1200)
- worked on PLS algorithms (generally they seem more directed towards many more predictors > 1000, and not so interested in feature interpretation)
    - implemented balanced CV for survival time and status
    - plsRcox has predict function, not available for sparse PLSDR or other algorithms (less accurate for CV)
    - tested plsRcox against simple Cox model -> plsRCox performs better when more components are allowed, when looking only at first component, simple Cox model outperforms it
    - questions:
        - which PLS cox algorithm to use? interpretability vs. performance?
- high dimensional: number of predictors > number of participants, for survival analysis: 10 cases per predictor -> 500 cases for 50predictors -> high dimensional setting
- also found some papers on other survival analysis machine learning algorithms
    - Survival Analysis in Cognitively Normal Subjects and in Patients with Mild Cognitive Impairment Using a Proportional Hazards Model with Extreme Gradient Boosting Regression:
        - applied  Xgboost to 14 predictor variables on 882 aMCI/100 CN and performed better than cox-PH
    - A comparison of machine learning methods for survival analysis of high‑dimensional clinical data for dementia prediction
        - n~800, and 150-250 features
        - applied different methods to 2 datasets, many algorithms performed well
        - found Cox model with likelihood-based boosting to perform well (but also regularized regression in Cox)
        - Random Forest Minimal Depth algorithm is the best-performing method for feature selection
    - packages:
        - SurvHiDim : does not seem so promising
        - SurvBoost: boosting + Cox PH -> interesting
        - hdnom: different algorithms + ways to interpret etc. Shiny APP!
        - mlr3proba: algorithms for survival machine learning
        - ‘highMLR: feature selection for survival outcomes
        - survELM: using "extreme learning machine" for survival analysis
- but what about our data (high collinearity of MRI data, GMV & WML...)? 

**06.02.23**
- continued on glmnet, and tried to figure out what is needed for survival curves
- outcomes of predict.coxPLSR: Choices are the linear predictor ("lp"), the risk score exp(lp) ("risk"), the expected number of events given the covariates and follow-up time ("expected"), the terms of the linear predictor ("terms") or the scores ("scores") -> linear predictor is best choice for "risk score" (as it quantifies higher/lower relative risk)
- implemented Ajanas script for glmnet and CoxBoost for simulated data
- which algorithm is best to deal with collinear/correlated data like brain imaging data?

**10.02.23**
- discussions with Simon and Omega group on models
- cross-validation & bootstrapping are similar resampling techniques, usually CV used for model validation, bootstrapping used for standard error of parameter estimates etc.. but CV with random splits repeatedly can also be used.
- many of the models in Spooner 2020 perform similarly well -> CoxBoost, ElasticNet 
- in this study: "not only do the linear regression models outperform the random forests, but the extreme gradient boosting models using a linear booster outperforms that 
  using a tree-based booster in most cases as well. This could indicate a linear relationship amongst the features" -> maybe also true for our setting?
- random forest & gradient boosting (XGboost) both choose randomly among "perfectly" correlated variables, but in random forests 50% of trees will choose A/B, diluted info, while in XGboost all importance will be on A or B (https://datascience.stackexchange.com/questions/12554/does-xgboost-handle-multicollinearity-by-itself)(https://datascience.stackexchange.com/questions/77663/decision-tree-regression-to-avoid-multicollinearity-for-regression-model)

**11-22.2.23**-
- worked on preparing data from 3C for survival analysis (high vs low dimensional)
- ideas from NeuroTK & group meeting:
    - what about resilience? maybe key factor
    - how are loadings distributed across participants? do PCA on that?
    - use non-linear decomposition of WML components (Samyogita)
    - how does WML affect brain networks longitudinally
    - rerun decomposition in LIFE adult older subjects
    - run decomposition in UKBB (check with Lina)
- found out that CoxBoost is no longer supported, cannot calculate Brier score with riskRegression toolbox. Go on for now with this model, discuss again
- worked on deep survival with Simons model


**26.2-10.3.23**
- further prepared data for performing high vs low-dimensional prediction models 
- leaving out Brier score for the moment
- implemented multiple imputation (CoxBoost does not allow missing values), competing risks, time dependent ROC curves
- found out that CoxBoost is no longer supported, cannot calculate Brier score with riskRegression toolbox. Go on for now with this model, discuss agains

**05.3.23-08.03.23**
- finished first round of analysis with 1000 repetitions of low vs high-dim model
- more ideas for unsupervised learning:
    - apply FLICA to WML and GMV maps
    - Results:
        - WML and GMV yielded almost completely separated components (for 10, 50 and 70 components)
        - for 70 components,first component was dominated by a single subject, but it was not evident which one
        - still an error with template which makes the figures look weird but this does not impact interpretation
        - does not seem to work well for WML & GMV
    - apply Spectral Clustering like in Phuah et al 2022
        - optimal number of components: array([ 1, 15,  5,  7, 28]) according to Eigengap method when using Gaussian Euclidean distance for constructing similarity matrix (sigma=4)
        - optimal number of components:array([ 8, 14,  6,  9, 17, 15,  3,  5,  1, 16]) according to Eigengap method when using Gaussian Euclidean distance for constructing similarity matrix (sigma=2)
        - this yields only very few voxels included into the clusters.. 
        - but no nice distribution of eigenvalues when using cosine similarity and sigma=8,16 (as in Phuah et al) -> very steep rise of eigenvalues after the first.
        - which is optimal way to construct similarity then?
    - use non-linear decomposition as in papers on glucose metabolism in AD (?)

**12.03.23-17.03.23**
- prepared presentation for SHIVA meeting
- meeting with Stephanie, adjusted analysis to leave out detailed markers of GMV & Co + only focus on cSVD markers (no difference between measures, WML Component 4 is selected as most predictive)
- thought about NAKO & UKBB

**20.03.-31.03.23**
- SHIVA meeting
    - Ami: what is association with age, distribution of these components. Could be hypertension-related or natural age evolution of components
    - overlap of WMH and sPVS in iSHARE in parietal horn of lateral ventricle (occipital lobe), but otherwise relatively distinct, both affect PSMD probably. 
    - are the phenotypes we see in the old the same as in the young? is the variance we see for example in PSMD the same? Could those also be developmental differences present since birth?
    - WML genetic risk associated with changes in NODDI measures in same locations as later WML -> less resilience to VRF as genetic predisposition?
    - causal relation of genetic WMH risk with dementia
    - but counterdictory results that BP lowers dementia risk (Muralis study)
- meeting with WP4:
    - prediction vs explanation:
        - Fine and Gray model is good to predict individual risk of dementia before death
        - if only interested in mechanisms, Cox model is good enough to determine dementia risk
        - can develop a model with Cox and test with Fine and Gray for prediction
    - define components in specific? cohort and then apply maps to other cohorts
        - 3C and LIFE result are distinct
        - do combined analysis?
        - exclude stroke cases
        - try FLICA with only one modality?
        - 
    - are lacunes available region-specific?
**01.04.-31.05.23**
- worked on RR 
- worked on bullseye decomposition in LIFE
- created MNI space bullseye decomposition
- wrote RFMASA abstract
- extracted bullseye decomposition for UKBB
- meeting with Stephanie, Ami, Marc on 28.04. and 30.05
- 
# Analysis Plan
## Mass-Univariate approach
- bring brain images into dataframe (row = participants, columns = voxels)
- apply mask with WM voxels only (probability > 0.9, for now JHU mask which is smaller) and voxels which have lesions in ~at least 5 participants (as in Lampe, 2019)~~
- rather use in at least 1% of participants (N=19) as in Neurology paper
- models to look at:
    1. res.cox1 <- coxph(survobj ~ vox1 + age + sex, data =  df
    1. res.cox1 <- coxph(survobj ~ vox1 + age + sex + totallesionvol, data =  df

- apply Coxmodel per column (res.cox1 <- coxph(survobj ~ vox1 + age + sex, data =  df)
- use parallel implementation
- identify regions which predict dementia (where RR of vox is above 1)

## Multivariate prediction using survival information
1. CoxPLS -> different models available in plsRCox but issue is interpretability when using more than one component (more than one component was estimated to be optimal by CV)

2. Lasso/elastic net
- [glmnet](https://cran.r-project.org/web/packages/glmnet/vignettes/Coxnet.pdf)
- [x] implemented
**regarding LASSO/ELASTIC NET/RIDGE REGRESSION** 
- L1 tends to shrink coefficients to zero (for only few predictors, LASSO) whereas L2 tends to shrink coefficients evenly (for collinear predictors, Ridge Regression). Thus Lasso can tend to randomly select one of collinear predictors from a set, while dropping the others. Both are linear! assuming linear relationship of predictors and hazard.

3. ranger (survival tree models, suitable for GWAS data) -> random survival forests & boosting in other packages

**regarding boosting and stuff**
- best performing model in [Spooner](https://link.springer.com/content/pdf/10.1038/s41598-020-77220-w.pdf): CoxBoost (feature selection through RF min. depth)
- XGBoost: For the MCI data, model performance was estimated using 10-fold cross-validation, dividing the survival data into 90%training and 10%testing sets. In Xgboost, the hyperparameters were estimated by further dividing the training set into 75%for learning and 25%for validation. We used the model trained on MCI data to stratify risk in the 100 CN subjects and computed the C-index for this cohort.

3. Illness/Death or Fine and Gray models:
- need few predictors (?? 10 ??), how to do that?

### Cross-validation scheme
like [Ajana 2019](https://github.com/SoufianeAjana/BLISAR/blob/master/methods/Lasso.R)
- for i=1:rep (100 times)
    split data into 10 folds
    for j=1:10
        use one fold as test, 9 folds as training data
        optimize model parameter LASSO? or also elastic net possible? in each training data (using leave-one-out CV or another k-fold CV)
        evaluate prediction error on each test fold


### Multivariate feature selection + modeling survival
1. NNMF with matlab script (validation through inflection point of the slope of the reconstruction error or largest spatial coverage with the lowest number of components)
- random half splits to show robustness (inner product of component pairs)
- use four (?) component loads in IllnessDeathModels

2. LinkedICA
- input both WML and GMV maps to LinkedICA
- use component loads in IDM

-> use loads with other predictors in LASSO/similar Coxmodel and propagate to IDM?

# Questions:
- what is the best follow-up time to take for Cox-model? All the data there is right?
- Topic of controlling for total lesion volume
- 15 events per predictor being considered adequate for fitting Coxmodel directly 
- how many events in 3C, so how many predictors can be adequately fitted at all?


# Project Outline
## Sample Selection
3C Dijon participants (N=4931) 
MRI:
T0: 1924 (39%)
T4: 1434 (29%)

Exclusion criteria: 
based on MRI:
- poor scan quality (N=) (Stephan et al.: N=123)
    - defined visually based on CAT report for analysis including GMV
    - defined visually based on warpedT2 + warped WML using slicesdir
- artefacts (N=) (Stephan et al.: N=123), check visually
- no WML maps available (N=122)

based on clinical assessment:
- prevalent dementia (Stephan et al.:N=8)
- individuals with missing dementia status over the 10 years of follow-up (Stephan et al.: n=71) -> is this relevant for survival analysis?


## Variables of interest
- age
- sex
- totallesionvol

For benchmark model (in line with Stephan et al., 2015)
- Sociodemographic factors included age, sex, and educational attainment. 
- Lifestyle factors included smoking and alcohol use.
- Functional assessment: Lawton and Brody scale for instrumental activities of daily living
- Cognition: mini-mental state examination, Benton visual retention test,39 and the digit span test
- Health variables: cardiovascular events (combining self reported history of myocardial infarction, coronary surgery, coronary angioplasty, surgery of the arteries in the legs, or stroke requiring admission to hospital), metabolic disease (diabetes; self reported, high glucose concentration ≥7.0 mmol/L, or receipt of hypoglycaemic treatment including oral diabetic drugs or insulin), and systolic blood pressure (continuous). 
- Genetic risk assessed apolipoprotein e4 status (coded as e4 positive v e4 negative).


## Image preprocessing (WML)
- important: T2-w, T1-w and PD is available in 3C T0 & T4
- LST cannot be used!
- but possible to use random forest classifier from https://nist.mni.mcgill.ca/white-matter-hyperintensities/ for multimodal data.
- BIANCA needs training data
- other algorithms: https://wmh.isi.uu.nl/ -> WMH segmentation challenge
- hopefully, lesion segmentation masks + T1w-coregistered brain masks available for T0 [x]
- co-registration of T2w to T1, and T1 to template to bring all masks into template space (using CAT12 + SPM) [x]
- eventually use WML progression from T0 to T4

## Image preprocessing (GMV)
- ~SPM 99 -> GMV maps~



# Folder structure:
Throughout the analysis, the reference space is [MNI305](http://www.bic.mni.mcgill.ca/~vfonov/icbm/2009/mni_icbm152_nlin_asym_09c_nifti.zip), the same used as in the latest CAT12 version. 
- Analysis
    - MRIpreprocessing
        - CAT12_cross
            - `CAT12_crosssectional.m`: matlabbatch script to run CAT crosssectional processing
            - `flair_rawT1_coreg_estimate.m`: matlabbatch script to coregister T1w and T2w, and apply deformation field from CAT12 to realigned T2w and WMH map with MNI space
            - `coregister_T2w_to_T1_for_sub-0005.sh`: there were some issues due to wrong header info for sub-0005 (now fixed), so I used flirt + CAT12 to coregister T2w and WMH map to MNI --> FILENAMES ARE CHANGED TO BE LIKE FOR ALL OTHER SUBJECTS!!
            - `rearrange_CAT12_outputsto_BIDS.py`: copy and gzip outputs of CAT12 & intermodal registration to BIDS/derivatives
            - `log_files`: all log_files for CAT, not tracked in git
        - CAT12_long
            - `CAT12_expertmode_revised.m`: longitudinal CAT processing with expertmode options
        - prep_data
            - `check_dataquality.ipynb`: notebook to visualize data (without access to GUI) & check which transforms are necessary for T2w and WMH maps to match
            - `realign_WML_to_T2.py`: script to realign T2w and WMH maps by rotating the image matrix several times and writing this matrix with the same header as original T2w. Yields correct result.
            - `rearrange_to_BIDS.py`: rearrange `raw_data`and `WMH`folders in `3C`to BIDS format, with participant id to be `0005`... `4961` without leading 2
            - `correctT2w_images`: script to reorder results obtained with SPM Estimate and Reslice, fetch original T2w images from `raw_data`
        - LIFE
            - `mergeWML.py`: merge WML files according to selected subjects file
            - `downsample_mergedWML.sh`
    - Proposal
        Scripts that were used for generating figures/finding information for the proposal.
    - UnivariateCox
        Scripts to run univariate Cox Analysis on WMH maps.
        - `prep_univ_cox_parallel.R`
        - `run_univ_cox_parallel.R`
        - `results_univ_cox_parallel.R`
        - `illness_death_test.R`: test SmoothHazard package
        - `univ_cox_test.R`: test univariate Cox modeling & parallel package in R
    - NNMF
    
    - LIFE 

        - `Analysis_WMH_components_LIFE.R`
- Organisation
    - Figures: all figures used in proposal or presentations
    - Presentations: all presentations held about this project in Bordeaux or elsewhere
    - Proposal: all files related to DFG proposal and acceptance



# Resources
- overview for imaging analysis in R: https://johnmuschelli.com/imaging_in_r/
- a good thread on Cox LASSO modeling: https://stats.stackexchange.com/questions/393937/clarification-for-lasso-based-cox-model-using-glmnet
- overview of survival packages available in R: https://cran.r-project.org/web/views/Survival.html
- good tutorial of survival with ranger package: http://amunategui.github.io/survival-ensembles/index.html
## A similar paper on Macular degeneration
- 33 variables -> variable selection using Lasso in Cox setting (R glmnet)
- use bootstrapping (resampling with replacement, 100 bootstraps) to ensure stability of predictors
- The penalization parameter of the lasso was estimated by minimizing the partial likelihood deviance using a 5-fold cross-validation stratified on the incidence rate of advanced AMD. 
- The final set of selected variables was composed of the most frequently selected variables on all the bootstrap samples using the elbow criterion.
- 9 variables were selected
- use these variables as predictors in the illness-death model (comme PAQUID example)

Validation:
- cross validation of predicted and observed cumulative incidence in same cohort
- validation in other cohorts


[https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjog6HokJT7AhU4wAIHHSdzAZsQFnoECAoQAQ&url=http%3A%2F%2Fscikit-learn.org%2Fstable%2Fmodules%2Fgenerated%2Fsklearn.decomposition.NMF.html&usg=AOvVaw3C01dOuMeJHIY8vceDjpwS]: /https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&cad=rja&uact=8&ved=2ahUKEwjog6HokJT7AhU4wAIHHSdzAZsQFnoECAoQAQ&url=http%3A%2F%2Fscikit-learn.org%2Fstable%2Fmodules%2Fgenerated%2Fsklearn.decomposition.NMF.html&usg=AOvVaw3C01dOuMeJHIY8vceDjpwS


```{r "load necessary packages survival 3C", echo=FALSE, warnings=FALSE, cache=TRUE, cache.extra=tools::md5sum("/data/pt_life_whm/Results/multivariate_cSVD/bullseye/3C/bullseye/cogperformance/survivalres.Rdata")}
library(dplyr)
library(ggplot2)
library(geomtextpath )
library(readODS)
library(tidyr)
library(survival)
library(flextable)
library(adjustedCurves)
library(ragg)
library(grid)
library(gridExtra)

#extract legend
#https://github.com/hadley/ggplot2/wiki/Share-a-legend-between-two-ggplot2-graphs
g_legend<-function(a.gplot){
  tmp <- ggplot_gtable(ggplot_build(a.gplot))
  leg <- which(sapply(tmp$grobs, function(x) x$name) == "guide-box")
  legend <- tmp$grobs[[leg]]
  return(legend)}
```

```{r loading data, eval=T, echo=F}
load("/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Results/3C/bullseye/bullseye/survivalres.Rdata")
survAC=res
#load("/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Results/3C/bullseye/bullseye/survival_allComp_TIV/survival_allComp_TIVsurvivalres.Rdata")
#survAC=res
load("/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Results/3C/bullseye/bullseye/survival_AD_allComp_TIV/survival_AD_allComp_TIVsurvivalres.Rdata")
survAD=res
```

```{r calculate survival All Cause results, echo=FALSE}
res=c()

for (i in seq(1:7)){
#print(i)
res=rbind(res,round(summary(survAC[[i]])$coefficients[1,],3))
}

rfp=data.frame(comp=paste0("Comp ", seq(1:7)), res[,c(1,2,3,5)])

#results for total WMH volume
res_t=round(summary(survAC[[11]])$coefficients,3)

rfp=rbind(rfp, c("WMH volume", res_t[1,c(1,2,3,5)]))

#results when taking into account all components
res_c=summary(survAC[[8]])$coefficients[c(1:7),]
rfp$est_c=c(res_c[,1],0)
rfp$se_c=c(res_c[,3],0)
rfp$p_c=c(res_c[,5],0)

rfp[rfp$comp=="WMH volume",c("est_c","se_c","p_c", "HR_adj",
                             "HR_adj_low", "HR_adj_high")]=c(NA,NA,NA,NA,NA,NA)

#calculate CI
rfp = rfp %>% mutate(low= exp(as.numeric(coef)-1.96*as.numeric(se.coef.)),
                     high=exp(as.numeric(coef)+1.96*as.numeric(se.coef.)),
                     HR_adj=exp(as.numeric(est_c)),
                     HR_adj_low=exp(as.numeric(est_c)-1.96*as.numeric(se_c)),
                     HR_adj_high=exp(as.numeric(est_c)+1.96*as.numeric(se_c)))

rfp_AC=rfp
rfp_AC$p_adj=p.adjust(rfp_AC$Pr...z..)

rfp_AC$exp.coef.=as.numeric(rfp_AC$exp.coef.)
rfp_AC$Pr...z..=as.numeric(rfp_AC$Pr...z..)
rfp_AC=purrr::modify_if(rfp_AC, ~is.numeric(.), ~round(., 4))
```

```{r create table for survival 3C, echo=FALSE, eval=T}
ref_table <- data.frame(key = colnames(rfp_AC[,c("comp", "exp.coef.", "low", "high", "Pr...z..", "p_adj")]),
  label = c("WMH component", 
    "HR", 
    "2.5% CI",
    "97.5% CI",
    "P",
    "FDR-p"))

ft <- flextable(rfp_AC[,c("comp", "exp.coef.", "low", "high", "Pr...z..", "p_adj")])%>% colformat_double(digits=2)
ft <-colformat_double(ft,j=5,digits=3)
#ft <-colformat_double(ft,j=9,digits=3)
ft <- set_header_df(ft, mapping = ref_table, key = "key")  %>%
  set_table_properties(layout = "autofit", width = .8)%>% 
  set_caption("Associations of WMH spatial components with all-cause dementia incidence in 3C-Dijon")

ftab_surv <- theme_booktabs(ft)
```

```{r calculate survival AD results, echo=FALSE}
res=c()

for (i in seq(1:7)){
#print(i)
res=rbind(res,round(summary(survAD[[i]])$coefficients[1,],3))
}

rfp=data.frame(comp=paste0("Comp ", seq(1:7)), res[,c(1,2,3,5)])

#results for total WMH volume
res_t=round(summary(survAD[[11]])$coefficients,3)

rfp=rbind(rfp, c("WMH volume", res_t[1,c(1,2,3,5)]))

#results when taking into account all components
res_c=summary(survAD[[8]])$coefficients[c(1:7),]
rfp$est_c=c(res_c[,1],0)
rfp$se_c=c(res_c[,3],0)
rfp$p_c=c(res_c[,5],0)

rfp[rfp$comp=="WMH volume",c("est_c","se_c","p_c", "HR_adj",
                             "HR_adj_low", "HR_adj_high")]=c(NA,NA,NA,NA,NA,NA)

#calculate CI
rfp = rfp %>% mutate(low= exp(as.numeric(coef)-1.96*as.numeric(se.coef.)),
                     high=exp(as.numeric(coef)+1.96*as.numeric(se.coef.)),
                     HR_adj=exp(as.numeric(est_c)),
                     HR_adj_low=exp(as.numeric(est_c)-1.96*as.numeric(se_c)),
                     HR_adj_high=exp(as.numeric(est_c)+1.96*as.numeric(se_c)))

rfp$exp.coef.=as.numeric(rfp$exp.coef.)
rfp$Pr...z..=as.numeric(rfp$Pr...z..)
rfp_AD=purrr::modify_if(rfp, ~is.numeric(.), ~round(., 4))
rfp_AD$p_adj=p.adjust(rfp_AD$Pr...z..)
```

```{r create table for survival AD 3C, echo=FALSE, eval=T}
ref_table <- data.frame(key = colnames(rfp_AD[,c("comp", "exp.coef.", "low", "high", "Pr...z..", "p_adj")]),
  label = c("WMH component", 
    "HR", 
    "2.5% CI",
    "97.5% CI",
    "P",
    "FDR-p"))

ft <- flextable(rfp_AD[,c("comp", "exp.coef.", "low", "high", "Pr...z..", "p_adj")])%>% colformat_double(digits=2)
#ft <-colformat_double(ft,j=5,digits=3)
#ft <-colformat_double(ft,j=9,digits=3)
ft <- set_header_df(ft, mapping = ref_table, key = "key")  %>%
  set_table_properties(layout = "autofit", width = .8)%>% 
  set_caption("Associations of WMH spatial components with probable or possible AD dementia incidence in 3C-Dijon")

ftab_surv_AD <- theme_booktabs(ft)
```



```{r plotting survival curves, eval=T, echo=F,results='hide',fig.show='hide'}
surva=read.csv("/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Data/surv_for_plotting.csv")
surva$time_cph_y=surva$time_cph/365

#compare three groups of highest, medium, lowest scores
qt5=quantile(surva$scores.TC5)
surva$stratscorestc5=NA
surva[surva$scores.TC5>qt5["75%"],"stratscorestc5"]="High"
surva[surva$scores.TC5<qt5["25%"],"stratscorestc5"]="Low"
surva$stratscorestc5=factor(surva$stratscorestc5, levels=c("Low", "High"))


surv5=survival::coxph(Surv(time_cph_y,status_cph)~stratscorestc5+sexeb_as+age0_as+DIPNIV0+EstimatedTotalIntraCranialVol, 
                     data=surva, x=T)
s=summary(surv5)

adjsurv5 <- adjustedsurv(data=surva,
                        variable="stratscorestc5",
                        ev_time="time_cph_y",
                        event="status_cph",
                        method="direct",
                        outcome_model=surv5,
                        conf_int=TRUE)

qt3=quantile(surva$scores.TC3)
surva$stratscorestc3="Low"
surva[surva$scores.TC3>qt3["75%"],"stratscorestc3"]="High"
surva[surva$scores.TC3<=qt3["75%"]&surva$scores.TC3>=qt3["25%"],"stratscorestc3"]=NA
surva$stratscorestc3=factor(surva$stratscorestc3, levels=c("Low", "High"))


surv3=survival::coxph(Surv(time_cph_y,status_cph)~stratscorestc3+sexeb_as+age0_as+DIPNIV0+EstimatedTotalIntraCranialVol, 
                     data=surva, x=T)
s=summary(surv3)



adjsurv3 <- adjustedsurv(data=surva,
                        variable="stratscorestc3",
                        ev_time="time_cph_y",
                        event="status_cph",
                        method="direct",
                        outcome_model=surv3,
                        conf_int=TRUE)



# plot with confidence intervals
p3=plot(adjsurv3, conf_int=TRUE, xlab = "Time [y]",  custom_colors=c("darkgreen", "darkblue", "red"), legend.title="",subtitle="All-cause dementia:\nComponent 3",line_size=0.8)+
  theme_bw(base_size = 12) + 
         theme(
               legend.position="none", 
               axis.title.x=element_blank(),
               axis.title.y=element_blank()
        ) 
  # theme(axis.text.x = element_text(color = "grey20", size = 16, family = "Roboto"),
  #       axis.title.x = element_text(color = "grey20", size = 18,  family = "Roboto"),
  #       axis.text.y = element_text(color = "grey20", size = 16, family = "Roboto"),
  #       axis.title.y = element_text(color = "grey20", size = 18,  family = "Roboto"),
  #       legend.title = element_text(color = "grey20", size = 18, family = "Roboto"),
  #       legend.text = element_text(color = "grey20", size = 18, family = "Roboto"),
  #       plot.margin = margin(t = 0,  # Top margin
  #                            r = 0,  # Right margin
  #                            b = 0,  # Bottom margin
  #                            l = 0,  # Left margin
  #                            unit = "cm"))


p5=plot(adjsurv5, conf_int=TRUE, xlab = "Time [y]", custom_colors=c("darkgreen", "darkblue", "red"), legend.title="",subtitle="Component 5", line_size=0.8)+
  theme_bw(base_size = 12) + 
         theme(legend.position="bottom",
               legend.title=element_blank(),
               axis.title.x=element_blank(),
               axis.title.y=element_blank()
        ) 

  # theme(axis.text.x = element_text(color = "grey20", size = 16, family = "Roboto"),
  #       axis.title.x = element_text(color = "grey20", size = 18,  family = "Roboto"),
  #       axis.text.y = element_text(color = "grey20", size = 16, family = "Roboto"),
  #       axis.title.y = element_text(color = "grey20", size = 18,  family = "Roboto"),
  #       legend.title = element_text(color = "grey20", size = 18, family = "Roboto"),
  #       legend.text = element_text(color = "grey20", size = 18, family = "Roboto"),
  #       plot.margin = margin(t = 0,  # Top margin
  #                            r = 0,  # Right margin
  #                            b = 0,  # Bottom margin
  #                            l = 0,  # Left margin
  #                            unit = "cm"))

```


```{r plotting forest, echo=F,results='hide',fig.show='hide', eval=F}
p1 <- ggplot(all)+
  geom_pointrange(size=0.8, aes(x=as.numeric(exp.coef.), y=comp, xmin=low, xmax=high, color="black"), position=position_nudge(y = -0.2))+
  geom_pointrange(size=0.8,aes(x=HR_adj, y=comp, xmin=HR_adj_low, xmax=HR_adj_high, color="orange"), position=position_nudge(y = 0.2))+
  geom_vline(xintercept = 1, linetype=3)+
  xlab("HR for all-cause dementia")+ylab("WMH components")+theme_classic()+scale_y_discrete(limits = rev(rfp$comp))+
  scale_color_manual(name="Model", values = c("black", "orange"), labels = c("univariable", "multivariable")) +theme(legend.position="bottom")+guides(color=guide_legend(nrow=1,byrow=TRUE))

```


```{r combining the plot, echo=F, eval=T, results='hide',fig.show='hide'}
rfp_AC$type="all-cause"
rfp_AD$type="AD"
all=rbind(rfp_AC, rfp_AD)
all$type=factor(all$type, levels=c("all-cause", "AD"))
all$comp=rep(c(paste0("Component ", seq(1:7)), "WMH volume"), times=2)
levels(all$type)=c("All-cause\ndementia", "Alzheimer's\ndisease")
p1 <- ggplot()+
  geom_pointrange(data=all, aes(x=as.numeric(exp.coef.), y=comp, xmin=low, xmax=high), position=position_nudge(y = 0), alpha=0.8)+facet_wrap(~type, scales="free_x")+ #, color="darkred"
  #geom_pointrange(data=all,aes(x=as.numeric(HR_adj), y=comp, xmin=HR_adj_low, xmax=HR_adj_high, color="darkgreen"), position=position_nudge(y = -0.35), alpha=0.8)+facet_wrap(~type, scales = 'free_x')+
  geom_vline(xintercept = 1, linetype=3)+ 
  #scale_color_manual(name="Model",labels = c("adjusted for all components","not adjusted"), values=c("darkgreen","darkred"))+#,
  xlab("HR")+
  ylab("WMH Measure")+  scale_y_discrete(limits = rev(unique(all$comp)))+ 
  labs(color="Model") + 
  theme_classic(base_size = 14) + 
  theme(legend.position="bottom", legend.spacing.y = unit(0.01, 'cm'),
        legend.spacing.x = unit(0.35, 'cm'),
        axis.text.x = element_text(angle = 45, vjust = 0.5, hjust = 0.5),
        axis.title.y = element_text(margin = margin(r = 0.5, unit = "cm")),
        legend.margin = margin(0, 0, 0, 0))+
  guides(color=guide_legend(nrow=2,byrow=TRUE))

mylegend<-g_legend(p5)

yleft <- textGrob(expression(paste("Adjusted survival probability")), 
                  rot = 90, gp = gpar(fontsize = 12))
bottom = textGrob(expression(paste("Time [y]")))
                  
subplot1 <- grid.arrange(p3,p5 + theme(legend.position="none"),
                         
                        layout_matrix = cbind(c(1,2),c(1,2)),
                        left=yleft, bottom=bottom)

plots=grid.arrange(p1, mylegend, subplot1,
                   layout_matrix = cbind(c(1,1,1,1,1,1),
                                         c(1,1,1,1,1,1),
                                         c(1,1,1,1,1,1),
                                         c(1,1,1,1,1,1),
                                         c(3,3,3,3,3,4),
                                         c(3,3,3,3,3,2), 
                                         c(3,3,3,3,3,4)))


ggsave(
  "/data/tu_fbeyer/myDocuments/Results/multivariate_risk_svd/Publications/Figures/dementia_overview.png", 
  plots, 
  device = agg_png, 
  width = 40, height = 20, units = "cm", res = 300,
  scaling = 1.4
)

```

```{r testing assumptions dementia, echo=F, eval=F}
#Testing Assumptions -> http://www.sthda.com/english/wiki/cox-model-assumptions
#test proportional hazard -> beta coefficients for risk factors does not vary over time
test.ph <- cox.zph(surv3)
ggcoxzph(test.ph)

#test influential cases outliers
ggcoxdiagnostics(surv3, type = "dfbeta",
                 linear.predictions = FALSE, ggtheme = theme_bw())

ggcoxdiagnostics(surv3, type = "deviance",
                 linear.predictions = FALSE, ggtheme = theme_bw())

```
